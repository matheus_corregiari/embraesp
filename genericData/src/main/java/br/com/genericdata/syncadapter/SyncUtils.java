package br.com.genericdata.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;

import br.com.genericdata.R;

public class SyncUtils {
	
    private static final String PREF_SETUP_COMPLETE = "setup_complete";

    public static void CreateSyncAccount(Context context) {

    	boolean newAccount = false;
        boolean setupComplete = PreferenceManager
                .getDefaultSharedPreferences(context).getBoolean(PREF_SETUP_COMPLETE, false);

        Account account = AuthenticatationService.GetAccount(context);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        if (accountManager.addAccountExplicitly(account, null, null)) {
        	
            ContentResolver.setIsSyncable(account, context.getResources().getString(R.string.accountType), 1);
            ContentResolver.setSyncAutomatically(account, context.getResources().getString(R.string.accountType), true);
            ContentResolver.addPeriodicSync(account, context.getResources().getString(R.string.accountType), new Bundle(), context.getResources().getInteger(R.integer.RepeatTimeInMillis));
            newAccount = true;
        }
        
        if (newAccount || !setupComplete) {
            TriggerRefresh(context);
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_SETUP_COMPLETE, true).commit();
        }
    }

    public static void TriggerRefresh(Context context) {
        
    	Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL   , true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        
        ContentResolver.requestSync(
        		AuthenticatationService.GetAccount(context), 
        		context.getResources().getString(R.string.accountType),
        		bundle);
    }
}
