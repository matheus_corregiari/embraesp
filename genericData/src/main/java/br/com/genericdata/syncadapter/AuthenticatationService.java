package br.com.genericdata.syncadapter;

import android.accounts.Account;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import br.com.genericdata.R;

public class AuthenticatationService extends Service{

	public static final String TAG = "AuthenticatationService";
	
	@Override
	public IBinder onBind(Intent intent) {
		return new AccountAuthenticator(this).getIBinder();
	}

	public static Account GetAccount(Context context) {
        final String accountName = context.getResources().getString(R.string.app_name);
        return new Account(accountName, context.getResources().getString(R.string.accountType));
    }
	
}
