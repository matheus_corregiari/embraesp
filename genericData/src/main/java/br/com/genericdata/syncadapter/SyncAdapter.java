package br.com.genericdata.syncadapter;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Collections;
import java.util.List;

import br.com.genericdata.coredata.activity.CoreActivity;
import br.com.genericdata.coredata.enums.ConnectionType;
import br.com.genericdata.coredata.interfaces.GenericDataResultDelegate;
import br.com.genericdata.database.DataBaseGerericData;
import br.com.genericdata.database.interfaces.DataBaseResultDelegate;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods;
import br.com.genericdata.manager.GenericDataManager;
import br.com.genericdata.utils.SerializableBundle;
import br.com.genericdata.valueobjects.GenericDataVO;
import br.com.genericdata.valueobjects.ValidatorVO;

public class SyncAdapter extends AbstractThreadedSyncAdapter implements GenericDataResultDelegate, DataBaseResultDelegate{

    private final ContentResolver mContentResolver;

    private Context mContext;
	private DataBaseGerericData gerericData;
	private GenericDataManager  genericDataManager;
    
    
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.mContentResolver = context.getContentResolver();
        
        this.mContext           = context;
		this.gerericData        = new DataBaseGerericData(this.mContext, this);
		this.genericDataManager = GenericDataManager.getInstance();
		this.genericDataManager.init(this.mContext, this);
		
    }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        this.mContentResolver = context.getContentResolver();
        
        this.mContext           = context;
		this.gerericData        = new DataBaseGerericData(this.mContext, this);
		this.genericDataManager = GenericDataManager.getInstance();
		this.genericDataManager.init(this.mContext, this);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

    	if(this.mContentResolver != null){
    		this.mContentResolver.getClass();
    	}

        if(this.gerericData != null){
            this.gerericData.getAllGenericDataVO();
        }
    }
    
    @SuppressWarnings("unchecked")
	@Override
	public void onDataBaseResult(String method, Object result) {
		if(method != null){
			if(method.equals(DataBaseGenericDataMethods.GetAllGenericDataVO.class.getSimpleName())){
				try{
					List<GenericDataVO> genericDataVOs = (List<GenericDataVO>) result;
					if(genericDataVOs != null && !genericDataVOs.isEmpty() && this.genericDataManager != null){
						Collections.sort(genericDataVOs);
                        for (GenericDataVO genericDataVO : genericDataVOs) {
                            Object[] parameters = (Object[]) genericDataVO.getData();
                            this.genericDataManager.sendGenericData(false, ConnectionType.valueOf(genericDataVO.getConnectionType()), genericDataVO.getServiceName(), parameters);
                        }
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onDataBaseError(String method, Exception exception) {}

	@Override
	public void onServerResult(String serviceName, boolean hasErrors,
			List<?> resultList, Object result, Object... parameters) {

        Intent intent = new Intent(CoreActivity.BROADCAST_WIFI);

        intent.putExtra("status", CoreActivity.RESULT_OK);

        intent.putExtra("serviceName", serviceName);
        intent.putExtra("hasErrors"  , hasErrors);
        intent.putExtra("resultList" , new SerializableBundle<Object>(resultList));
        intent.putExtra("result"     , new SerializableBundle<Object>(result));
        intent.putExtra("parameters" , new SerializableBundle<Object>(parameters));

        LocalBroadcastManager.getInstance(this.mContext).sendBroadcast(intent);
	}

	@Override
	public void onServerError(String serviceName, Exception exception,
			List<ValidatorVO> validators, Object... parameters) {
        Intent intent = new Intent(CoreActivity.BROADCAST_WIFI);

        intent.putExtra("status", CoreActivity.RESULT_ERROR);

        intent.putExtra("serviceName", serviceName);
        intent.putExtra("exception"  , new SerializableBundle<Exception>(exception));
        intent.putExtra("validators" , new SerializableBundle<Object>(validators));
        intent.putExtra("parameters" , new SerializableBundle<Object>(parameters));

        LocalBroadcastManager.getInstance(this.mContext).sendBroadcast(intent);
	}
}
