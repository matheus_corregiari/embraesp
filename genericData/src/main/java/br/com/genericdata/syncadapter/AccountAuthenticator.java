package br.com.genericdata.syncadapter;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import br.com.genericdata.R;

public class AccountAuthenticator extends AbstractAccountAuthenticator{

	private Context mContext;
	private final Handler handler = new Handler();
	
	public AccountAuthenticator(Context context) {
		super(context);
		this.mContext = context;
	}

	@Override
    public Bundle editProperties(AccountAuthenticatorResponse accountAuthenticatorResponse,
                                 String s) {
        throw new UnsupportedOperationException();
    }

	@Override
	public Bundle addAccount(AccountAuthenticatorResponse response,
			String accountType, String authTokenType,
			String[] requiredFeatures, Bundle options)
			throws NetworkErrorException {
		final Bundle bundle = new Bundle();
		
		AccountManager accountManager = AccountManager.get(this.mContext);
        Account account = AuthenticatationService.GetAccount(this.mContext);
        if(!accountManager.addAccountExplicitly(account, null, null)){
        	
            final String message = "Se for possivel adicionar uma conta.";
        	bundle.putInt(AccountManager.KEY_ERROR_CODE, 1);
            bundle.putString(AccountManager.KEY_ERROR_MESSAGE, message);
        	this.handler.post(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
				}
			});
        }else{
        	bundle.putString(AccountManager.KEY_ACCOUNT_NAME, this.mContext.getResources().getString(R.string.app_name));
        	bundle.putString(AccountManager.KEY_ACCOUNT_TYPE, this.mContext.getResources().getString(R.string.accountType));
        	bundle.putString(AccountManager.KEY_AUTHTOKEN   , this.mContext.getResources().getString(R.string.accountType));
        }

        return bundle;
	}

	@Override
	public Bundle confirmCredentials(AccountAuthenticatorResponse response,
			Account account, Bundle options) throws NetworkErrorException {
		return null;
	}

	@Override
    public Bundle getAuthToken(AccountAuthenticatorResponse accountAuthenticatorResponse,
                               Account account, String s, Bundle bundle)
            throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getAuthTokenLabel(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse,
                                    Account account, String s, Bundle bundle)
            throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse accountAuthenticatorResponse,
                              Account account, String[] strings)
            throws NetworkErrorException {
        throw new UnsupportedOperationException();
    }

}
