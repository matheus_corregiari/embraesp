package br.com.genericdata.manager;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.genericdata.R;
import br.com.genericdata.coredata.CoreData.SendGenericDataVO;
import br.com.genericdata.coredata.enums.ConnectionType;
import br.com.genericdata.coredata.interfaces.GenericDataResultDelegate;
import br.com.genericdata.database.interfaces.DataBaseResultDelegate;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods;
import br.com.genericdata.utils.DeviceUidGenerator;
import br.com.genericdata.valueobjects.GenericDataVO;
import br.com.genericdata.valueobjects.ValidatorVO;
import br.com.genericdata.valueobjects.WhoisVO;

public class GenericDataManager implements GenericDataResultDelegate, DataBaseResultDelegate{
	
	private Context                   mContext;
	private GenericDataResultDelegate coreDataDelegate;
	private static GenericDataManager genericDataManager;
	private WhoisVO                   whoisVO;
	
	private GenericDataManager() {
		this.mContext         = null;
		this.coreDataDelegate = null;
	}
	
	public static GenericDataManager getInstance(){
		
		if(genericDataManager == null){
			genericDataManager = new GenericDataManager();
		}
		
		return genericDataManager;
	}
	
	public void init(Context mContext,
			GenericDataResultDelegate coreDataDelegate) {
		this.mContext         = mContext;
		this.coreDataDelegate = coreDataDelegate;

        if(this.mContext == null){
            return;
        }

        String deviceUid = null;
        if(mContext != null){
            WifiManager wm = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            if(wm != null){
                deviceUid = wm.getConnectionInfo().getMacAddress();
            }
        }

		if( deviceUid == null ){
			DeviceUidGenerator uidGen = new DeviceUidGenerator(mContext);
			deviceUid = uidGen.getDeviceUuid().toString();
		}
		
		this.whoisVO = new WhoisVO(
				this.mContext.getString(R.string.applicationId),
				deviceUid, 
				0L, 
				0L, 
				null, 
				Locale.getDefault().getDisplayLanguage());
		
	}
	
	public void init(Context mContext) {
		this.mContext         = mContext;
		this.coreDataDelegate = null;

		TelephonyManager telephonyManager = (TelephonyManager) this.mContext.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.getDeviceId();
		
		this.whoisVO = new WhoisVO(
                this.mContext.getString(R.string.applicationId),
				telephonyManager.getDeviceId(), 
				0L, 
				0L, 
				null, 
				Locale.getDefault().getDisplayLanguage());
	}
	
	public void sendGenericData(boolean keepLocalData, ConnectionType connectionType, String serviceName, Object... object){

        if(object == null){
            if(this.coreDataDelegate != null){
                this.coreDataDelegate.onServerError(serviceName, null, null);
            }
        }

        if(!keepLocalData) {
            Object[] parameters = new Object[object.length + 1];
            parameters[0] = this.whoisVO;
            for (int i = 0; i < object.length; i++) {
                parameters[i+1] = object[i];
            }
            SendGenericDataVO genericDataVO = new SendGenericDataVO(this.mContext, this, connectionType, serviceName);
            genericDataVO.execute(parameters);
        }else{

            GenericDataVO genericDataVO = new GenericDataVO();

            Object[] parameters = new Object[4];
            parameters[0] = this.whoisVO;
            parameters[1] = genericDataVO.getToken();
            parameters[2] = serviceName;
            parameters[3] = object;

            genericDataVO.setConnectionType(connectionType.name());
            genericDataVO.setData(parameters);
            genericDataVO.setServiceName("genericDataServiceMobile.proxy");

            DataBaseGenericDataMethods.AddGenericDataVO addGenericDataVO = new DataBaseGenericDataMethods.AddGenericDataVO(this.mContext, this);
            addGenericDataVO.execute(genericDataVO);
        }
	}

	@Override
	public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result,
			Object... parameters) {
		if(this.coreDataDelegate != null){
			this.coreDataDelegate.onServerResult(serviceName, hasErrors, resultList, result, parameters);
		}
	}

	@Override
	public void onServerError(String serviceName, Exception exception,
			List<ValidatorVO> validators, Object... parameters) {
		if(this.coreDataDelegate != null){
			this.coreDataDelegate.onServerError(serviceName, exception, validators, parameters);
		}
	}

    @Override
    public void onDataBaseResult(String method, Object result) {

        if(DataBaseGenericDataMethods.AddGenericDataVO.class.getSimpleName().equals(method)){
            GenericDataVO dataVO = ((GenericDataVO) result);
            Object[] object = (Object[]) dataVO.getData();
            SendGenericDataVO genericDataVO = new SendGenericDataVO(this.mContext, this, ConnectionType.valueOf(dataVO.getConnectionType()), dataVO.getServiceName());
            genericDataVO.execute(object);
        }

    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        if(this.coreDataDelegate != null){
            this.coreDataDelegate.onServerError(
                    "DATABASE_" + DataBaseGenericDataMethods.AddGenericDataVO.class.getName(), exception, null);
        }
    }
}
