package br.com.genericdata.coredata;

import android.content.Context;
import android.os.AsyncTask;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalDate;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.genericdata.R;
import br.com.genericdata.coredata.enums.ConnectionType;
import br.com.genericdata.coredata.interfaces.GenericDataResultDelegate;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods;
import br.com.genericdata.utils.MarshalDouble;
import br.com.genericdata.valueobjects.GenericDataVO;
import br.com.genericdata.valueobjects.ResultVO;
import br.com.genericdata.valueobjects.ValidatorVO;
import br.com.genericdata.valueobjects.WhoisVO;
import flex.messaging.io.amf.client.AMFConnection;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;
import flex.messaging.io.amf.client.exceptions.ServerStatusException;

public class CoreData{
	public static class SendGenericDataVO extends AsyncTask<Object, Object, Object>{

        private ConnectionType connectionType;
        private Context                   mContext;
		private GenericDataResultDelegate dataDelegate;
		private String                    serviceName;

		public SendGenericDataVO(Context mContext, GenericDataResultDelegate dataDelegate, ConnectionType connectionType, String serviceName){
			this.mContext     = mContext;
			this.dataDelegate = dataDelegate;
            this.connectionType = connectionType;
            this.serviceName = serviceName;
		}

		@Override
		protected Object doInBackground(Object... params) {
			
			try {

                if(this.mContext == null){
                    throw new IllegalArgumentException("Context Null");
                }

                if(this.connectionType == null){
                    throw new IllegalArgumentException("ConnectionType Null");
                }

                if(this.serviceName == null || this.serviceName.equals("")){
                    throw new IllegalArgumentException("ServiceName Null or Empty");
                }

                if(params == null || params.length <= 0){
                    throw new IllegalArgumentException("Params Null Or Empty");
                }

                WhoisVO whoisVO = null;

                whoisVO = (WhoisVO) params[0];

                if(whoisVO == null){
                    throw new IllegalArgumentException("Whois Null");
                }

                if(this.connectionType.equals(ConnectionType.TYPE_AMF)){
                    return this.providerAMF(params);
                }

                if(this.connectionType.equals(ConnectionType.TYPE_REST)){
                    return this.providerREST(params);
                }

                if(this.connectionType.equals(ConnectionType.TYPE_SOAP)){
                    return this.providerSOAP(params);
                }

                return null;

            } catch (Exception e) {
                return e;
            }
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			
			if(this.dataDelegate != null ){
				if(result == null){
					this.dataDelegate.onServerResult(this.serviceName, true, null, null);
				}else if(!(result instanceof Exception)){
					proccessResult(result);
				}else{
					this.dataDelegate.onServerError(this.serviceName, (Exception) result, null);
				}
			}
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private void proccessResult (Object object){
			
			Object            result       = null;
			List<?>           resultList   = null;
			List<ValidatorVO> validators   = null;
            Object            data         = null;
            Integer           resultStatus;
			boolean           hasErrors    = false;

            ResultVO resultVO;
			
			if(object instanceof ResultVO){
				resultVO = (ResultVO) object;
				
				result       = resultVO.getResult();
				resultList   = resultVO.getResultList();
				validators   = resultVO.getValidators();
                data         = resultVO.getData();
				resultStatus = resultVO.getStatus();
				hasErrors    = resultStatus == ResultVO.ERROR;
				
			}

            if(data != null && data instanceof String){

                GenericDataVO genericDataVO = new GenericDataVO();
                genericDataVO.setToken((String) data);

                DataBaseGenericDataMethods.DeleteGenericDataVO deleteGenericDataVO = new DataBaseGenericDataMethods.DeleteGenericDataVO(this.mContext, null);
                deleteGenericDataVO.execute(genericDataVO);
            }

			if(validators != null && !validators.isEmpty()){
				this.dataDelegate.onServerError(this.serviceName, null, validators);
			}
			
			if(object instanceof ResultVO){
				this.dataDelegate.onServerResult(this.serviceName, hasErrors, resultList, result);
				return;
			}
			
			if(object instanceof List){
				this.dataDelegate.onServerResult(this.serviceName, false, (List<?>) object, null);
			}else{
				this.dataDelegate.onServerResult(this.serviceName, false, null, object);
			}
			
		}

        private void resgisterAlias(){
            String[] aliases = this.mContext.getResources().getStringArray(R.array.registerAlias);
            if(aliases != null && (aliases.length %2) == 0){
                for (int i = 0; i < (aliases.length - 1); i += 2) {
                    AMFConnection.registerAlias(aliases[i], aliases[i+1]);
                }
            }
        }

        private Object providerAMF(Object... parameters){
            try {

                Object object = null;

                AMFConnection amfConnection = new AMFConnection();
                amfConnection.connect(this.mContext.getResources().getString(R.string.endpointAmf));
                amfConnection.addAmfHeader("Accept-Encoding" , "gzip");
                amfConnection.addAmfHeader("Content-Encoding", "gzip");
                amfConnection.addAmfHeader("User-Agent"      , "gzip");

                AMFConnection.registerAlias(
                        "br.com.xds.proximity.domain.core.valueobjects.GenericDataVO",
                        GenericDataVO.class.getName());
                AMFConnection.registerAlias(
                        "br.com.xds.proximity.domain.core.valueobjects.ResultVO",
                        ResultVO.class.getName());
                AMFConnection.registerAlias(
                        "br.com.xds.proximity.domain.core.valueobjects.ValidatorVO",
                        ValidatorVO.class.getName());
                AMFConnection.registerAlias(
                        "br.com.xds.proximity.domain.core.valueobjects.WhoisVO",
                        WhoisVO.class.getName());

                this.resgisterAlias();

                if(parameters == null || parameters.length == 0){
                    object = amfConnection.call(this.serviceName);
                }else{
                    object = amfConnection.call(this.serviceName, parameters);
                }

                amfConnection.close();

                return object;
            }catch (ClientStatusException e) {
                return e;
            }catch (ServerStatusException e) {
                return e;
            }catch (Exception e) {
                return e;
            }
        }

        private Object providerREST(Object... parameters){
            try{

                InputStream inputStream = null;
                String result = "";

                HttpClient httpclient = new DefaultHttpClient();

                HttpResponse httpResponse = httpclient.execute(new HttpGet(this.serviceName + "?" + "token=" + "geoembraesp" + "&" + "unidadeId=" + "1"));

                inputStream = httpResponse.getEntity().getContent();

                /*if(inputStream != null){
                    result = convertInputStreamToString(inputStream);
                    return result;
                }*/
                return null;
            } catch (Exception e) {
                return e;
            }
        }

        private Object providerSOAP(Object... parameters){
            try{
                SoapObject request = new SoapObject((String) parameters[1], (String) parameters[2]);

                WhoisVO parameter = (WhoisVO) parameters[0];
                PropertyInfo propertyInfo1 = new PropertyInfo();
                propertyInfo1.setName("deviceId");
                propertyInfo1.setValue(parameter.getDeviceUniqueIdentifier());
                System.out.println(parameter.getDeviceUniqueIdentifier());
                propertyInfo1.setType(String.class);
                request.addProperty(propertyInfo1);

                if(parameters.length > 4){
                    for (int i = 4; i < parameters.length; i++){
                        PropertyInfo propertyInfo = (PropertyInfo) parameters[i];
                        request.addProperty(propertyInfo);
                    }
                }

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                new MarshalDouble().register(envelope);
                new MarshalDate().register(envelope);
                new MarshalFloat().register(envelope);

                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE androidHttpTransport = new HttpTransportSE(this.mContext.getString(R.string.endpoint));

                androidHttpTransport.call(this.serviceName, envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                return parseJsonObject(response.toString(), (Class) parameters[3]);
            } catch (Exception e) {
                return e;
            }
        }

        private Object parseJsonObject(String stringToParse, Class classToParse){

            if(stringToParse == null || stringToParse.equals("")){
                return null;
            }

            try {

                if(!stringToParse.startsWith("[") && !stringToParse.startsWith("{")) {
                    return stringToParse;
                }

                List<Object> objects = new ArrayList<Object>();
                ObjectMapper mapper = new ObjectMapper();
                JsonFactory f = new JsonFactory();
                JsonParser jp = f.createParser(stringToParse);
                jp.nextToken();
                if(stringToParse.startsWith("[")) {
                    while (jp.nextToken() == JsonToken.START_OBJECT) {
                        Object instance = mapper.readValue(jp, classToParse);
                        objects.add(instance);
                    }
                }else{
                    if(stringToParse.startsWith("{")) {
                        Object instance = mapper.readValue(jp, classToParse);
                        return instance;
                    }else{
                        return stringToParse;
                    }
                }

                return objects;

            }catch (JsonParseException e) {
                return e;
            } catch (IOException e) {
                return e;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return e;
            }
        }

	}
	
}
