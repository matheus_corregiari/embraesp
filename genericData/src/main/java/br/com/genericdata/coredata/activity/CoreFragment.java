package br.com.genericdata.coredata.activity;

import android.support.v4.app.Fragment;

import br.com.genericdata.coredata.interfaces.GenericDataResultDelegate;
import br.com.genericdata.coredata.servermanager.ServerManagerDelegate;

public abstract class CoreFragment extends Fragment implements GenericDataResultDelegate {
	
	private ServerManagerDelegate serverManagerDelegate = null;
	
	public void getFromServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.get(parameters);
		}
	}

	public void setFromServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.set(parameters);
		}
	}
	
	public void deleteFromServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.delete(parameters);
		}
	}
	
	public void sendToServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.send(parameters);
		}
	}
	
	public ServerManagerDelegate getServerManagerDelegate() {
		return serverManagerDelegate;
	}

	public void setServerManagerDelegate(ServerManagerDelegate serverManagerDelegate) {
		this.serverManagerDelegate = serverManagerDelegate;
	}
	
}
