package br.com.genericdata.coredata.interfaces;

import java.util.List;

import br.com.genericdata.valueobjects.ValidatorVO;

public interface GenericDataResultDelegate {
		
	public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters);
	
	public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters);
}
