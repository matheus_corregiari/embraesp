package br.com.genericdata.coredata.enums;

/**
 * Created by matheus on 25/07/2014.
 */
public enum ConnectionType {
    TYPE_AMF ,
    TYPE_REST,
    TYPE_SOAP;
}
