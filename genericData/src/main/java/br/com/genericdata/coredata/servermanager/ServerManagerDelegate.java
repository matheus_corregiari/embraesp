package br.com.genericdata.coredata.servermanager;

import br.com.genericdata.coredata.enums.ConnectionType;

public interface ServerManagerDelegate {

	public void sendToServer(boolean keepLocalData, ConnectionType connectionType, String serviceName, Object... objects);
	public void get         (Object... objects);
	public void set         (Object... objects);
	public void delete      (Object... objects);
	public void send        (Object... objects);
    public void save        (Object... objects);
	
}
