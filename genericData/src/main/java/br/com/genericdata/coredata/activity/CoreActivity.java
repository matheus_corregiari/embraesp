package br.com.genericdata.coredata.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;

import java.util.List;

import br.com.genericdata.coredata.interfaces.GenericDataResultDelegate;
import br.com.genericdata.coredata.servermanager.ServerManagerDelegate;
import br.com.genericdata.syncadapter.SyncUtils;
import br.com.genericdata.utils.SerializableBundle;
import br.com.genericdata.valueobjects.ValidatorVO;

public abstract class CoreActivity extends FragmentActivity implements GenericDataResultDelegate {
	
	private ServerManagerDelegate serverManagerDelegate = null;

    public static final int RESULT_OK = 1;
    public static final int RESULT_ERROR = 0;

    public static final String BROADCAST_WIFI = "br.com.genericdata.coredata.activity.goToHomeReceiver";

    protected BroadcastReceiver goToHomeReceiver = new BroadcastReceiver() {

        @SuppressWarnings({ "rawtypes", "unchecked" })
        @Override
        public void onReceive(Context context, Intent intent) {

            int status = intent.getExtras().getInt("status");

            String serviceName  = intent.getExtras().getString("serviceName");
            Object[] parameters = (Object[])  ((SerializableBundle) intent.getExtras().getSerializable("parameters")).getObject();

            if(status == RESULT_OK){

                boolean hasErrors  = intent.getExtras().getBoolean("hasErrors");
                List<?> resultList = ((SerializableBundle) intent.getExtras().getSerializable("resultList")).getList();
                Object result      = ((SerializableBundle) intent.getExtras().getSerializable("result")).getObject();
                onServerResult(serviceName, hasErrors, resultList, result, parameters);

            }else if (status == RESULT_ERROR){

                List<ValidatorVO> validators = ((SerializableBundle) intent.getExtras().getSerializable("validators")).getList();
                Exception exception          = (Exception) ((SerializableBundle) intent.getExtras().getSerializable("exception")).getObject();
                onServerError(serviceName, exception, validators, parameters);

            }

        }

    };

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver( goToHomeReceiver, new IntentFilter(BROADCAST_WIFI));
        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(goToHomeReceiver);
        super.onPause();
    }

	public void getFromServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.get(parameters);
		}
	}

	public void setFromServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.set(parameters);
		}
	}
	
	public void deleteFromServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.delete(parameters);
		}
	}
	
	public void sendToServer(Object...parameters){
		if(this.serverManagerDelegate != null){
			this.serverManagerDelegate.send(parameters);
		}
	}

    public void saveOnServer(Object...parameters){
        if(this.serverManagerDelegate != null){
            this.serverManagerDelegate.save(parameters);
        }
    }

    public void CreateSyncAccount(){
        SyncUtils.CreateSyncAccount(this.getApplicationContext());
    }

    public void SyncNow(){
        SyncUtils.TriggerRefresh(this.getApplicationContext());
    }

	public ServerManagerDelegate getServerManagerDelegate() {
		return serverManagerDelegate;
	}

	public void setServerManagerDelegate(ServerManagerDelegate serverManagerDelegate) {
		this.serverManagerDelegate = serverManagerDelegate;
	}
	
}
