package br.com.genericdata.coredata.servermanager;

import android.content.Context;

import java.util.List;

import br.com.genericdata.coredata.enums.ConnectionType;
import br.com.genericdata.coredata.interfaces.GenericDataResultDelegate;
import br.com.genericdata.manager.GenericDataManager;
import br.com.genericdata.valueobjects.ValidatorVO;

public abstract class ServerManager implements ServerManagerDelegate, GenericDataResultDelegate{
	
	public    Context                   mContext;
	protected GenericDataResultDelegate genericDataResultDelegate;
	protected GenericDataManager        genericDataManager;
	
	public ServerManager(Context mContext, GenericDataResultDelegate genericDataResultDelegate) {
		super();
		this.mContext = mContext;
		this.genericDataResultDelegate = genericDataResultDelegate;
		this.genericDataManager = GenericDataManager.getInstance();
		this.genericDataManager.init(this.mContext, this);
	}

	@Override
	public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result,
			Object... parameters) {
        try {
            if (this.genericDataResultDelegate != null) {
                this.genericDataResultDelegate.onServerResult(serviceName, hasErrors, resultList, result, parameters);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
	}

	@Override
	public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {
        try {
            if(this.genericDataResultDelegate != null){
                this.genericDataResultDelegate.onServerError(serviceName, exception, validators, parameters);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
	}

	@Override
	public void sendToServer(boolean keepLocalData, ConnectionType connectionType, String serviceName, Object... objects) {
		if(this.genericDataManager == null){
			return;
		}
		if(objects == null){
			return;
		}
		if(serviceName == null || serviceName.equals("")){
			return;
		}
		this.genericDataManager.sendGenericData(keepLocalData, connectionType, serviceName, objects);
	}

}
