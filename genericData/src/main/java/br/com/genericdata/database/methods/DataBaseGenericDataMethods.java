package br.com.genericdata.database.methods;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.genericdata.database.DataBaseGerericData;
import br.com.genericdata.database.interfaces.DataBaseResultDelegate;
import br.com.genericdata.utils.Utils;
import br.com.genericdata.valueobjects.GenericDataVO;

public class DataBaseGenericDataMethods {

	public static class AddGenericDataVO extends AsyncTask<GenericDataVO, GenericDataVO, Object>{
		private Context mContext;
		private DataBaseResultDelegate resultDelegate;
		
		public AddGenericDataVO(Context mContext, DataBaseResultDelegate resultDelegate){
			this.mContext = mContext;
			this.resultDelegate = resultDelegate;
		}
		
		@Override
		protected Object doInBackground(GenericDataVO... params) {
				
				try {
					
					GenericDataVO dataVO = params[0];
					
					if (!checkIfGenericDataVOExists(this.mContext, dataVO)){
						DataBaseGerericData base = new DataBaseGerericData(this.mContext);
						SQLiteDatabase db = base.getWritableDatabase();
						ContentValues values = new ContentValues();
						long timeInMillis = Calendar.getInstance().getTimeInMillis();
                        values.put(DataBaseGerericData.GENERIC_DATA_TOKEN           , dataVO.getToken());
						values.put(DataBaseGerericData.GENERIC_DATA_CREATION_DATE   , timeInMillis);
						values.put(DataBaseGerericData.GENERIC_DATA_LAST_UPDATE_DATE, timeInMillis);
						values.put(DataBaseGerericData.GENERIC_DATA_DATA            , Utils.serialize(dataVO.getData()));
						values.put(DataBaseGerericData.GENERIC_DATA_SERVICE_NAME    , dataVO.getServiceName());
                        values.put(DataBaseGerericData.GENERIC_DATA_CONNECTION_TYPE , dataVO.getConnectionType());

                        Long aux = db.insert(DataBaseGerericData.TABLE_GENERIC_DATA , null, values);
						db.close();
						base.close();
						
						if(aux == null || aux <= 0){
							throw new IllegalArgumentException( "<<ADD_GENERIC_DATA_VO_INSERT_ERROR>>" );
						}
						
						Calendar date = Calendar.getInstance();
						date.setTimeInMillis(timeInMillis);
						
						dataVO.setLastUpdateDate(date.getTime());
						dataVO.setCreationDate(date.getTime());
						
						return dataVO;
					}
					else {
						UpdateGenericDataVO genericData = new UpdateGenericDataVO(this.mContext, this.resultDelegate);
						genericData.execute(dataVO);
						return null;
					}
				} catch(IllegalArgumentException e ){
					return e;
				} catch (IOException e) {
					return e;
				} catch (Exception e) {
					return e;
				}
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			
			if(result != null){
				if(this.mContext != null && this.resultDelegate != null && !(result instanceof Exception)){
					this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
				}else{
					if(this.resultDelegate!= null && result instanceof Exception)
						this.resultDelegate.onDataBaseError(this.getClass().getSimpleName(), (Exception) result);
				}
			}else{
                if(this.resultDelegate!= null)
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
            }

		}
	}
	
	public static class DeleteGenericDataVO extends AsyncTask<GenericDataVO, GenericDataVO, Object>{
		
		private Context mContext;
		private DataBaseResultDelegate resultDelegate;
		
		public DeleteGenericDataVO(Context mContext, DataBaseResultDelegate resultDelegate){
			this.mContext = mContext;
			this.resultDelegate = resultDelegate;
		}
		
		@Override
		protected Object doInBackground(GenericDataVO... params) {
			try{
				GenericDataVO dataVO = params[0];
				
				if (dataVO.getToken() != null && !dataVO.getToken().equals("")) {
					throw new IllegalArgumentException( "<<DELETE_GENERIC_DATA_VO_TOKEN_NULL_OR_EMPTY>>" );
				}
					
				DataBaseGerericData base = new DataBaseGerericData(this.mContext);
				SQLiteDatabase db = base.getWritableDatabase();
				Integer deleted = db.delete(DataBaseGerericData.TABLE_GENERIC_DATA, DataBaseGerericData.GENERIC_DATA_TOKEN + " = ?",
						new String[] { dataVO.getToken() });
				db.close();
				base.close();
				
				if(deleted == null || deleted <= 0){
					throw new IllegalArgumentException( "<<DELETE_GENERIC_DATA_VO_DELETE_ERROR>>" );
				}
					
				return dataVO;
				
			} catch (Exception e) {
				return e;
			}
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);

            if(result != null){
                if(this.mContext != null && this.resultDelegate != null && !(result instanceof Exception)){
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
                }else{
                    if(this.resultDelegate!= null && result instanceof Exception)
                        this.resultDelegate.onDataBaseError(this.getClass().getSimpleName(), (Exception) result);
                }
            }else{
                if(this.resultDelegate!= null)
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
            }
			
		}
	}
	
	public static class UpdateGenericDataVO extends AsyncTask<GenericDataVO, GenericDataVO, Object>{
		
		private Context mContext;
		private DataBaseResultDelegate resultDelegate;
		
		public UpdateGenericDataVO(Context mContext, DataBaseResultDelegate delegate){
			this.mContext = mContext;
			this.resultDelegate = delegate;
		}
		
		@Override
		protected Object doInBackground(GenericDataVO... params) {
			try {
				
				GenericDataVO dataVO = params[0];
				
				if(dataVO.getToken() == null || dataVO.getToken().equals("")){
					throw new IllegalArgumentException( "<<UPDATE_GENERIC_DATA_VO_TOKEN_NULL_OR_EMPTY>>" );
				}
				
				DataBaseGerericData base = new DataBaseGerericData(this.mContext);
				SQLiteDatabase db = base.getWritableDatabase();
				ContentValues values = new ContentValues();
				Integer rows_updated = 0;
				long timeInMillis = Calendar.getInstance().getTimeInMillis();
				values.put(DataBaseGerericData.GENERIC_DATA_TOKEN           , dataVO.getToken());
				values.put(DataBaseGerericData.GENERIC_DATA_CREATION_DATE   , dataVO.getCreationDate().getTime());
				values.put(DataBaseGerericData.GENERIC_DATA_LAST_UPDATE_DATE, timeInMillis);
				values.put(DataBaseGerericData.GENERIC_DATA_DATA            , Utils.serialize(dataVO.getData()));
				values.put(DataBaseGerericData.GENERIC_DATA_SERVICE_NAME    , dataVO.getServiceName());
                values.put(DataBaseGerericData.GENERIC_DATA_CONNECTION_TYPE , dataVO.getConnectionType());
				rows_updated = db.update(DataBaseGerericData.TABLE_GENERIC_DATA, values, DataBaseGerericData.GENERIC_DATA_TOKEN + " = ?",
						new String[] { dataVO.getToken() });
				db.close();
				base.close();
				
				if(rows_updated == null){
					throw new IllegalArgumentException( "<<UPDATE_GENERIC_DATA_VO_UPDATE_ERROR>>" );
				}
				
				Calendar date = Calendar.getInstance();
				date.setTimeInMillis(timeInMillis);
				
				dataVO.setLastUpdateDate(date.getTime());
				
				return dataVO;
			} catch (IOException e) {
				return e;
			}
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);

            if(result != null){
                if(this.mContext != null && this.resultDelegate != null && !(result instanceof Exception)){
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
                }else{
                    if(this.resultDelegate!= null && result instanceof Exception)
                        this.resultDelegate.onDataBaseError(this.getClass().getSimpleName(), (Exception) result);
                }
            }else{
                if(this.resultDelegate!= null)
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
            }
		}
	}
	
	public static class GetGenericDataVO extends AsyncTask<String, GenericDataVO, Object>{
		
		private Context mContext;
		private DataBaseResultDelegate resultDelegate;
		
		public GetGenericDataVO(Context mContext, DataBaseResultDelegate delegate){
			this.mContext = mContext;
			this.resultDelegate = delegate;
		}
		
		@Override
		protected Object doInBackground(String... params) {
			try {
			
				if(params == null || params.length == 0){
					throw new IllegalArgumentException( "<<GET_GENERIC_DATA_VO_PARAMS_NULL_OR_EMPTY>>" );
				}
				
				String token = params[0];
				
				if(token == null || token.equals("")){
					throw new IllegalArgumentException( "<<GET_GENERIC_DATA_VO_PARAMS_TOKEN_NULL_OR_EMPTY>>" );
				}
				
				DataBaseGerericData base = new DataBaseGerericData(this.mContext);
				SQLiteDatabase db = base.getReadableDatabase();
				Cursor cursor = db.query(DataBaseGerericData.TABLE_GENERIC_DATA, DataBaseGerericData.TABLE_GENERIC_DATA_COLUMNS, DataBaseGerericData.GENERIC_DATA_TOKEN
						+ " = ?", params, null, null, null, null);
				GenericDataVO genericDataVO = null;
				if (cursor != null && cursor.getCount() > 0) {
					cursor.moveToFirst();
	
					try {
						genericDataVO = new GenericDataVO();
						genericDataVO.setToken(cursor.getString(1));
						Calendar aux = Calendar.getInstance();
						aux.setTimeInMillis(cursor.getLong(2));
						genericDataVO.setCreationDate(aux.getTime());
						aux.setTimeInMillis(cursor.getLong(3));
						genericDataVO.setLastUpdateDate(aux.getTime());
						genericDataVO.setData(Utils.deserialize(cursor.getBlob(4)));
						genericDataVO.setServiceName(cursor.getString(5));
                        genericDataVO.setConnectionType(cursor.getString(6));
					} catch (ClassNotFoundException e) {
						db.close();
						base.close();
						if (cursor != null ) {
							cursor.close();
			            }
						return e;
					} catch (IOException e) {
						db.close();
						base.close();
						if (cursor != null ) {
							cursor.close();
			            }
						return e;
					}
					
					if (cursor != null ) {
						cursor.close();
		            }
				}else{
					db.close();
					base.close();
					throw new IllegalArgumentException( "<<GET_GENERIC_DATA_VO_TOKEN_NOT_FOUND>>" );
				}
				
				db.close();
				base.close();
				return genericDataVO;
			} catch (Exception e) {
				return e;
			}
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);

            if(result != null){
                if(this.mContext != null && this.resultDelegate != null && !(result instanceof Exception)){
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
                }else{
                    if(this.resultDelegate!= null && result instanceof Exception)
                        this.resultDelegate.onDataBaseError(this.getClass().getSimpleName(), (Exception) result);
                }
            }else{
                if(this.resultDelegate!= null)
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
            }
		}
		
	}
	
	public static class GetAllGenericDataVO extends AsyncTask<Void, GenericDataVO, Object>{
		
		private Context mContext;
		private DataBaseResultDelegate resultDelegate;
		
		public GetAllGenericDataVO(Context mContext, DataBaseResultDelegate delegate){
			this.mContext = mContext;
			this.resultDelegate = delegate;
		}
		
		@Override
		protected Object doInBackground(Void... params) {
			try {
			
				DataBaseGerericData base = new DataBaseGerericData(this.mContext);
				String query = "SELECT  * FROM " + DataBaseGerericData.TABLE_GENERIC_DATA;
				SQLiteDatabase db = base.getWritableDatabase();
				Cursor cursor = db.rawQuery(query, null);
	
				List<GenericDataVO> genericDataVOs = new ArrayList<GenericDataVO>();
	
				if (cursor != null && cursor.getCount() > 0) {
					cursor.moveToFirst();
					do {
						try {
							GenericDataVO genericDataVO = new GenericDataVO();
							genericDataVO.setToken(cursor.getString(1));
							Calendar aux = Calendar.getInstance();
							aux.setTimeInMillis(cursor.getLong(2));
							genericDataVO.setCreationDate(aux.getTime());
							aux.setTimeInMillis(cursor.getLong(3));
							genericDataVO.setLastUpdateDate(aux.getTime());
							genericDataVO.setData(Utils.deserialize(cursor.getBlob(4)));
							genericDataVO.setServiceName(cursor.getString(5));
                            genericDataVO.setConnectionType(cursor.getString(6));
							
							genericDataVOs.add(genericDataVO);
	
						} catch (ClassNotFoundException e) {
							db.close();
							base.close();
							if (cursor != null ) {
								cursor.close();
				            }
							return e;
						} catch (IOException e) {
							db.close();
							base.close();
							if (cursor != null ) {
								cursor.close();
				            }
							return e;
						}
					} while (cursor.moveToNext());
					
					if (cursor != null ) {
						cursor.close();
		            }
				}else{
					db.close();
					base.close();
					throw new IllegalArgumentException( "<<GET_ALL_GENERIC_DATA_VO_NOTHING_FOUND>>" );
				}
		
				db.close();
				base.close();
				return genericDataVOs;
				
			} catch (Exception e) {
				return e;
			}
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);

            if(result != null){
                if(this.mContext != null && this.resultDelegate != null && !(result instanceof Exception)){
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
                }else{
                    if(this.resultDelegate!= null && result instanceof Exception)
                        this.resultDelegate.onDataBaseError(this.getClass().getSimpleName(), (Exception) result);
                }
            }else{
                if(this.resultDelegate!= null)
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
            }

		}
	}

	public static class DeleteGenericDataVOByList extends AsyncTask<Object, GenericDataVO, Object>{
		
		private Context mContext;
		private DataBaseResultDelegate resultDelegate;
		
		public DeleteGenericDataVOByList(Context mContext, DataBaseResultDelegate resultDelegate){
			this.mContext = mContext;
			this.resultDelegate = resultDelegate;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		protected Object doInBackground(Object... params) {
			try{
				
				if(params == null || params.length <= 0){
					throw new IllegalArgumentException( "<<DELETE_GENERIC_DATA_VO_BY_LIST_PARAMS_NULL_OR_EMPTY>>" );
				}
				
				List<GenericDataVO> dataVOs = (List<GenericDataVO>) params[0];
				
				if(dataVOs == null || dataVOs.isEmpty()){
					throw new IllegalArgumentException( "<<DELETE_GENERIC_DATA_VO_BY_LIST_PARAMS_OBJECT_NULL_OR_EMPTY>>" );
				}
				
				DataBaseGerericData base = new DataBaseGerericData(this.mContext);
				SQLiteDatabase db = base.getWritableDatabase();
				for (GenericDataVO list : dataVOs) {
					try{
						if (list.getToken() != null && !list.getToken().equals("")) {
							Integer deleted = db.delete(DataBaseGerericData.TABLE_GENERIC_DATA, DataBaseGerericData.GENERIC_DATA_TOKEN + " = ?",
									new String[] { list.getToken() });
							
							if(deleted == null || deleted <=0){
								dataVOs.remove(list);
							}
							
						}else{
							dataVOs.remove(list);
						}
					} catch (Exception e) {
						db.close();
						base.close();
						return e;
					}
				}
				db.close();
				base.close();
				return dataVOs;
			}catch(IllegalArgumentException e){
				return e;
			}catch(Exception e){
				return e;
			}
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);

            if(result != null){
                if(this.mContext != null && this.resultDelegate != null && !(result instanceof Exception)){
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
                }else{
                    if(this.resultDelegate!= null && result instanceof Exception)
                        this.resultDelegate.onDataBaseError(this.getClass().getSimpleName(), (Exception) result);
                }
            }else{
                if(this.resultDelegate!= null)
                    this.resultDelegate.onDataBaseResult(this.getClass().getSimpleName(), result);
            }

		}
		
	}	
	
	private static boolean checkIfGenericDataVOExists(Context mContext, GenericDataVO genericDataVO){
		try {
			
			DataBaseGerericData base = new DataBaseGerericData(mContext);
			SQLiteDatabase db = base.getReadableDatabase();
			Cursor cursor = db.query(DataBaseGerericData.TABLE_GENERIC_DATA, DataBaseGerericData.TABLE_GENERIC_DATA_COLUMNS, DataBaseGerericData.GENERIC_DATA_TOKEN
					+ " = ?", new String[]{ genericDataVO.getToken() }, null, null, null, null);
			if (cursor != null) {
				if(cursor.getCount() > 0){
					db.close();
					base.close();
					cursor.close();
					return true;
				}
				cursor.close();
			}
			
			db.close();
			base.close();
			return false;
		} catch (Exception e) {
			return false;
		}
	}

}
