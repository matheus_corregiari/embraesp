package br.com.genericdata.database.interfaces;

public interface DataBaseResultDelegate {

	public void onDataBaseResult(String method, Object result);
	
	public void onDataBaseError(String method, Exception exception);
	
}
