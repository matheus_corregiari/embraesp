package br.com.genericdata.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;
import java.util.concurrent.Executor;

import br.com.genericdata.database.interfaces.DataBaseResultDelegate;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods.AddGenericDataVO;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods.DeleteGenericDataVO;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods.DeleteGenericDataVOByList;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods.GetAllGenericDataVO;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods.GetGenericDataVO;
import br.com.genericdata.database.methods.DataBaseGenericDataMethods.UpdateGenericDataVO;
import br.com.genericdata.valueobjects.GenericDataVO;

public class DataBaseGerericData extends SQLiteOpenHelper {

	public final static String DATABASE_NAME = "DATABASE_GENERIC_DATA";
	public final static int DATABASE_VERSION = 1;
	public Context mContext;
	public DataBaseResultDelegate resultDelegate;

	// TABELA CATEGORIA
	public final static String TABLE_GENERIC_DATA              = "Generic_Data";
	public final static String GENERIC_DATA_ID                 = "Id";
	public final static String GENERIC_DATA_TOKEN              = "Token";
	public final static String GENERIC_DATA_CREATION_DATE      = "Creation_Date";
	public final static String GENERIC_DATA_LAST_UPDATE_DATE   = "Last_Update_Date";
	public final static String GENERIC_DATA_DATA               = "Data";
	public final static String GENERIC_DATA_SERVICE_NAME       = "Service_Name";
    public final static String GENERIC_DATA_CONNECTION_TYPE    = "Connection_Type";
	public final static String[] TABLE_GENERIC_DATA_COLUMNS = { GENERIC_DATA_ID, GENERIC_DATA_TOKEN,
			GENERIC_DATA_CREATION_DATE, GENERIC_DATA_LAST_UPDATE_DATE, GENERIC_DATA_DATA,
            GENERIC_DATA_SERVICE_NAME, GENERIC_DATA_CONNECTION_TYPE};

	public DataBaseGerericData(Context mContext) {
		super(mContext, DATABASE_NAME, null, DATABASE_VERSION);
		this.mContext = mContext;
	}

	public DataBaseGerericData(Context mContext, DataBaseResultDelegate resultDelegate) {
		super(mContext, DATABASE_NAME, null, DATABASE_VERSION);
		this.mContext = mContext;
		this.resultDelegate = resultDelegate;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_DATABASE = "CREATE TABLE " + TABLE_GENERIC_DATA + " ( "
				+ GENERIC_DATA_ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ GENERIC_DATA_TOKEN            + " TEXT NOT NULL, "
				+ GENERIC_DATA_CREATION_DATE    + " DATE NOT NULL, "
				+ GENERIC_DATA_LAST_UPDATE_DATE + " DATE NOT NULL, "
				+ GENERIC_DATA_DATA	            + " BLOB NOT NULL, " 
				+ GENERIC_DATA_SERVICE_NAME     + " TEXT NOT NULL, "
                + GENERIC_DATA_CONNECTION_TYPE  + " TEXT NOT NULL"+ ")";

		db.execSQL(CREATE_DATABASE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GENERIC_DATA);
		this.onCreate(db);

	}

	public void updateGenericDataVO(GenericDataVO genericDataVO){
		UpdateGenericDataVO genericData = new UpdateGenericDataVO(this.mContext, this.resultDelegate);
		genericData.execute(new GenericDataVO[]{ genericDataVO });
	}
	
	public void addGenericDataVO(GenericDataVO genericDataVO){
		AddGenericDataVO genericData = new AddGenericDataVO(this.mContext, this.resultDelegate);
		genericData.execute(new GenericDataVO[]{ genericDataVO });
	}
	
	public void deleteGenericDataVO(GenericDataVO genericDataVO){
		DeleteGenericDataVO genericData = new DeleteGenericDataVO(this.mContext, this.resultDelegate);
		genericData.execute(new GenericDataVO[]{ genericDataVO });
	}
	
	public void deleteGenericDataVOByList(List<GenericDataVO> genericDataVO){
		DeleteGenericDataVOByList genericData = new DeleteGenericDataVOByList(this.mContext, this.resultDelegate);
		genericData.execute(new Object[]{ genericDataVO });
	}

	public void getAllGenericDataVO(){
		GetAllGenericDataVO genericData = new GetAllGenericDataVO(this.mContext, this.resultDelegate);
		genericData.execute();
	}
	
	public void getGenericDataVO(String token) {
		GetGenericDataVO genericData = new GetGenericDataVO(this.mContext, this.resultDelegate);
		genericData.execute(new String[]{ token });
	}
	
}
