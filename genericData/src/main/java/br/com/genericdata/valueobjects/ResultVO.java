package br.com.genericdata.valueobjects;

import java.io.Serializable;
import java.util.List;

public class ResultVO<T> implements Serializable {

	public static final int ERROR   = 0;
	public static final int SUCCESS = 1;
	
	private static final long serialVersionUID = -5038306618563701187L;
	
	private int                 status;
	private List< ValidatorVO > validators;
	private T                   result;
    private List<T>             resultList;
    private Object              data;

	public ResultVO (){
		this.status = SUCCESS;
	}
	
	public ResultVO(int status, List<ValidatorVO> validators,
			T result, List<T> resultList) {
		super();
		this.status     = status;
		this.validators = validators;
		this.result     = result;
		this.resultList = resultList;
	}
	
	public ResultVO(int status, List<ValidatorVO> validators, T result) {
		super();
		this.status     = status;
		this.validators = validators;
        this.result     = result;
		this.resultList = null;
	}
	
	public ResultVO(int status, List<ValidatorVO> validators, List<T> resultList) {
		super();
		this.status     = status;
		this.validators = validators;
		this.result     = null;
		this.resultList = resultList;
	}

	public int getStatus() {
		return status;
	}

	public List<ValidatorVO> getValidators() {
		return validators;
	}

	public T getResult() {
		return result;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setValidators(List<ValidatorVO> validators) {
		this.validators = validators;
	}

	public void setResult(T result) {
		this.result = result;
	}
	
	public List<T> getResultList() {
		return resultList;
	}

	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
	public String toString() {
		return "ResultVO [ " + result + " " + this.status + " " + validators + " ]"; 
	}
	
}
