package br.com.genericdata.valueobjects;

import java.io.Serializable;

public class WhoisVO implements Serializable {

	private static final long serialVersionUID = 7953829593369127355L;

	private String applicationCode;
	private String deviceUniqueIdentifier;
	private Long   accountId;
	private Long   contentId;
	private String componentCode;
	private String languageCode;
	private Long   appLocalVersion;
	
	public WhoisVO () {}

	public WhoisVO ( String applicationCode, String deviceUniqueIdentifier, Long accountId, Long contentId, String componentCode, String languageCode ) {
		this.applicationCode        = applicationCode;
		this.deviceUniqueIdentifier = deviceUniqueIdentifier;
		this.accountId              = accountId;
		this.contentId              = contentId;
		this.componentCode          = componentCode;
		this.languageCode           = languageCode;
	}

	public static long getSerialversionuid () {
		return serialVersionUID;
	}

	public String getApplicationCode () {
		return applicationCode;
	}

	public String getDeviceUniqueIdentifier () {
		return deviceUniqueIdentifier;
	}

	public Long getAccountId () {
		return accountId;
	}

	public Long getContentId () {
		return contentId;
	}

	public String getComponentCode () {
		return componentCode;
	}

	public String getLanguageCode () {
		return languageCode;
	}

	public Long getAppLocalVersion() {
		return appLocalVersion;
	}

	public void setAppLocalVersion(Long appLocalVersion) {
		this.appLocalVersion = appLocalVersion;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public void setDeviceUniqueIdentifier(String deviceUniqueIdentifier) {
		this.deviceUniqueIdentifier = deviceUniqueIdentifier;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}

	public void setComponentCode(String componentCode) {
		this.componentCode = componentCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
}