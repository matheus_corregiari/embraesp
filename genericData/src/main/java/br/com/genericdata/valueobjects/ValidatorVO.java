package br.com.genericdata.valueobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValidatorVO implements Serializable{

	private static final long serialVersionUID = -7635396243058823050L;
	public static final int REQUIRED = 1;
	public static final int NOT_REQUIRED = 0;
	
	private String key;	
	private String message;
	private int    required;
	
	public ValidatorVO () {}
	
	public ValidatorVO ( String campo, String message, int required ){
		this.key      = campo;
		this.message  = message;
		this.required = required;
	}
	
	public ValidatorVO ( String campo, String message ){
		this.key      = campo;
		this.message  = message;
		this.required = REQUIRED;
	}	

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getRequired() {
		return required;
	}

	public void setRequired(int required) {
		this.required = required;
	}

	public static List< ValidatorVO > newList ( ValidatorVO... validator ) {		
		return Arrays.asList(validator);
	}
	
	public static List< ValidatorVO > newList () {		
		return new ArrayList<ValidatorVO>();
	}	
	
}
