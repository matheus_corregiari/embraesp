package br.com.genericdata.valueobjects;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class GenericDataVO implements Serializable, Comparable< GenericDataVO >{

	private static final long serialVersionUID = -2284519487430611846L;

	public final static String DATAKEY = GenericDataVO.class.getSimpleName();
	
	private String token;
	private Date   creationDate;
	private Date   lastUpdateDate;
	private Object data;
	private String serviceName;
    private String connectionType;
	
	public GenericDataVO(){
		this.token          = RandomStringUtils.random(32, true, true);
		this.creationDate   = null;
		this.lastUpdateDate = null;
		this.data           = null;
		this.serviceName    = "";
        this.connectionType = "";
	}

    public GenericDataVO(Object data, String serviceName, String connectionType) {
        this.token          = RandomStringUtils.random(32, true, true);
        this.creationDate   = Calendar.getInstance().getTime();
        this.lastUpdateDate = Calendar.getInstance().getTime();
        this.data           = data;
        this.serviceName    = serviceName;
        this.connectionType = connectionType;
    }

    public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    @Override
	public int compareTo(GenericDataVO another) {
		
		if ( this.serviceName == null || another.serviceName == null || this.serviceName == null || another.serviceName == null ){
			return -1;
		}
		
		Integer serviceName = this.serviceName.compareTo(another.serviceName) * -1;
		
		return serviceName;
	}

}
