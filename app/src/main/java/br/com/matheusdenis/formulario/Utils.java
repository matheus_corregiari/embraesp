package br.com.matheusdenis.formulario;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;

import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;

/**
 * Created by matheus on 27/08/2014.
 */
public class Utils {

    public static int indexOf(List<BasicValueObject> basicValueObjects, BasicValueObject basicValueObject){
        if(basicValueObjects == null || basicValueObjects.isEmpty()){
            return -1;
        }

        if(basicValueObject == null){
            return -1;
        }

        for(int i = 0; i < basicValueObjects.size(); i++){

            if(basicValueObject.getId() != 0){
                if(basicValueObjects.get(i).getId() == basicValueObject.getId()){
                    return i;
                }
            }else{
                if(basicValueObjects.get(i).getIdServer() == basicValueObject.getIdServer()){
                    return i;
                }
            }

        }

        return -1;
    }

    public static void sendBroadcastToUpdateForm(final Context context, final DropDownItemVO dropDownItemVO){

        if(dropDownItemVO == null || context == null){
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean hideTerreno         = false;
                boolean hideComercializacao = false;
                boolean hideUnidade         = false;
                boolean hideEndereco        = false;
                boolean hideEdificacao      = false;

                if(dropDownItemVO.getCode().equals(DropDownKey.TIPO_IMOVEL.name())){
                    String value = dropDownItemVO.getValue();
                    if(value != null){
                        if(value.equals("Terreno")){
                            hideTerreno         = false;
                            hideComercializacao = false;
                            hideUnidade         = false;
                            hideEndereco        = false;
                            hideEdificacao      = true;
                        }else{
                            if(value.equals("Chácara")){
                                hideTerreno         = false;
                                hideComercializacao = false;
                                hideUnidade         = false;
                                hideEndereco        = false;
                                hideEdificacao      = false;
                            }else{
                                if(value.equals("Casa")){
                                    hideTerreno         = false;
                                    hideComercializacao = false;
                                    hideUnidade         = false;
                                    hideEndereco        = false;
                                    hideEdificacao      = false;
                                }else{
                                    if(value.equals("Apartamento")){
                                        hideTerreno         = true;
                                        hideComercializacao = false;
                                        hideUnidade         = false;
                                        hideEndereco        = false;
                                        hideEdificacao      = false;
                                    }else{
                                        if(value.equals("Vaga de Garagem")){
                                            hideTerreno         = true;
                                            hideComercializacao = false;
                                            hideUnidade         = false;
                                            hideEndereco        = false;
                                            hideEdificacao      = false;
                                        }else{
                                            if(value.equals("Conj. Comercial")){
                                                hideTerreno         = true;
                                                hideComercializacao = false;
                                                hideUnidade         = false;
                                                hideEndereco        = false;
                                                hideEdificacao      = false;
                                            }else{
                                                if(value.equals("Loja")){
                                                    hideTerreno         = false;
                                                    hideComercializacao = false;
                                                    hideUnidade         = false;
                                                    hideEndereco        = false;
                                                    hideEdificacao      = false;
                                                }else{
                                                    if(value.equals("Calpão / Armazém")){
                                                        hideTerreno         = false;
                                                        hideComercializacao = false;
                                                        hideUnidade         = false;
                                                        hideEndereco        = false;
                                                        hideEdificacao      = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                Intent intent = new Intent("BROADCAST_HIDE_FRAGMENTS");
                intent.putExtra("hideTerreno"        , hideTerreno);
                intent.putExtra("hideComercializacao", hideComercializacao);
                intent.putExtra("hideUnidade"        , hideUnidade);
                intent.putExtra("hideEndereco"       , hideEndereco);
                intent.putExtra("hideEdificacao"     , hideEdificacao);

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        }).start();

    }













    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


}
