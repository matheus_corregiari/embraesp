package br.com.matheusdenis.formulario;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.LocalCoreDataDAO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentImagem extends DataBaseCoreFragment implements View.OnClickListener {

    private ImagemVO imagemVO;

    private View img;
    private View progress;
    private View error;

    private ImageView image;
    private boolean useClick;
    private ArrayList<ImagemVO> imagemVOs;

    public static FragmentImagem newInstance(ImagemVO imagemVO, List<ImagemVO> imagemVOs, boolean useClick){

        if(imagemVO == null){
            imagemVO = new ImagemVO();
        }

        if(imagemVOs == null){
            imagemVOs = new ArrayList<ImagemVO>();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ImagemVO.DATAKEY, imagemVO);
        bundle.putSerializable(ImagemVO.DATAKEY+"2", new SerializableBundle<ImagemVO>(imagemVOs));
        bundle.putBoolean("USE_CLICK", useClick);
        FragmentImagem fragmentFromImagem = new FragmentImagem();
        fragmentFromImagem.setArguments(bundle);
        return fragmentFromImagem;

    }

    @Override
    public void save() {}

    @Override
    protected void disableElements() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setUseSaveBroadcast(false);
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.imagemVO = (ImagemVO)savedInstanceState.getSerializable(ImagemVO.DATAKEY);
            this.imagemVOs = ((SerializableBundle<ImagemVO>)savedInstanceState.getSerializable(ImagemVO.DATAKEY+"2")).getList();
            this.useClick = savedInstanceState.getBoolean("USE_CLICK", false);
        }else{
            this.imagemVO = (ImagemVO)getArguments().getSerializable(ImagemVO.DATAKEY);
            this.imagemVOs = ((SerializableBundle<ImagemVO>)getArguments().getSerializable(ImagemVO.DATAKEY+"2")).getList();
            this.useClick = getArguments().getBoolean("USE_CLICK", false);
        }

        if(this.imagemVO == null){
            this.imagemVO = new ImagemVO();
        }

        if(this.imagemVOs == null){
            this.imagemVOs = new ArrayList<ImagemVO>();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImagemVO.DATAKEY, this.imagemVO);
        outState.putSerializable(ImagemVO.DATAKEY + "2", new SerializableBundle<ImagemVO>(this.imagemVOs));
        outState.putBoolean("USE_CLICK", this.useClick);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_imagens, null);
        this.initializeVariables(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.updateUI(true);
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
        }

        img.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
        error.setVisibility(View.GONE);

        if(this.imagemVO.getImagePackage() != null && !this.imagemVO.getImagePackage().isEmpty()){
            byte[] bytes = (byte[]) LocalCoreDataDAO.get(getActivity(), this.imagemVO.getImagePackage());
            if(bytes != null && bytes.length > 0){
                this.imagemVO.setImageBytes(bytes);
            }
        }

        if(this.imagemVO.getImageBytes() != null && this.imagemVO.getImageBytes().length > 0){
            Bitmap bitmap = BitmapFactory.decodeByteArray(this.imagemVO.getImageBytes(), 0, this.imagemVO.getImageBytes().length);
            this.image.setMinimumHeight(1000);
            this.image.setMinimumWidth(1000);

            if(this.useClick){
                this.image.setOnClickListener(this);
            }else{
                this.image.setOnClickListener(null);
            }

            this.image.setImageBitmap(bitmap);

            img.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            error.setVisibility(View.GONE);


        }else{
            this.image.setImageResource(R.drawable.sem_imagem_peq);
            this.image.setMinimumHeight(150);
            this.image.setMinimumWidth(150);
            this.image.setMaxHeight(150);
            this.image.setMaxWidth(150);

            img.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            error.setVisibility(View.GONE);

        }

        if(this.imagemVO.getImageUrl() != null && !this.imagemVO.getImageUrl().isEmpty()){
            Picasso.with(getActivity()).load(this.imagemVO.getImageUrl()).into(this.image);
            if(this.useClick){
                this.image.setOnClickListener(this);
            }else{
                this.image.setOnClickListener(null);
            }

            img.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            error.setVisibility(View.GONE);

        }else{
            if(this.imagemVO.getImagePath() != null && !this.imagemVO.getImagePath().isEmpty()){
                Bitmap bitmap = BitmapFactory.decodeFile(this.imagemVO.getImagePath());
                this.image.setImageBitmap(bitmap);
                if(this.useClick){
                    this.image.setOnClickListener(this);
                }else{
                    this.image.setOnClickListener(null);
                }

                img.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                error.setVisibility(View.GONE);

            }
        }

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.formImagemImageView:
                Intent intent = new Intent(this.getActivity(), ActivityImagens.class);
                intent.putExtra(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(imagemVOs));
                startActivityForResult(intent, 9182);
                break;
        }
    }

    private void initializeVariables(View view){

        if(view == null){
            return;
        }
        this.image    = (ImageView) view.findViewById(R.id.formImagemImageView);
        this.img      = view.findViewById(R.id.img);
        this.progress = view.findViewById(R.id.progress);
        this.error    = view.findViewById(R.id.erro);
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
