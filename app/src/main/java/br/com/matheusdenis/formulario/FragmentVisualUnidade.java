package br.com.matheusdenis.formulario;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.UnidadeManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.UnidadeVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentVisualUnidade extends DataBaseCoreFragment implements AdapterView.OnItemSelectedListener, RadioGroup.OnCheckedChangeListener {

    private UnidadeVO unidadeVO;

    private TextView formUnidadeCodigoEmbraesp;
    private TextView formUnidadeNomeUsuario;
    private RadioGroup formUnidadeRadioGroupInformacaoAuditada;
    private Spinner  formUnidadeSpinnerDadoOrigem;

    private NumberPickerBuilder numberPickerDialog;

    private List<String> mInformacaoAuditada;
    private List<BasicValueObject> dadoOrigem;

    public static FragmentVisualUnidade newInstance(UnidadeVO unidadeVO){

        if(unidadeVO == null){
            unidadeVO = new UnidadeVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(UnidadeVO.DATAKEY, unidadeVO);
        FragmentVisualUnidade fragmentFromUnidade = new FragmentVisualUnidade();
        fragmentFromUnidade.setArguments(bundle);
        return fragmentFromUnidade;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setUseSaveBroadcast(false);
        this.setDataBaseManagement(new UnidadeManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        this.mInformacaoAuditada = new ArrayList<String>();

        String[] mInformacaoAuditada = this.getResources().getStringArray(R.array.InformacoesAuditadas);
        if(mInformacaoAuditada != null) {
            for (int i = 0; i < mInformacaoAuditada.length; i++) {
                this.mInformacaoAuditada.add(mInformacaoAuditada[i]);
            }
        }

        if(savedInstanceState != null){
            this.unidadeVO = (UnidadeVO) savedInstanceState.getSerializable(UnidadeVO.DATAKEY);
            this.dadoOrigem = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.DADO_ORIGEM.name())).getList();

            if(this.unidadeVO == null){
                this.unidadeVO = new UnidadeVO();
            }

            if(this.dadoOrigem == null){
                this.dadoOrigem = new ArrayList<BasicValueObject>();
            }

        }else{
            this.unidadeVO = (UnidadeVO) getArguments().getSerializable(UnidadeVO.DATAKEY);
            this.dadoOrigem = new ArrayList<BasicValueObject>();
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.DADO_ORIGEM.name());
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(UnidadeVO.DATAKEY, this.unidadeVO);
        outState.putSerializable(DropDownKey.DADO_ORIGEM    .name(), new SerializableBundle<BasicValueObject>(this.dadoOrigem));
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visual_unidade, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {


            this.formUnidadeCodigoEmbraesp    .setText(this.unidadeVO.getCodigoEmbraesp() == null ? "" : this.unidadeVO.getCodigoEmbraesp().toUpperCase());
            this.formUnidadeNomeUsuario       .setText(this.unidadeVO.getNomeUsuario() == null ? "" : this.unidadeVO.getNomeUsuario().toUpperCase());
        }

        int k = Utils.indexOf(dadoOrigem, this.unidadeVO.getDadoOrigem());
        k = k < 0? 0:k;

        this.formUnidadeSpinnerDadoOrigem.setOnItemSelectedListener(null);
        this.formUnidadeSpinnerDadoOrigem.setSelection(k);
        this.formUnidadeSpinnerDadoOrigem.setOnItemSelectedListener(this);

        int i = this.unidadeVO.getInformacaoAuditada() != null? this.unidadeVO.getInformacaoAuditada().equals("Sim")? R.id.formUnidadeRadioButtonSim:R.id.formUnidadeRadioButtonNao:R.id.formUnidadeRadioButtonNao;

        this.formUnidadeRadioGroupInformacaoAuditada.check(i);

    }

    private void initializeVariables(View view){

        if(view == null){
            return;
        }

        this.formUnidadeCodigoEmbraesp       = (TextView) view.findViewById(R.id.formUnidadeCodigoEmbraesp);
        this.formUnidadeNomeUsuario          = (TextView) view.findViewById(R.id.formUnidadeNomeUsuario);
        this.formUnidadeSpinnerDadoOrigem    = (Spinner)  view.findViewById(R.id.formUnidadeSpinnerDadoOrigem);
        this.formUnidadeRadioGroupInformacaoAuditada = (RadioGroup) view.findViewById(R.id.formUnidadeRadioGroupInformacaoAuditada);

        //view.findViewById(R.id.formUnidadeCodigoEmbraespLabel).setVisibility(this.unidadeVO.getCodigoEmbraesp() == null || this.unidadeVO.getCodigoEmbraesp().equals("") ? View.GONE : View.VISIBLE);
        this.formUnidadeCodigoEmbraesp.setVisibility(this.unidadeVO.getCodigoEmbraesp() == null || this.unidadeVO.getCodigoEmbraesp().equals("") ? View.GONE : View.VISIBLE);
        //view.findViewById(R.id.formUnidadeNomeUsuarioLabel).setVisibility(this.unidadeVO.getNomeUsuario() == null || this.unidadeVO.getNomeUsuario().equals("") ? View.GONE : View.VISIBLE);
        formUnidadeNomeUsuario.setVisibility(this.unidadeVO.getNomeUsuario() == null || this.unidadeVO.getNomeUsuario().equals("") ? View.GONE : View.VISIBLE);

        this.formUnidadeSpinnerDadoOrigem.setEnabled(false);
        this.formUnidadeSpinnerDadoOrigem.setClickable(false);

        if(this.unidadeVO == null){
            this.unidadeVO = new UnidadeVO();
        }

        if(this.dadoOrigem == null){
            this.dadoOrigem = new ArrayList<BasicValueObject>();
        }

        this.formUnidadeSpinnerDadoOrigem.setAdapter(new BasicSpinnerAdapter(getActivity(), dadoOrigem));
        this.formUnidadeRadioGroupInformacaoAuditada.setOnCheckedChangeListener(this);

        this.unidadeVO.setNomeUsuario(getString(R.string.user));
        this.formUnidadeNomeUsuario.setEnabled(false);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch(adapterView.getId()){
            case R.id.formUnidadeSpinnerDadoOrigem:
                if(this.dadoOrigem == null || this.dadoOrigem.isEmpty()){
                    return;
                }
                this.unidadeVO.setDadoOrigem((DropDownItemVO) this.dadoOrigem.get(i));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void save() {

        this.unidadeVO.setCodigoEmbraesp(this.formUnidadeCodigoEmbraesp.getText().toString() == null ? "" : this.formUnidadeCodigoEmbraesp.getText().toString());
        this.unidadeVO.setNomeUsuario       (this.formUnidadeNomeUsuario       .getText().toString() == null? "" : this.formUnidadeNomeUsuario       .getText().toString());

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "UNIDADE");
        intent.putExtra("RESULT_DATA", this.unidadeVO);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);

        if(method != null){
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.DADO_ORIGEM.name())){
                this.dadoOrigem = (List<BasicValueObject>) object;
                this.formUnidadeSpinnerDadoOrigem.setAdapter(new BasicSpinnerAdapter(getActivity(), dadoOrigem));
                updateUI(false);
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch(i){
            case R.id.formUnidadeRadioButtonNao:
                this.unidadeVO.setInformacaoAuditada("Não");
            break;

            case R.id.formUnidadeRadioButtonSim:
                this.unidadeVO.setInformacaoAuditada("Sim");
                break;

        }
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
