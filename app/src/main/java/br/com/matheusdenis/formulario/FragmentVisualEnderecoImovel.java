package br.com.matheusdenis.formulario;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.EnderecoImovelManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentVisualEnderecoImovel extends DataBaseCoreFragment implements RadioGroup.OnCheckedChangeListener, View.OnTouchListener, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private EnderecoImovelVO enderecoImovelVO;

    private TextView formEnderecoImovelLogradouro;
    private TextView formEnderecoImovelNumero;
    private TextView formEnderecoImovelCep;
    private TextView formEnderecoImovelBairro;
    private TextView formEnderecoImovelMunicipio;
    private TextView formEnderecoImovelLatitude;
    private TextView formEnderecoImovelLongitude;
    private RadioGroup formEnderecoImovelEnderecoAproximado;
    private Spinner    formEnderecoImovelUf;
    private Spinner    formEnderecoImovelSpinnerTipoImovel;
    //private View     formEnderecoImovelLogradouroLabel;
    //private View     formEnderecoImovelNumeroLabel;
    //private View     formEnderecoImovelCepLabel;
    //private View     formEnderecoImovelLatitudeLabel;
    //private View     formEnderecoImovelLongitudeLabel;

    private NumberPickerBuilder numberPickerDialog;

    private List<String> mUFs;
    private List<BasicValueObject> tipoImovel;

    public static FragmentVisualEnderecoImovel newInstance(EnderecoImovelVO enderecoImovelVO){

        if(enderecoImovelVO == null){
            enderecoImovelVO = new EnderecoImovelVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(EnderecoImovelVO.DATAKEY, enderecoImovelVO);
        FragmentVisualEnderecoImovel fragmentFromEnderecoImovel = new FragmentVisualEnderecoImovel();
        fragmentFromEnderecoImovel.setArguments(bundle);
        return fragmentFromEnderecoImovel;

    }

    @Override
    protected void disableElements() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setUseSaveBroadcast(false);
        this.setDataBaseManagement(new EnderecoImovelManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        this.mUFs = new ArrayList<String>();

        String[] UFs = this.getResources().getStringArray(R.array.UFs);
        if(UFs != null) {
            for (int i = 0; i < UFs.length; i++) {
                this.mUFs.add(UFs[i]);
            }
        }

        if(savedInstanceState != null){
            this.enderecoImovelVO = (EnderecoImovelVO) savedInstanceState.getSerializable(EnderecoImovelVO.DATAKEY);
            if(this.enderecoImovelVO.getTipoImovel() != null){
                Utils.sendBroadcastToUpdateForm(this.getActivity(), this.enderecoImovelVO.getTipoImovel());
            }

            this.tipoImovel = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.TIPO_IMOVEL.name())).getList();

            if(this.enderecoImovelVO == null){
                this.enderecoImovelVO = new EnderecoImovelVO();
            }

            if(this.tipoImovel == null){
                this.tipoImovel = new ArrayList<BasicValueObject>();
            }

        }else{
            this.enderecoImovelVO = (EnderecoImovelVO) getArguments().getSerializable(EnderecoImovelVO.DATAKEY);
            if(this.enderecoImovelVO.getTipoImovel() != null){
                Utils.sendBroadcastToUpdateForm(this.getActivity(), this.enderecoImovelVO.getTipoImovel());
            }
            this.tipoImovel = new ArrayList<BasicValueObject>();
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.TIPO_IMOVEL.name());
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(EnderecoImovelVO.DATAKEY, this.enderecoImovelVO);
        outState.putSerializable(DropDownKey.TIPO_IMOVEL.name(), new SerializableBundle<BasicValueObject>(this.tipoImovel));
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visual_endereco, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
            this.formEnderecoImovelLogradouro.setText(this.enderecoImovelVO.getLogradouro() == null ? "" : this.enderecoImovelVO.getLogradouro().toUpperCase());
            this.formEnderecoImovelNumero    .setText(this.enderecoImovelVO.getNumero() == null ? "" : this.enderecoImovelVO.getNumero().toUpperCase());
            this.formEnderecoImovelCep       .setText(this.enderecoImovelVO.getCep() == null ? "" : this.enderecoImovelVO.getCep().toUpperCase());
            this.formEnderecoImovelBairro    .setText(this.enderecoImovelVO.getBairro() == null ? "" : this.enderecoImovelVO.getBairro().toUpperCase());
            this.formEnderecoImovelMunicipio .setText(this.enderecoImovelVO.getMunicipio() == null ? "" : this.enderecoImovelVO.getMunicipio().toUpperCase());
        }

        this.formEnderecoImovelLatitude.setText(String.format("%.5f", this.enderecoImovelVO.getLatitude()));
        this.formEnderecoImovelLatitude.setTag(this.enderecoImovelVO.getLatitude());
        this.formEnderecoImovelLongitude.setText(String.format("%.5f", this.enderecoImovelVO.getLongitude()));
        this.formEnderecoImovelLongitude.setTag(this.enderecoImovelVO.getLongitude());

        this.formEnderecoImovelEnderecoAproximado.check(this.enderecoImovelVO.isEnderecoAproximado() ?
                R.id.formEnderecoImovelAproximado : R.id.formEnderecoImovelPreciso);

        int i = this.mUFs.indexOf(this.enderecoImovelVO.getUF());
        i = i < 0? 0:i;
        this.formEnderecoImovelUf.setSelection(i);

        i = Utils.indexOf(tipoImovel, this.enderecoImovelVO.getTipoImovel());
        i = i < 0? 0:i;

        this.formEnderecoImovelSpinnerTipoImovel.setOnItemSelectedListener(this);
        this.formEnderecoImovelSpinnerTipoImovel.setSelection(i);

        if(this.enderecoImovelVO.isEnderecoAproximado()) {

            this.formEnderecoImovelLogradouro.setVisibility(View.GONE);
            this.formEnderecoImovelNumero    .setVisibility(View.GONE);
            this.formEnderecoImovelCep       .setVisibility(View.GONE);
            this.formEnderecoImovelLatitude  .setVisibility(View.GONE);
            this.formEnderecoImovelLongitude .setVisibility(View.GONE);

            /*
            this.formEnderecoImovelLogradouroLabel.setVisibility(View.GONE);
            this.formEnderecoImovelNumeroLabel    .setVisibility(View.GONE);
            this.formEnderecoImovelCepLabel       .setVisibility(View.GONE);
            this.formEnderecoImovelLatitudeLabel  .setVisibility(View.GONE);
            this.formEnderecoImovelLongitudeLabel .setVisibility(View.GONE);
            */

        }else{

            this.formEnderecoImovelLogradouro.setVisibility(View.VISIBLE);
            this.formEnderecoImovelNumero    .setVisibility(View.VISIBLE);
            this.formEnderecoImovelCep       .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLatitude  .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLongitude .setVisibility(View.VISIBLE);

            /*
            this.formEnderecoImovelLogradouroLabel.setVisibility(View.VISIBLE);
            this.formEnderecoImovelNumeroLabel    .setVisibility(View.VISIBLE);
            this.formEnderecoImovelCepLabel       .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLatitudeLabel  .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLongitudeLabel .setVisibility(View.VISIBLE);
            */

        }

    }

    private void initializeVariables(View view){

        if(view == null){
            return;
        }

        this.formEnderecoImovelLogradouro         = (TextView) view.findViewById(R.id.formEnderecoImovelLogradouro);
        this.formEnderecoImovelNumero             = (TextView) view.findViewById(R.id.formEnderecoImovelNumero);
        this.formEnderecoImovelCep                = (TextView) view.findViewById(R.id.formEnderecoImovelCep);
        this.formEnderecoImovelBairro             = (TextView) view.findViewById(R.id.formEnderecoImovelBairro);
        this.formEnderecoImovelMunicipio          = (TextView) view.findViewById(R.id.formEnderecoImovelMunicipio);
        this.formEnderecoImovelLatitude           = (TextView) view.findViewById(R.id.formEnderecoImovelLatitude);
        this.formEnderecoImovelLongitude          = (TextView) view.findViewById(R.id.formEnderecoImovelLongitude);
        this.formEnderecoImovelEnderecoAproximado = (RadioGroup) view.findViewById(R.id.formEnderecoImovelEnderecoAproximado);
        this.formEnderecoImovelUf                 = (Spinner)    view.findViewById(R.id.formEnderecoImovelUF);
        this.formEnderecoImovelSpinnerTipoImovel  = (Spinner)    view.findViewById(R.id.formEnderecoImovelSpinnerTipoImovel);

        this.formEnderecoImovelUf.setEnabled(false);
        this.formEnderecoImovelUf.setClickable(false);

        this.formEnderecoImovelSpinnerTipoImovel.setEnabled(false);
        this.formEnderecoImovelSpinnerTipoImovel.setClickable(false);

        /*
        this.formEnderecoImovelLogradouroLabel = view.findViewById(R.id.formEnderecoImovelLogradouroLabel);
        this.formEnderecoImovelNumeroLabel     = view.findViewById(R.id.formEnderecoImovelNumeroLabel);
        this.formEnderecoImovelCepLabel        = view.findViewById(R.id.formEnderecoImovelCepLabel);
        this.formEnderecoImovelLatitudeLabel   = view.findViewById(R.id.formEnderecoImovelLatitudeLabel);
        this.formEnderecoImovelLongitudeLabel  = view.findViewById(R.id.formEnderecoImovelLongitudeLabel);
        */

        this.formEnderecoImovelLatitude .setEnabled(false);
        this.formEnderecoImovelLongitude.setEnabled(false);
        this.formEnderecoImovelEnderecoAproximado.setOnCheckedChangeListener(this);

        this.formEnderecoImovelUf.setAdapter(new BasicSpinnerAdapter(getActivity(), mUFs));
        this.formEnderecoImovelUf.setOnItemSelectedListener(this);

        this.formEnderecoImovelSpinnerTipoImovel.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoImovel));

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch(i){
            case R.id.formEnderecoImovelAproximado:
                this.enderecoImovelVO.setEnderecoAproximado(true);

                this.formEnderecoImovelLogradouro.setVisibility(View.GONE);
                this.formEnderecoImovelNumero    .setVisibility(View.GONE);
                this.formEnderecoImovelCep       .setVisibility(View.GONE);
                this.formEnderecoImovelLatitude  .setVisibility(View.GONE);
                this.formEnderecoImovelLongitude .setVisibility(View.GONE);

                /*
                this.formEnderecoImovelLogradouroLabel.setVisibility(View.GONE);
                this.formEnderecoImovelNumeroLabel    .setVisibility(View.GONE);
                this.formEnderecoImovelCepLabel       .setVisibility(View.GONE);
                this.formEnderecoImovelLatitudeLabel  .setVisibility(View.GONE);
                this.formEnderecoImovelLongitudeLabel .setVisibility(View.GONE);
                */

                break;
            case R.id.formEnderecoImovelPreciso:
                this.enderecoImovelVO.setEnderecoAproximado(false);

                this.formEnderecoImovelLogradouro.setVisibility(View.VISIBLE);
                this.formEnderecoImovelNumero    .setVisibility(View.VISIBLE);
                this.formEnderecoImovelCep       .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLatitude  .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLongitude .setVisibility(View.VISIBLE);

                /*
                this.formEnderecoImovelLogradouroLabel.setVisibility(View.VISIBLE);
                this.formEnderecoImovelNumeroLabel    .setVisibility(View.VISIBLE);
                this.formEnderecoImovelCepLabel       .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLatitudeLabel  .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLongitudeLabel .setVisibility(View.VISIBLE);
                */

                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {

        if((v.getId() != R.id.formEnderecoImovelLatitude
                && v.getId() != R.id.formEnderecoImovelLongitude) || motionEvent.getAction() != 0 ){
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch(adapterView.getId()) {
            case R.id.formEnderecoImovelSpinnerTipoImovel:
                if (this.tipoImovel == null || this.tipoImovel.isEmpty()) {
                    return;
                }
                this.enderecoImovelVO.setTipoImovel((DropDownItemVO) this.tipoImovel.get(i));
                Utils.sendBroadcastToUpdateForm(this.getActivity(), (DropDownItemVO) this.tipoImovel.get(i));
                break;
            case R.id.formEnderecoImovelUF:
                this.enderecoImovelVO.setUF(mUFs.get(i));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}


    @Override
    public void save() {
        this.enderecoImovelVO.setLogradouro(this.formEnderecoImovelLogradouro.getText().toString());
        this.enderecoImovelVO.setNumero(this.formEnderecoImovelNumero.getText().toString());
        this.enderecoImovelVO.setCep(this.formEnderecoImovelCep.getText().toString());
        this.enderecoImovelVO.setBairro(this.formEnderecoImovelBairro.getText().toString());
        this.enderecoImovelVO.setMunicipio(this.formEnderecoImovelMunicipio.getText().toString());

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "ENDERECO_IMOVEL");
        intent.putExtra("RESULT_DATA", this.enderecoImovelVO);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);

        if(method != null){
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.TIPO_IMOVEL.name())){
                this.tipoImovel = (List<BasicValueObject>) object;
                this.formEnderecoImovelSpinnerTipoImovel.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoImovel));
                updateUI(false);
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
