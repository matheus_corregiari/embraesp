package br.com.matheusdenis.formulario;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.marvinlabs.widget.floatinglabel.edittext.FloatingLabelEditText;

import java.util.List;

import br.com.database.Activity.DataBaseCoreActivity;
import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.LocalCoreDataDAO;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.genericdata.appmsg.AppMsg;
import br.com.genericdata.valueobjects.ValidatorVO;
import br.com.matheusdenis.formulario.ValueObjects.Usuario;

public class ActivityLogin extends DataBaseCoreActivity {

    private Usuario usuario;
    private FloatingLabelEditText username;
    private FloatingLabelEditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setDataBaseManagement(new DropDownItemManagement(this, this));
        super.onCreate(savedInstanceState);

        Crashlytics.start(this);
        this.getActionBar().setTitle("");
        this.getActionBar().setIcon(R.drawable.ic_launcher2);
        setContentView(R.layout.activity_login);

        usuario = (Usuario) LocalCoreDataDAO.get(this,"usuarioLoginOPASUIDHPOSUFHdfjghdifgh");
        if(usuario != null){
            if(!isNetworkConnected()){
                Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                startActivity(intent);
                finish();
            }else{
                usuario = new ServerConnectManager(this, this).login(usuario.getUser(), usuario.getPassword());
            }
        }

        CardView cardView = (CardView) this.findViewById(R.id.activityLoginLoginButton);

        username = (FloatingLabelEditText) findViewById(R.id.activityLoginEmailEditText);
        password = (FloatingLabelEditText) findViewById(R.id.activityLoginPasswordEditText);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkConnected()){

                    if(username == null){
                        return;
                    }

                    if(password == null){
                        return;
                    }

                    usuario = new ServerConnectManager(getApplicationContext(), ActivityLogin.this).login(username.getInputWidgetText().toString(), password.getInputWidgetText().toString());
                }
            }
        });

        this.sendToDataBase(MethodTag.GETALL);

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }

    public void putData(){

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.TIPO_IMOVEL.name(),"Selecione o Tipo Imóvel", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(7L,"", DropDownKey.TIPO_IMOVEL.name(),"Terreno"        , null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.TIPO_IMOVEL.name(),"Chácara"        , null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.TIPO_IMOVEL.name(),"Casa"           , null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(6L,"", DropDownKey.TIPO_IMOVEL.name(),"Loja"           , null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(5L,"", DropDownKey.TIPO_IMOVEL.name(),"Galpão / Armazém", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.DADO_ORIGEM.name(),"Selecione o Dado Origem", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.DADO_ORIGEM.name(),"Embraesp / Emplasa", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.DADO_ORIGEM.name(),"Embraesp", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.TERRENO_POTENCIALIDADE.name(),"Selecione a potencialidade", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.TERRENO_POTENCIALIDADE.name(),"Dinâmica Metropolitana", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.TERRENO_POTENCIALIDADE.name(),"Urbanização Inclusiva", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.TERRENO_POTENCIALIDADE.name(),"Requalificação e Revitalização Urbana", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.TERRENO_POTENCIALIDADE.name(),"Remanegamento e Recuperação Urbana", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.EDIFICACAO_TIPO_ESTRUTURA.name(),"Selecione Tipo Estrutura", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.EDIFICACAO_TIPO_ESTRUTURA.name(),"Concreto Armado", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.EDIFICACAO_TIPO_ESTRUTURA.name(),"Alvenaria", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.EDIFICACAO_TIPO_ESTRUTURA.name(),"Metálica", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.EDIFICACAO_TIPO_ESTRUTURA.name(),"Pré-Moldado de concreto armado", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Selecione Tipo Cobertura", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas de Barro", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas de Fibrocimento", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas Sanduíche", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas de Zinco / Alumínio", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(5L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas Ecológicas", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(6L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas de Vidro", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(7L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas de Cimento", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(8L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Telhas Translúcidas", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(9L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Laje", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(10L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Laje Impermeabilizada", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(11L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Canaletões de Concreto Armado", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(12L,"", DropDownKey.EDIFICACAO_TIPO_COBERTURA.name(),"Canaletões Fibrocimento", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Selecione Tipo Edificação", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Unidade Duplex", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Unidade Triplex", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Cobertura", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(5L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Cobertura Duplex", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(6L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Cobertura Triplex", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(7L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Garden", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(8L,"", DropDownKey.EDIFICACAO_TIPO.name(),"Penthouse", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name(),"Selecione Padrão Econômico", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name(),"AAA", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name(),"A", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name(),"B", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name(),"C", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(5L,"", DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name(),"D", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name(),"Selecione Tipo Conservação", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name(),"Perfeito", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name(),"Bom", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name(),"Médio", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name(),"Ruim", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name(),"Selecione Tipo Venda", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name(),"Direto com Incorporador", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name(),"Direto com Proprietário", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name(),"Representante do Proprietário", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name(),"Imobiliária / Corretor", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name(),"Selecione Tipo Locação", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name(),"Residencial", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name(),"Comercial", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name(),"Temporada", null));

        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(0L,"", DropDownKey.COMERCIALIZACAO_FONTE.name(),"Selecione Fonte", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(1L,"", DropDownKey.COMERCIALIZACAO_FONTE.name(),"Placa", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(2L,"", DropDownKey.COMERCIALIZACAO_FONTE.name(),"Mídea Impressa", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(3L,"", DropDownKey.COMERCIALIZACAO_FONTE.name(),"Mídia Digital", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(4L,"", DropDownKey.COMERCIALIZACAO_FONTE.name(),"Imobiliária / Corretor", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(5L,"", DropDownKey.COMERCIALIZACAO_FONTE.name(),"Proprietário", null));
        this.sendToDataBase(MethodTag.SAVE, new DropDownItemVO(6L,"", DropDownKey.COMERCIALIZACAO_FONTE.name(),"Incorporador", null));
    }

    @Override
    public void onDataBaseResult(String method, Object object) {

        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.GETALL))){
                List<BasicValueObject> basicValueObjects = (List<BasicValueObject>) object;

                if(basicValueObjects == null || basicValueObjects.isEmpty()){
                    putData();
                }

            }
        }

    }

    @Override
    public void onDataBaseError(String method, Exception exception) {

        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.GETALL))){
            }
        }

    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {
        if(serviceName.equals(getString(R.string.login_soap_action))){
            if(result.equals("true")){
                LocalCoreDataDAO.save(this, usuario,"usuarioLoginOPASUIDHPOSUFHdfjghdifgh");
                Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                startActivity(intent);
                finish();
            }else{
                Toast.makeText(this,"Email ou senha incorretos!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {
        if(serviceName.equals(getString(R.string.login_soap_action))){
            Toast.makeText(this,"Ocorreu um erro na solicitação", Toast.LENGTH_SHORT).show();
        }
    }
}
