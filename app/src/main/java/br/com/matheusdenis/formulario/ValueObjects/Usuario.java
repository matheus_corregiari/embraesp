package br.com.matheusdenis.formulario.ValueObjects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Matheus on 22/10/2014.
 */
public class Usuario implements Serializable {

    @JsonProperty("user")
    private String user;
    @JsonProperty("password")
    private String password;

    public Usuario() {
    }

    public Usuario(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
