package br.com.matheusdenis.formulario;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.database.Activity.DataBaseCoreActivity;
import br.com.database.Enum.MethodTag;
import br.com.database.Management.ImovelManagement;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.genericdata.valueobjects.ValidatorVO;

public class ActivityImagens extends DataBaseCoreActivity {

    private ArrayList<ImagemVO> imagemVOs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_imagens);

        if(savedInstanceState != null){
            this.imagemVOs = ((SerializableBundle<ImagemVO>)savedInstanceState.getSerializable(ImagemVO.DATAKEY)).getList();
        }else{
            this.imagemVOs = ((SerializableBundle<ImagemVO>)getIntent().getExtras().getSerializable(ImagemVO.DATAKEY)).getList();
        }

        if(this.imagemVOs == null){
            this.imagemVOs = new ArrayList<ImagemVO>();
        }

        this.getSupportFragmentManager().beginTransaction().replace(R.id.places_imagens, FragmentFromImagem.newInstance(this.imagemVOs, false)).commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(this.imagemVOs));
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
