package br.com.matheusdenis.formulario;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.ImagemManagement;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentVisualImagem extends DataBaseCoreFragment {

    private List<ImagemVO> imagemVOs;
    private boolean useClick;

    private ViewPager viewPager;

    public static FragmentVisualImagem newInstance(List<ImagemVO> imagemVOs, boolean useClick){

        if(imagemVOs == null){
            imagemVOs = new ArrayList<ImagemVO>();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(imagemVOs));
        bundle.putBoolean("USE_CLICK", useClick);
        FragmentVisualImagem fragmentFromImagem = new FragmentVisualImagem();
        fragmentFromImagem.setArguments(bundle);
        return fragmentFromImagem;

    }

    @Override
    public void save() {

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "IMAGEM");
        intent.putExtra("RESULT_DATA", new SerializableBundle<ImagemVO>(this.imagemVOs));
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

    }

    @Override
    protected void disableElements() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setUseSaveBroadcast(false);
        this.setDataBaseManagement(new ImagemManagement(getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.imagemVOs = ((SerializableBundle<ImagemVO>)savedInstanceState.getSerializable(ImagemVO.DATAKEY)).getList();
            this.useClick = savedInstanceState.getBoolean("USE_CLICK", false);
        }else{
            this.imagemVOs = ((SerializableBundle<ImagemVO>)getArguments().getSerializable(ImagemVO.DATAKEY)).getList();
            this.useClick = getArguments().getBoolean("USE_CLICK", false);
        }

        if(this.imagemVOs == null){
            this.imagemVOs = new ArrayList<ImagemVO>();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(this.imagemVOs));
        outState.putBoolean("USE_CLICK", this.useClick);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visual_imagens, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
        }

        this.viewPager.setAdapter(new HomeAdapter(getFragmentManager()));
    }

    private void initializeVariables(View view){

        if(view == null){
            return;
        }

        this.viewPager    = (ViewPager) view.findViewById(R.id.formImagemViewPager);

        this.viewPager.setSaveEnabled(true);

    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.DELETE))){

                ImagemVO valueObject = (ImagemVO) object;

                for (Iterator<ImagemVO> iterator = imagemVOs.iterator(); iterator.hasNext();){
                    ImagemVO ImagemVO = iterator.next();
                    if(ImagemVO.getId() == valueObject.getId()){
                        iterator.remove();
                        break;
                    }
                }
                updateUI(false);
                Toast.makeText(getActivity(), "Imagem Excluída", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.DELETE))){
                Toast.makeText(getActivity(), "Não foi possível excluir a Imagem", Toast.LENGTH_SHORT).show();
                exception.printStackTrace();
                Log.wtf("DELETEE", exception.getMessage());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 9182 && resultCode == Activity.RESULT_OK){

            this.imagemVOs = ((SerializableBundle<ImagemVO>)data.getExtras().getSerializable(ImagemVO.DATAKEY)).getList();
            this.updateUI(false);

        }
    }

    @Override
    public void onPause() {
        Intent intent = new Intent();
        intent.putExtra(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(imagemVOs));
        getActivity().setResult(Activity.RESULT_OK, intent);
        super.onPause();
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }

    class HomeAdapter extends FragmentStatePagerAdapter {

        public HomeAdapter( FragmentManager fm ) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if(imagemVOs == null || imagemVOs.isEmpty()){
                return FragmentImagem.newInstance(new ImagemVO(), new ArrayList<ImagemVO>(), false);
            }

            return FragmentVisualDoisImagem.newInstance((ImagemVO) imagemVOs.get(position), imagemVOs,  useClick);
        }

        @SuppressLint("DefaultLocale")
        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        @Override
        public int getCount() {

            if(imagemVOs == null || imagemVOs.isEmpty()){
                return 1;
            }

            return imagemVOs.size();
        }

    }

}
