package br.com.matheusdenis.formulario;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.EnderecoImovelManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentFromEnderecoImovel extends DataBaseCoreFragment implements RadioGroup.OnCheckedChangeListener, View.OnTouchListener, View.OnClickListener, AdapterView.OnItemSelectedListener {

    public List<EnderecoImovelVO> enderecoImovelVOs;
    private View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {

            if(keyEvent.getAction() == KeyEvent.ACTION_UP) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.KEYCODE_TAB:
                    case KeyEvent.KEYCODE_ENTER:

                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        EnderecoImovelVO enderecoImovelVO = new EnderecoImovelVO();
                        enderecoImovelVO.setLogradouro(formEnderecoImovelLogradouro.getText().toString().toUpperCase());
                        enderecoImovelVO.setNumero(formEnderecoImovelNumero.getText().toString().toUpperCase());
                        enderecoImovelVO.setCep(formEnderecoImovelCep.getText().toString().toUpperCase());
                        enderecoImovelVO.setBairro(formEnderecoImovelBairro.getText().toString().toUpperCase());
                        enderecoImovelVO.setMunicipio(formEnderecoImovelMunicipio.getText().toString().toUpperCase());

                        Intent intent = new Intent("GETLATITUDEFROMANDDRESS");

                        if (view.getId() == R.id.formEnderecoImovelNumero) {
                            intent.putExtra("onlyLatLong", true);
                        }
                        if (view.getId() == R.id.formEnderecoImovelBairro) {
                            intent.putExtra("onlyLatLong", true);
                        }
                        intent.putExtra(EnderecoImovelVO.DATAKEY, enderecoImovelVO);

                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

                        return true;
                }
            }

            return false;
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent == null || intent.getExtras() == null){
                return;
            }

            EnderecoImovelVO enderecoImovelVO1 = (EnderecoImovelVO) intent.getExtras().get(EnderecoImovelVO.DATAKEY);

            boolean bool = intent.getExtras().getBoolean("BOOL", false);

            if(enderecoImovelVO1 == null){
                return;
            }

            enderecoImovelVO.setLogradouro(enderecoImovelVO1.getLogradouro());
            enderecoImovelVO.setNumero(enderecoImovelVO1.getNumero());
            enderecoImovelVO.setCep(enderecoImovelVO1.getCep());

            if(bool){
                enderecoImovelVO.setMunicipio(enderecoImovelVO1.getMunicipio());
                enderecoImovelVO.setUF(enderecoImovelVO1.getUF());
            }

            enderecoImovelVO.setBairro(enderecoImovelVO1.getBairro());
            enderecoImovelVO.setLatitude(enderecoImovelVO1.getLatitude());
            enderecoImovelVO.setLongitude(enderecoImovelVO1.getLongitude());
            enderecoImovelVO.setEnderecoAproximado(enderecoImovelVO1.isEnderecoAproximado());

            updateUI(true);
        }
    };

    private BroadcastReceiver broadcastReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            enderecoImovelVO.setLogradouro(formEnderecoImovelLogradouro.getText().toString().toUpperCase());
            enderecoImovelVO.setNumero(formEnderecoImovelNumero.getText().toString().toUpperCase());
            enderecoImovelVO.setCep(formEnderecoImovelCep.getText().toString().toUpperCase());
            enderecoImovelVO.setBairro(formEnderecoImovelBairro.getText().toString().toUpperCase());
            enderecoImovelVO.setMunicipio(formEnderecoImovelMunicipio.getText().toString().toUpperCase());

            if(!enderecoImovelVO.isEnderecoAproximado()){
                if(enderecoImovelVO.getLogradouro() == null || enderecoImovelVO.getLogradouro().equals("")){
                    Toast.makeText(context, "Preencha o Logradouro!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(enderecoImovelVO.getNumero() == null || enderecoImovelVO.getNumero().equals("")){
                    Toast.makeText(context, "Preencha o Número!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(enderecoImovelVO.getCep() == null || enderecoImovelVO.getCep().equals("")){
                    Toast.makeText(context, "Preencha o CEP!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }


            if(enderecoImovelVO.getBairro() == null || enderecoImovelVO.getBairro().equals("")){
                Toast.makeText(context, "Preencha o Bairro!", Toast.LENGTH_SHORT).show();
                return;
            }

            if(enderecoImovelVO.getMunicipio() == null || enderecoImovelVO.getMunicipio().equals("")){
                Toast.makeText(context, "Preencha o Município!", Toast.LENGTH_SHORT).show();
                return;
            }

            if(enderecoImovelVO.getUF() == null || enderecoImovelVO.getUF().equals("")){
                Toast.makeText(context, "Preencha o Estado!", Toast.LENGTH_SHORT).show();
                return;
            }

            if(enderecoImovelVO.getTipoImovel() == null || enderecoImovelVO.getTipoImovel().getIdServer() == 0L){
                Toast.makeText(context, "Preencha o Tipo do Imóvel!", Toast.LENGTH_SHORT).show();
                return ;
            }

            ImovelVO imovelVO = new ImovelVO();
            imovelVO.setEnderecoImovelVO(enderecoImovelVO);

            getFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentForm.newInstance(imovelVO)).commit();

        }
    };


    private EnderecoImovelVO enderecoImovelVO;

    private AutoCompleteTextView formEnderecoImovelLogradouro;
    private EditText formEnderecoImovelNumero;
    private EditText formEnderecoImovelCep;
    private EditText formEnderecoImovelBairro;
    private AutoCompleteTextView formEnderecoImovelMunicipio;
    private EditText formEnderecoImovelLatitude;
    private EditText formEnderecoImovelLongitude;
    private RadioGroup formEnderecoImovelEnderecoAproximado;
    private Spinner    formEnderecoImovelUf;
    private Spinner    formEnderecoImovelSpinnerTipoImovel;
    private View     formEnderecoImovelLogradouroLabel;
    private View     formEnderecoImovelNumeroLabel;
    private View     formEnderecoImovelCepLabel;
    private View     formEnderecoImovelLatitudeLabel;
    private View     formEnderecoImovelLongitudeLabel;

    private NumberPickerBuilder numberPickerDialog;

    private List<String> mUFs;
    private List<BasicValueObject> tipoImovel;
    private ArrayAdapter adapter;
    private ArrayAdapter adapterM;
    private ArrayList<EnderecoImovelVO> enderecoImovelVOs2;

    public static FragmentFromEnderecoImovel newInstance(EnderecoImovelVO enderecoImovelVO){

        if(enderecoImovelVO == null){
            enderecoImovelVO = new EnderecoImovelVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(EnderecoImovelVO.DATAKEY, enderecoImovelVO);
        FragmentFromEnderecoImovel fragmentFromEnderecoImovel = new FragmentFromEnderecoImovel();
        fragmentFromEnderecoImovel.setArguments(bundle);
        return fragmentFromEnderecoImovel;

    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver( broadcastReceiver, new IntentFilter("UPDATE_ENDERECO_VO"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver( broadcastReceiver2, new IntentFilter("GETIMOVELVO"));
        super.onResume();
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver2);
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setDataBaseManagement(new EnderecoImovelManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        this.mUFs = new ArrayList<String>();

        String[] UFs = this.getResources().getStringArray(R.array.UFs);
        if(UFs != null) {
            for (int i = 0; i < UFs.length; i++) {
                this.mUFs.add(UFs[i]);
            }
        }

        if(savedInstanceState != null){
            this.enderecoImovelVO = (EnderecoImovelVO) savedInstanceState.getSerializable(EnderecoImovelVO.DATAKEY);
            if(this.enderecoImovelVO.getTipoImovel() != null){
                Utils.sendBroadcastToUpdateForm(this.getActivity(), this.enderecoImovelVO.getTipoImovel());
            }

            this.tipoImovel = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.TIPO_IMOVEL.name())).getList();

            if(this.enderecoImovelVO == null){
                this.enderecoImovelVO = new EnderecoImovelVO();
            }

            if(this.tipoImovel == null){
                this.tipoImovel = new ArrayList<BasicValueObject>();
            }

        }else{
            this.enderecoImovelVO = (EnderecoImovelVO) getArguments().getSerializable(EnderecoImovelVO.DATAKEY);
            if(this.enderecoImovelVO.getTipoImovel() != null){
                Utils.sendBroadcastToUpdateForm(this.getActivity(), this.enderecoImovelVO.getTipoImovel());
            }
            this.tipoImovel = new ArrayList<BasicValueObject>();
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.TIPO_IMOVEL.name());
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(EnderecoImovelVO.DATAKEY, this.enderecoImovelVO);
        outState.putSerializable(DropDownKey.TIPO_IMOVEL.name(), new SerializableBundle<BasicValueObject>(this.tipoImovel));
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_endereco, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
            this.formEnderecoImovelLogradouro.setText(this.enderecoImovelVO.getLogradouro() == null ? "" : this.enderecoImovelVO.getLogradouro().toUpperCase());
            this.formEnderecoImovelNumero    .setText(this.enderecoImovelVO.getNumero() == null ? "" : this.enderecoImovelVO.getNumero().toUpperCase());
            this.formEnderecoImovelCep       .setText(this.enderecoImovelVO.getCep()        == null ? "" : this.enderecoImovelVO.getCep().toUpperCase());
            this.formEnderecoImovelBairro    .setText(this.enderecoImovelVO.getBairro()     == null ? "" : this.enderecoImovelVO.getBairro().toUpperCase());
            this.formEnderecoImovelMunicipio .setText(this.enderecoImovelVO.getMunicipio()  == null ? "" : this.enderecoImovelVO.getMunicipio().toUpperCase());
        }

        this.formEnderecoImovelLatitude.setText(String.format("%.5f", this.enderecoImovelVO.getLatitude()));
        this.formEnderecoImovelLatitude.setTag(this.enderecoImovelVO.getLatitude());
        this.formEnderecoImovelLongitude.setText(String.format("%.5f", this.enderecoImovelVO.getLongitude()));
        this.formEnderecoImovelLongitude.setTag(this.enderecoImovelVO.getLongitude());

        this.formEnderecoImovelEnderecoAproximado.check(this.enderecoImovelVO.isEnderecoAproximado() ?
                R.id.formEnderecoImovelAproximado : R.id.formEnderecoImovelPreciso);

        int i = this.mUFs.indexOf(this.enderecoImovelVO.getUF());
        i = i < 0? 0:i;
        this.formEnderecoImovelUf.setSelection(i);

        i = Utils.indexOf(tipoImovel, this.enderecoImovelVO.getTipoImovel());
        i = i < 0? 0:i;

        this.formEnderecoImovelSpinnerTipoImovel.setOnItemSelectedListener(this);
        this.formEnderecoImovelSpinnerTipoImovel.setSelection(i);

        if(this.enderecoImovelVO.isEnderecoAproximado()) {

            this.formEnderecoImovelLogradouro.setVisibility(View.GONE);
            this.formEnderecoImovelNumero    .setVisibility(View.GONE);
            this.formEnderecoImovelCep       .setVisibility(View.GONE);
            this.formEnderecoImovelLatitude  .setVisibility(View.GONE);
            this.formEnderecoImovelLongitude .setVisibility(View.GONE);

            this.formEnderecoImovelLogradouroLabel.setVisibility(View.GONE);
            this.formEnderecoImovelNumeroLabel    .setVisibility(View.GONE);
            this.formEnderecoImovelCepLabel       .setVisibility(View.GONE);
            this.formEnderecoImovelLatitudeLabel  .setVisibility(View.GONE);
            this.formEnderecoImovelLongitudeLabel .setVisibility(View.GONE);

        }else{

            this.formEnderecoImovelLogradouro.setVisibility(View.VISIBLE);
            this.formEnderecoImovelNumero    .setVisibility(View.VISIBLE);
            this.formEnderecoImovelCep       .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLatitude  .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLongitude .setVisibility(View.VISIBLE);

            this.formEnderecoImovelLogradouroLabel.setVisibility(View.VISIBLE);
            this.formEnderecoImovelNumeroLabel    .setVisibility(View.VISIBLE);
            this.formEnderecoImovelCepLabel       .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLatitudeLabel  .setVisibility(View.VISIBLE);
            this.formEnderecoImovelLongitudeLabel .setVisibility(View.VISIBLE);

        }

    }

    @SuppressLint("WrongViewCast")
    private void initializeVariables(View view){

        if(view == null){
            return;
        }

        this.formEnderecoImovelLogradouro         = (AutoCompleteTextView) view.findViewById(R.id.formEnderecoImovelLogradouro);
        this.formEnderecoImovelNumero             = (EditText) view.findViewById(R.id.formEnderecoImovelNumero);
        this.formEnderecoImovelCep                = (EditText) view.findViewById(R.id.formEnderecoImovelCep);
        this.formEnderecoImovelBairro             = (EditText) view.findViewById(R.id.formEnderecoImovelBairro);
        this.formEnderecoImovelMunicipio          = (AutoCompleteTextView) view.findViewById(R.id.formEnderecoImovelMunicipio);
        this.formEnderecoImovelLatitude           = (EditText) view.findViewById(R.id.formEnderecoImovelLatitude);
        this.formEnderecoImovelLongitude          = (EditText) view.findViewById(R.id.formEnderecoImovelLongitude);
        this.formEnderecoImovelEnderecoAproximado = (RadioGroup) view.findViewById(R.id.formEnderecoImovelEnderecoAproximado);
        this.formEnderecoImovelUf                 = (Spinner)    view.findViewById(R.id.formEnderecoImovelUF);
        this.formEnderecoImovelSpinnerTipoImovel  = (Spinner)    view.findViewById(R.id.formEnderecoImovelSpinnerTipoImovel);

        this.formEnderecoImovelLogradouroLabel = view.findViewById(R.id.formEnderecoImovelLogradouroLabel);
        this.formEnderecoImovelNumeroLabel     = view.findViewById(R.id.formEnderecoImovelNumeroLabel);
        this.formEnderecoImovelCepLabel        = view.findViewById(R.id.formEnderecoImovelCepLabel);
        this.formEnderecoImovelLatitudeLabel   = view.findViewById(R.id.formEnderecoImovelLatitudeLabel);
        this.formEnderecoImovelLongitudeLabel  = view.findViewById(R.id.formEnderecoImovelLongitudeLabel);

        //this.formEnderecoImovelLogradouro.setOnKeyListener(this.onKeyListener2);
        this.formEnderecoImovelLogradouro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(!isNetworkConnected()){
                    return;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final EnderecoImovelVO enderecoImovelVO = new EnderecoImovelVO();
                        enderecoImovelVO.setLogradouro(formEnderecoImovelLogradouro.getText().toString().toUpperCase());
                        enderecoImovelVO.setNumero    (formEnderecoImovelNumero    .getText().toString().toUpperCase());
                        enderecoImovelVO.setCep       (formEnderecoImovelCep       .getText().toString().toUpperCase());
                        enderecoImovelVO.setBairro    (formEnderecoImovelBairro    .getText().toString().toUpperCase());
                        enderecoImovelVO.setMunicipio (formEnderecoImovelMunicipio .getText().toString().toUpperCase());
                        enderecoImovelVO.setMunicipiooo(false);

                        try {

                            String endereco = enderecoImovelVO.getLogradouro() + ", " + enderecoImovelVO.getMunicipio() + " - " + FragmentFromEnderecoImovel.this.enderecoImovelVO.getUF() + " , Brasil";
                            System.out.println(endereco);

                            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                            final List<Address> addresses = geocoder.getFromLocationName(endereco,10);
                            StringBuilder sb = new StringBuilder();
                            enderecoImovelVOs = new ArrayList<EnderecoImovelVO>();

                            if(addresses == null || addresses.isEmpty()){
                                return;
                            }

                            for(final Address address : addresses) {
                                final EnderecoImovelVO enderecoImovelVO2 = new EnderecoImovelVO(address);

                                if (enderecoImovelVO2.getLogradouro() != null && !enderecoImovelVO2.getLogradouro().equals("")) {
                                    enderecoImovelVOs.add(enderecoImovelVO2);
                                }
                            }

                            if(enderecoImovelVOs == null || enderecoImovelVOs.isEmpty()){
                                return;
                            }

                            formEnderecoImovelLogradouro.post(new Runnable() {
                                @Override
                                public void run() {

                                    try {

                                        adapter = new ArrayAdapter(FragmentFromEnderecoImovel.this.getActivity(), android.R.layout.simple_list_item_1, enderecoImovelVOs);
                                        System.out.println("" + enderecoImovelVOs.size());
                                        formEnderecoImovelLogradouro.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                        formEnderecoImovelLogradouro.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                if(enderecoImovelVOs == null || enderecoImovelVOs.size() <= 0 || enderecoImovelVOs.size() < i || i < 0){
                                                    return;
                                                }

                                                EnderecoImovelVO enderecoImovelVO1 = enderecoImovelVOs.get(i);
                                                FragmentFromEnderecoImovel.this.enderecoImovelVO.setMunicipio(formEnderecoImovelMunicipio.getText().toString().toUpperCase());
                                                FragmentFromEnderecoImovel.this.enderecoImovelVO.setTipoImovel((DropDownItemVO) tipoImovel.get(formEnderecoImovelSpinnerTipoImovel.getSelectedItemPosition()));
                                                FragmentFromEnderecoImovel.this.enderecoImovelVO.setUF(mUFs.get(formEnderecoImovelUf.getSelectedItemPosition()));
                                                enderecoImovelVO1.setMunicipio(FragmentFromEnderecoImovel.this.enderecoImovelVO.getMunicipio());
                                                enderecoImovelVO1.setUF(FragmentFromEnderecoImovel.this.enderecoImovelVO.getUF());
                                                enderecoImovelVO1.setTipoImovel(FragmentFromEnderecoImovel.this.enderecoImovelVO.getTipoImovel());

                                                FragmentFromEnderecoImovel.this.enderecoImovelVO = enderecoImovelVO1;
                                                updateUI(true);
                                                Intent intent = new Intent("GETLATITUDEFROMANDDRESS");
                                                intent.putExtra(EnderecoImovelVO.DATAKEY, FragmentFromEnderecoImovel.this.enderecoImovelVO);
                                                LocalBroadcastManager.getInstance(FragmentFromEnderecoImovel.this.getActivity()).sendBroadcast(intent);
                                            }
                                        });
                                    }catch(Exception e){
                                        adapter = new ArrayAdapter(FragmentFromEnderecoImovel.this.getActivity(), android.R.layout.simple_list_item_1, new ArrayList<EnderecoImovelVO>());
                                        formEnderecoImovelLogradouro.setAdapter(adapter);
                                    }
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                            formEnderecoImovelLogradouro.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapter = new ArrayAdapter(FragmentFromEnderecoImovel.this.getActivity(), android.R.layout.simple_list_item_1, new ArrayList<EnderecoImovelVO>());
                                    formEnderecoImovelLogradouro.setAdapter(adapter);
                                }
                            });

                        }
                    }
                }).start();
            }
        });
        this.formEnderecoImovelNumero    .setOnKeyListener(this.onKeyListener);
        this.formEnderecoImovelCep       .setOnKeyListener(this.onKeyListener);
        this.formEnderecoImovelBairro    .setOnKeyListener(this.onKeyListener);
        this.formEnderecoImovelMunicipio .setOnKeyListener(this.onKeyListener);

        this.formEnderecoImovelLatitude .setEnabled(false);
        this.formEnderecoImovelLongitude.setEnabled(false);
        this.formEnderecoImovelEnderecoAproximado.setOnCheckedChangeListener(this);

        this.formEnderecoImovelUf.setAdapter(new BasicSpinnerAdapter(getActivity(), mUFs));
        this.formEnderecoImovelUf.setOnItemSelectedListener(this);

        this.formEnderecoImovelSpinnerTipoImovel.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoImovel));

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch(i){
            case R.id.formEnderecoImovelAproximado:
                this.enderecoImovelVO.setEnderecoAproximado(true);

                this.formEnderecoImovelLogradouro.setVisibility(View.GONE);
                this.formEnderecoImovelNumero    .setVisibility(View.GONE);
                this.formEnderecoImovelCep       .setVisibility(View.GONE);
                this.formEnderecoImovelLatitude  .setVisibility(View.GONE);
                this.formEnderecoImovelLongitude .setVisibility(View.GONE);

                this.formEnderecoImovelLogradouroLabel.setVisibility(View.GONE);
                this.formEnderecoImovelNumeroLabel    .setVisibility(View.GONE);
                this.formEnderecoImovelCepLabel       .setVisibility(View.GONE);
                this.formEnderecoImovelLatitudeLabel  .setVisibility(View.GONE);
                this.formEnderecoImovelLongitudeLabel .setVisibility(View.GONE);

                break;
            case R.id.formEnderecoImovelPreciso:
                this.enderecoImovelVO.setEnderecoAproximado(false);

                this.formEnderecoImovelLogradouro.setVisibility(View.VISIBLE);
                this.formEnderecoImovelNumero    .setVisibility(View.VISIBLE);
                this.formEnderecoImovelCep       .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLatitude  .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLongitude .setVisibility(View.VISIBLE);

                this.formEnderecoImovelLogradouroLabel.setVisibility(View.VISIBLE);
                this.formEnderecoImovelNumeroLabel    .setVisibility(View.VISIBLE);
                this.formEnderecoImovelCepLabel       .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLatitudeLabel  .setVisibility(View.VISIBLE);
                this.formEnderecoImovelLongitudeLabel .setVisibility(View.VISIBLE);

                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {

        if((v.getId() != R.id.formEnderecoImovelLatitude
                && v.getId() != R.id.formEnderecoImovelLongitude) || motionEvent.getAction() != 0 ){
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch(adapterView.getId()) {
            case R.id.formEnderecoImovelSpinnerTipoImovel:
                if (this.tipoImovel == null || this.tipoImovel.isEmpty()) {
                    return;
                }
                this.enderecoImovelVO.setTipoImovel((DropDownItemVO) this.tipoImovel.get(i));
                Utils.sendBroadcastToUpdateForm(this.getActivity(), (DropDownItemVO) this.tipoImovel.get(i));
                break;
            case R.id.formEnderecoImovelUF:
                this.enderecoImovelVO.setUF(mUFs.get(i));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}


    @Override
    public void save() {
        this.enderecoImovelVO.setLogradouro(this.formEnderecoImovelLogradouro.getText().toString().toUpperCase());
        this.enderecoImovelVO.setNumero(this.formEnderecoImovelNumero.getText().toString().toUpperCase());
        this.enderecoImovelVO.setCep(this.formEnderecoImovelCep.getText().toString().toUpperCase());
        this.enderecoImovelVO.setBairro(this.formEnderecoImovelBairro.getText().toString().toUpperCase());
        this.enderecoImovelVO.setMunicipio(this.formEnderecoImovelMunicipio.getText().toString().toUpperCase());

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "ENDERECO_IMOVEL");
        intent.putExtra("RESULT_DATA", this.enderecoImovelVO);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);

        if(method != null){
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.TIPO_IMOVEL.name())){
                this.tipoImovel = (List<BasicValueObject>) object;
                this.formEnderecoImovelSpinnerTipoImovel.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoImovel));
                updateUI(false);
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
