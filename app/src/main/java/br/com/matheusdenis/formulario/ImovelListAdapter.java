package br.com.matheusdenis.formulario;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.LocalCoreDataDAO;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.matheusdenis.formulario.ValueObjects.UnidadeGrid;

/**
 * Created by Matheus on 18/08/2014.
 */
public class ImovelListAdapter extends BaseAdapter {

    private List<?> mList;
    private List<?> mList2;
    private Context mContext;

    public ImovelListAdapter(Context context, List<?> list, List<?> list2) {
        this.mList = list;
        this.mList2 = list2;
        this.mContext = context;
    }

    public ImovelListAdapter(Context context, List<?> list) {
        this.mList = list;
        this.mContext = context;
    }

    public ImovelListAdapter(Context context, DropDownKey[] values) {
        List<DropDownKey> mList = new ArrayList<DropDownKey>();

        for (int i = 0; i < values.length; i++) {
            mList.add(values[i]);
        }

        this.mList = mList;

        this.mContext = context;
    }

    @Override
    public int getCount() {

        int mListCount = mList != null? mList.size() : 0;
        int mList2Count = mList2 != null? mList2.size() : 0;
        return mListCount + mList2Count;
    }

    @Override
    public Object getItem(int position) {

        if(this.mList != null && position < this.mList.size()){
            return this.mList.get(position);
        }else{
            return this.mList2.get(position - this.mList.size());
        }

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if(view == null || view.getTag() == null) {

            view = LayoutInflater.from(mContext).inflate(R.layout.imovel_list_item, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

        if(this.mList != null && position < this.mList.size()){
            ImovelVO o = (ImovelVO) getItem(position);
            viewHolder.endereco.setText(o.getEnderecoImovelVO().getLogradouro() + ", " + o.getEnderecoImovelVO().getNumero() + " - " + o.getEnderecoImovelVO().getBairro());
            viewHolder.cidade  .setText(o.getEnderecoImovelVO().getMunicipio() + " - " + o.getEnderecoImovelVO().getUF() + ", " + o.getEnderecoImovelVO().getCep());
            viewHolder.tipo_imovel  .setText(o.getEnderecoImovelVO().getTipoImovel() != null ? o.getEnderecoImovelVO().getTipoImovel().getValue() : "");
            viewHolder.tipo_imovel  .setVisibility(o.getEnderecoImovelVO().getTipoImovel() != null ? View.VISIBLE : View.GONE);

            if(o.getImagemVOs() != null && !o.getImagemVOs().isEmpty()){
                if(o.getImagemVOs().get(0).getImagePackage() != null && !o.getImagemVOs().get(0).getImagePackage().isEmpty()){
                    byte[] bytes = (byte[]) LocalCoreDataDAO.get(mContext, o.getImagemVOs().get(0).getImagePackage());
                    if(bytes != null && bytes.length > 0){
                        o.getImagemVOs().get(0).setImageBytes(bytes);
                    }
                }

                if(o.getImagemVOs().get(0).getImageBytes() != null
                        && o.getImagemVOs().get(0).getImageBytes().length > 0){
                    Bitmap bitmap = BitmapFactory.decodeByteArray(o.getImagemVOs().get(0).getImageBytes(), 0, o.getImagemVOs().get(0).getImageBytes().length);
                    viewHolder.imagem.setImageBitmap(bitmap);
                    viewHolder.imagem.setVisibility(View.VISIBLE);
                }else{
                    if(o.getImagemVOs().get(0).getImagePath() != null && !o.getImagemVOs().get(0).getImagePath().isEmpty()){
                        Bitmap bitmap = BitmapFactory.decodeFile(o.getImagemVOs().get(0).getImagePath());
                        viewHolder.imagem.setImageBitmap(bitmap);
                        viewHolder.imagem.setVisibility(View.VISIBLE);
                    }else{
                        viewHolder.imagem.setImageResource(R.drawable.sem_imagem_peq);
                        viewHolder.imagem.setVisibility(View.VISIBLE);
                    }                }
            }


        }else{
            UnidadeGrid o = (UnidadeGrid) getItem(position);
            viewHolder.endereco.setText(o.getLabel01());
            viewHolder.cidade  .setText(o.getLabel02());
            viewHolder.tipo_imovel  .setText(o.getLabel03() != null ? o.getLabel03() : "");
            viewHolder.tipo_imovel  .setVisibility(o.getLabel03() != null && !o.getLabel03().equals("") ? View.VISIBLE : View.GONE);
            viewHolder.imagem.setImageResource(R.drawable.sem_imagem_peq);
            if(o.getImagemVO() != null &&
                    (o.getImagemVO()).getImageBytes() != null
                    && (o.getImagemVO()).getImageBytes().length > 0){
                Bitmap bitmap = BitmapFactory.decodeByteArray((o.getImagemVO()).getImageBytes(), 0, (o.getImagemVO()).getImageBytes().length);
                viewHolder.imagem.setImageBitmap(bitmap);
                viewHolder.imagem.setVisibility(View.VISIBLE);
            }else{
                viewHolder.imagem.setImageResource(R.drawable.sem_imagem_peq);
                viewHolder.imagem.setVisibility(View.GONE);
            }

            if(o.getImageUrl() != null && !o.getImageUrl().isEmpty()){
                Picasso.with(mContext).load(o.getImageUrl()).into(viewHolder.imagem);
                viewHolder.imagem.setVisibility(View.VISIBLE);
            }else{
                if(o.getImagemVO() != null) {
                    if (o.getImagemVO().getImageUrl() != null && !o.getImagemVO().getImageUrl().isEmpty()) {
                        Picasso.with(mContext).load(o.getImagemVO().getImageUrl()).resize(130, 130).centerCrop().into(viewHolder.imagem);
                        viewHolder.imagem.setVisibility(View.VISIBLE);
                    } else {
                        if (o.getImagemVO().getImagePath() != null && !o.getImagemVO().getImagePath().isEmpty()) {
                            Bitmap bitmap = BitmapFactory.decodeFile(o.getImagemVO().getImagePath());
                            viewHolder.imagem.setImageBitmap(bitmap);
                            viewHolder.imagem.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.imagem.setImageResource(R.drawable.sem_imagem_peq);
                            viewHolder.imagem.setVisibility(View.VISIBLE);
                        }
                    }
                }else {
                    viewHolder.imagem.setImageResource(R.drawable.sem_imagem_peq);
                    viewHolder.imagem.setVisibility(View.VISIBLE);
                }
            }
        }
        return view;
    }

    class ViewHolder {
        public TextView endereco;
        public TextView cidade;
        public TextView tipo_imovel;
        public ImageView imagem;

        ViewHolder(View view) {

            endereco = (TextView) view.findViewById(R.id.endereco);
            cidade = (TextView) view.findViewById(R.id.cidade);
            imagem = (ImageView) view.findViewById(R.id.imagem);
            tipo_imovel = (TextView) view.findViewById(R.id.tipoimovel);

        }
    }
}