package br.com.matheusdenis.formulario;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

public class SerializableBundle<T> implements Serializable{

	private static final long serialVersionUID = 9043892721732141295L;
	private Object value;
	
	public SerializableBundle (){
		
	}
	
	public SerializableBundle ( Object value  ){
		this.value = value;
	}	

	@SuppressWarnings("unchecked")
	public T getObject(){
		return (T) value;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> getList(){
		return (ArrayList<T>) value;
	}	

	@SuppressWarnings("unchecked")
	public Map<String, Object> getMap(){
		return (Map<String, Object>) value;
	}	
}
