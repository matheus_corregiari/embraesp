package br.com.matheusdenis.formulario;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.LocalCoreDataDAO;
import br.com.database.Management.ImagemManagement;
import br.com.database.ValueObjects.ImagemVO;
import br.com.genericdata.valueobjects.ValidatorVO;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentFromImagem extends DataBaseCoreFragment implements View.OnClickListener {

    private List<ImagemVO> imagemVOs;
    private boolean useClick;

    private ViewPager viewPager;
    private FloatingActionButton buttonRemove;
    private FloatingActionButton buttonAdd;
    public Uri urii;


    public static FragmentFromImagem newInstance(List<ImagemVO> imagemVOs, boolean useClick){

        if(imagemVOs == null){
            imagemVOs = new ArrayList<ImagemVO>();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(imagemVOs));
        bundle.putBoolean("USE_CLICK", useClick);
        FragmentFromImagem fragmentFromImagem = new FragmentFromImagem();
        fragmentFromImagem.setArguments(bundle);
        return fragmentFromImagem;

    }

    @Override
    public void save() {

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "IMAGEM");
        intent.putExtra("RESULT_DATA", new SerializableBundle<ImagemVO>(this.imagemVOs));
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

    }

    @Override
    protected void disableElements() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setDataBaseManagement(new ImagemManagement(getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.imagemVOs = ((SerializableBundle<ImagemVO>)savedInstanceState.getSerializable(ImagemVO.DATAKEY)).getList();
            this.useClick = savedInstanceState.getBoolean("USE_CLICK", false);
        }else{
            this.imagemVOs = ((SerializableBundle<ImagemVO>)getArguments().getSerializable(ImagemVO.DATAKEY)).getList();
            this.useClick = getArguments().getBoolean("USE_CLICK", false);
        }

        if(this.imagemVOs == null){
            this.imagemVOs = new ArrayList<ImagemVO>();
        }



    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(this.imagemVOs));
        outState.putBoolean("USE_CLICK", this.useClick);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_imagens, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
        }

        if(this.imagemVOs == null || this.imagemVOs.isEmpty()){
            buttonRemove.setVisibility(View.GONE);
        }

        this.viewPager.setAdapter(new HomeAdapter(getFragmentManager()));
        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                if(imagemVOs.isEmpty() || imagemVOs.get(i).getImagePackage() == null || imagemVOs.get(i).getImagePackage().equals("")){
                    buttonRemove.setVisibility(View.GONE);
                }else{
                    buttonRemove.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void initializeVariables(View view){

        if(view == null){
            return;
        }

        this.viewPager    = (ViewPager) view.findViewById(R.id.formImagemViewPager);
        this.buttonRemove = (FloatingActionButton) view.findViewById(R.id.formImagemRemovePictureButton);
        this.buttonAdd    = (FloatingActionButton) view.findViewById(R.id.formImagemAddPictureButton);

        this.buttonRemove.setOnClickListener(this);
        this.buttonAdd   .setOnClickListener(this);

        this.viewPager.setSaveEnabled(true);

    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.DELETE))){

                ImagemVO valueObject = (ImagemVO) object;

                for (Iterator<ImagemVO> iterator = imagemVOs.iterator(); iterator.hasNext();){
                    ImagemVO ImagemVO = iterator.next();
                    if(ImagemVO.getId() == valueObject.getId()){
                        iterator.remove();
                        break;
                    }
                }
                updateUI(false);
                Toast.makeText(getActivity(), "Imagem Excluída", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.DELETE))){
                Toast.makeText(getActivity(), "Não foi possível excluir a Imagem", Toast.LENGTH_SHORT).show();
                exception.printStackTrace();
                Log.wtf("DELETEE", exception.getMessage());
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.formImagemRemovePictureButton:

                ImagemVO ImagemVO = this.imagemVOs.get(this.viewPager.getCurrentItem());

                if(ImagemVO.getId() <= 0L){
                    this.imagemVOs.remove(ImagemVO);
                    updateUI(false);
                }else{
                    this.sendToDataBase(MethodTag.DELETE, this.imagemVOs.get(this.viewPager.getCurrentItem()).getId());
                }

                break;
            case R.id.formImagemAddPictureButton:

                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Escolher Ação");
                alertDialog.setMessage("Deseja escolher uma foto existente ou tirar uma nova foto?");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Tirar nova Foto", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        File mediaStoreDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "GeoEmbraesp");

                        if(!mediaStoreDir.exists()){
                            if(!mediaStoreDir.mkdirs()){
                                return;
                            }
                        }

                        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        File mediaFile = new File(mediaStoreDir.getPath()+ File.separator + "IMG_" + timestamp + "_" + RandomStringUtils.random(10, true, true) + ".jpg");
                        Uri uri = Uri.fromFile(mediaFile);

                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                        urii = uri;
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            //startActivityForResult(takePictureIntent, 235);
                            startActivityForResult(takePictureIntent, 200);
                        }
                    }
                });
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Escolher foto existente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Selecione uma imagem"), 250);
                    }
                });
                alertDialog.show();


                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 235 && resultCode == Activity.RESULT_OK){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Bundle extras = data.getExtras();
                    final Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                    byte[] byteArray = stream.toByteArray();

                    ImagemVO imagemVO = new ImagemVO();
                    imagemVO.setImageBytes(byteArray);
                    if(byteArray != null && byteArray.length > 0) {
                        LocalCoreDataDAO.save(getActivity(), byteArray, imagemVO.getImagePackage());
                    }
                    imagemVOs.add(imagemVO);
                    viewPager.post(new Runnable() {
                        @Override
                        public void run() {
                           updateUI(false);
                        }
                    });
                }
            }).start();
        }

        if(requestCode == 9182 && resultCode == Activity.RESULT_OK){

            this.imagemVOs = ((SerializableBundle<ImagemVO>)data.getExtras().getSerializable(ImagemVO.DATAKEY)).getList();
            this.updateUI(false);

        }

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Uri _uri;
            if(data != null && data.getData() != null){
                _uri = data.getData();
            }else{
                if(urii != null){
                    _uri = urii;
                    urii = null;
                }else{
                    return;
                }
            }
            final String selectedImagePath = getPath(_uri);

            ImagemVO imagemVO = new ImagemVO();
            imagemVO.setImagePath(selectedImagePath);
            imagemVOs.add(imagemVO);
            updateUI(false);

            uploadFile(new File(_uri.getPath()));
        }

        if (requestCode == 250 && resultCode == Activity.RESULT_OK) {
            Uri _uri;
            if(data != null && data.getData() != null){
                _uri = data.getData();
            }else{
                if(urii != null){
                    _uri = urii;
                    urii = null;
                }else{
                    return;
                }
            }
            final String selectedImagePath = Utils.getPath(getActivity(), _uri);

            ImagemVO imagemVO = new ImagemVO();
            imagemVO.setImagePath(selectedImagePath);
            imagemVOs.add(imagemVO);
            updateUI(false);

            uploadFile(new File(selectedImagePath));
        }
    }



    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    @Override
    public void onPause() {
        Intent intent = new Intent();
        intent.putExtra(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(imagemVOs));
        getActivity().setResult(Activity.RESULT_OK, intent);
        super.onPause();
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }

    class HomeAdapter extends FragmentStatePagerAdapter {

        public HomeAdapter( FragmentManager fm ) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if(imagemVOs == null || imagemVOs.isEmpty()){
                return FragmentImagem.newInstance(new ImagemVO(), new ArrayList<ImagemVO>(), false);
            }

            if( imagemVOs.get(position).getImagePackage() == null || imagemVOs.get(position).getImagePackage().equals("")){
                buttonRemove.setVisibility(View.GONE);
            }else{
                buttonRemove.setVisibility(View.VISIBLE);
            }

            return FragmentImagem.newInstance((ImagemVO) imagemVOs.get(position), imagemVOs,  useClick);
        }

        @SuppressLint("DefaultLocale")
        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        @Override
        public int getCount() {

            if(imagemVOs == null || imagemVOs.isEmpty()){
                return 1;
            }

            return imagemVOs.size();
        }
    }

    public void uploadFile(final File fileName){

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(fileName);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);

        if(!isNetworkConnected()){
            return;
        }

        if(!fileName.exists()){
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                FTPClient client = new FTPClient();

                try {

                    client.connect("ftpimg.geoembraesp.com.br",2211);
                    client.login("ftpimg", "ftp@geoimg");
                    client.setType(FTPClient.TYPE_BINARY);
                    client.setAutoNoopTimeout(0);
                    //client.changeDirectory("/upload/");

                    client.upload(fileName, null);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.wtf("DEU RUIM", e.toString());
                    try {
                        client.disconnect(true);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }
}
