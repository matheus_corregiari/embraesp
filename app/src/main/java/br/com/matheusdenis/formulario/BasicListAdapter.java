package br.com.matheusdenis.formulario;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImovelVO;

/**
 * Created by Matheus on 18/08/2014.
 */
public class BasicListAdapter extends BaseAdapter {

    private List<?> mList;
    private Context mContext;

    public BasicListAdapter(Context context, List<?> list) {
        this.mList = list;
        this.mContext = context;
    }

    public BasicListAdapter(Context context, DropDownKey[] values) {
        List<DropDownKey> mList = new ArrayList<DropDownKey>();

        for (int i = 0; i < values.length; i++) {
            mList.add(values[i]);
        }

        this.mList = mList;

        this.mContext = context;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return this.mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = LayoutInflater.from(mContext).inflate(R.layout.lala_spinner_list_item, null);
        TextView textView = (TextView) view.findViewById(R.id.textView);

        Object o = getItem(position);

        if(o instanceof DropDownKey){
            textView.setText(((DropDownKey) o).name());
        }

        if(o instanceof DropDownItemVO){
            textView.setText(((DropDownItemVO) o).getValue());
        }

        if(o instanceof EnderecoImovelVO){
            textView.setText(((EnderecoImovelVO) o).getLogradouro());
        }

        if(o instanceof ImovelVO){
            textView.setText(((ImovelVO) o).getEnderecoImovelVO().getLogradouro());
        }

        if(o instanceof String){
            textView.setText(((String) o));
        }

        return view;
    }

    class ViewHolder {
        public TextView title;
    }
}