package br.com.matheusdenis.formulario;

import android.app.ProgressDialog;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.VisibleRegion;

import org.ksoap2.serialization.PropertyInfo;

import java.util.ArrayList;
import java.util.List;

import br.com.database.LocalCoreDataDAO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.genericdata.coredata.enums.ConnectionType;
import br.com.genericdata.coredata.interfaces.GenericDataResultDelegate;
import br.com.genericdata.coredata.servermanager.ServerManager;
import br.com.genericdata.valueobjects.ValidatorVO;
import br.com.matheusdenis.formulario.ValueObjects.UnidadeGrid;
import br.com.matheusdenis.formulario.ValueObjects.UnidadeMapa;
import br.com.matheusdenis.formulario.ValueObjects.Usuario;

/**
 * Created by Matheus on 21/10/2014.
 */
public class ServerConnectManager extends ServerManager {

    private ProgressDialog ringProgressDialog;

    public ServerConnectManager(Context mContext, GenericDataResultDelegate genericDataResultDelegate) {
        super(mContext, genericDataResultDelegate);
    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

        try {
            Crashlytics.log(1, serviceName, exception.getMessage());
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            if (ringProgressDialog != null && ringProgressDialog.isShowing()) {
                ringProgressDialog.dismiss();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        super.onServerError(serviceName, exception, validators, parameters);
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

        if(result != null && result instanceof ImovelVO){
            ImovelVO imovelVO = (ImovelVO) result;
            if(imovelVO.getImagemVOs() != null){
                for(ImagemVO imagemVO : imovelVO.getImagemVOs()){
                    if(imagemVO.getImageUrl() != null && !imagemVO.getImageUrl().isEmpty()){
                        imagemVO.setImageBytes(null);
                    }
                }
            }
        }

        if(serviceName.equals(R.string.getUnidadesMapa_method_name)){
            if(resultList != null){
                for(Object object : resultList){
                    if(object instanceof UnidadeGrid){
                        if( ((UnidadeGrid) object).getImageUrl() != null && ! ((UnidadeGrid) object).getImageUrl().isEmpty()){
                            ((UnidadeGrid) object).setImagemVO(null);
                        }else{
                            if(((UnidadeGrid) object).getImagemVO() != null && ((UnidadeGrid) object).getImagemVO().getImageUrl() != null && !((UnidadeGrid) object).getImagemVO().getImageUrl().isEmpty()){
                                ((UnidadeGrid) object).getImagemVO().setImageBytes(null);
                            }
                        }
                    }
                }
            }
        }

        try {
            if (ringProgressDialog != null && ringProgressDialog.isShowing()) {
                ringProgressDialog.dismiss();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        super.onServerResult(serviceName, hasErrors, resultList, result, parameters);
    }

    @Override
    public void get(Object... objects) {

        if(objects != null && objects.length != 0){
            GoogleMap googleMap = (GoogleMap) objects[0];
            long tipoImovel   = (Long)    objects[1];

            if(googleMap != null){
                Projection projection = googleMap.getProjection();
                if(projection != null){
                    VisibleRegion visibleRegion = projection.getVisibleRegion();
                    if(visibleRegion != null){
                        float farLeftLat   = visibleRegion.farLeft   != null? (float) visibleRegion.farLeft.latitude  : 0F;
                        float farLeftLng   = visibleRegion.farLeft   != null? (float) visibleRegion.farLeft.longitude : 0F;
                        float farRightLat  = visibleRegion.farRight  != null? (float) visibleRegion.farLeft.latitude  : 0F;
                        float farRightLng  = visibleRegion.farRight  != null? (float) visibleRegion.farLeft.longitude : 0F;
                        float nearLeftLat  = visibleRegion.nearLeft  != null? (float) visibleRegion.farLeft.latitude  : 0F;
                        float nearLeftLng  = visibleRegion.nearLeft  != null? (float) visibleRegion.farLeft.longitude : 0F;
                        float nearRightLat = visibleRegion.nearRight != null? (float) visibleRegion.farLeft.latitude  : 0F;
                        float nearRightLng = visibleRegion.nearRight != null? (float) visibleRegion.farLeft.longitude : 0F;

                        PropertyInfo propertyInfo1 = new PropertyInfo();
                        propertyInfo1.setName("farLeftLat");
                        propertyInfo1.setValue(farLeftLat);
                        propertyInfo1.setType(Double.class);

                        PropertyInfo propertyInfo2 = new PropertyInfo();
                        propertyInfo2.setName("farLeftLng");
                        propertyInfo2.setValue(farLeftLng);
                        propertyInfo2.setType(Double.class);

                        PropertyInfo propertyInfo3 = new PropertyInfo();
                        propertyInfo3.setName("farRightLat");
                        propertyInfo3.setValue(farRightLat);
                        propertyInfo3.setType(Double.class);

                        PropertyInfo propertyInfo4 = new PropertyInfo();
                        propertyInfo4.setName("farRightLng");
                        propertyInfo4.setValue(farRightLng);
                        propertyInfo4.setType(Double.class);

                        PropertyInfo propertyInfo5 = new PropertyInfo();
                        propertyInfo5.setName("nearLeftLat");
                        propertyInfo5.setValue(nearLeftLat);
                        propertyInfo5.setType(Double.class);

                        PropertyInfo propertyInfo6 = new PropertyInfo();
                        propertyInfo6.setName("nearLeftLng");
                        propertyInfo6.setValue(nearLeftLng);
                        propertyInfo6.setType(Double.class);

                        PropertyInfo propertyInfo7 = new PropertyInfo();
                        propertyInfo7.setName("nearRightLat");
                        propertyInfo7.setValue(nearRightLat);
                        propertyInfo7.setType(Double.class);

                        PropertyInfo propertyInfo8 = new PropertyInfo();
                        propertyInfo8.setName("nearRightLng");
                        propertyInfo8.setValue(nearRightLng);
                        propertyInfo8.setType(Double.class);

                        PropertyInfo propertyInfo9 = new PropertyInfo();
                        propertyInfo9.setName("zoomLevel");
                        propertyInfo9.setValue((int) googleMap.getCameraPosition().zoom);
                        propertyInfo9.setType(Integer.class);

                        PropertyInfo propertyInfo10 = new PropertyInfo();
                        propertyInfo10.setName("tipoImovel");
                        propertyInfo10.setValue(tipoImovel);
                        propertyInfo10.setType(int.class);

                        this.sendToServer(false, ConnectionType.TYPE_SOAP,
                                mContext.getString(R.string.getUnidadesMapa_soap_action),
                                mContext.getString(R.string.getUnidadesMapa_namespace),
                                mContext.getString(R.string.getUnidadesMapa_method_name),
                                UnidadeMapa.class,
                                propertyInfo1,
                                propertyInfo2,
                                propertyInfo3,
                                propertyInfo4,
                                propertyInfo5,
                                propertyInfo6,
                                propertyInfo7,
                                propertyInfo8,
                                propertyInfo9,
                                propertyInfo10);

                        this.sendToServer(false, ConnectionType.TYPE_SOAP,
                                mContext.getString(R.string.getUnidadesGrid_soap_action),
                                mContext.getString(R.string.getUnidadesGrid_namespace),
                                mContext.getString(R.string.getUnidadesGrid_method_name),
                                UnidadeGrid.class,
                                propertyInfo1,
                                propertyInfo2,
                                propertyInfo3,
                                propertyInfo4,
                                propertyInfo5,
                                propertyInfo6,
                                propertyInfo7,
                                propertyInfo8,
                                propertyInfo9,
                                propertyInfo10);
                    }
                }
            }
        }
    }



    @Override
    public void set(Object... objects) {

        PropertyInfo propertyInfo1 = new PropertyInfo();
        propertyInfo1.setName("unidadeId");
        propertyInfo1.setValue(objects[0]);
        propertyInfo1.setType(int.class);

        this.sendToServer(false, ConnectionType.TYPE_SOAP,
                mContext.getString(R.string.getUnidade_soap_action),
                mContext.getString(R.string.getUnidade_namespace),
                mContext.getString(R.string.getUnidade_method_name),
                ImovelVO.class,
                propertyInfo1);

        try{
            ringProgressDialog = ProgressDialog.show(mContext, "", "Aguarde...", true);
            ringProgressDialog.setCancelable(true);
        }catch(Exception e){
            e.printStackTrace();
        }    }

    public Usuario login(String login, String senha) {

        PropertyInfo propertyInfo1 = new PropertyInfo();
        propertyInfo1.setName("username");
        propertyInfo1.setValue(login);
        propertyInfo1.setType(String.class);

        PropertyInfo propertyInfo2 = new PropertyInfo();
        propertyInfo2.setName("password");
        propertyInfo2.setValue(senha);
        propertyInfo2.setType(String.class);

        this.sendToServer(false, ConnectionType.TYPE_SOAP,
                mContext.getString(R.string.login_soap_action),
                mContext.getString(R.string.login_namespace),
                mContext.getString(R.string.login_method_name),
                String.class,
                propertyInfo1,
                propertyInfo2);

        try{
            ringProgressDialog = ProgressDialog.show(mContext, "", "Aguarde...", true);
            ringProgressDialog.setCancelable(true);
        }catch(Exception e){
            e.printStackTrace();
        }

        return new Usuario(login, senha);
    }

    @Override
    public void delete(Object... objects) {

    }

    @Override
    public void send(Object... objects) {

    }

    @Override
    public void save(Object... objects) {

        ImovelVO imovelVO = (ImovelVO) objects[0];

        if(imovelVO.getImagemVOs() != null && !imovelVO.getImagemVOs().isEmpty()){
            List<ImagemVO> imagemVOs = new ArrayList<ImagemVO>();
            for(ImagemVO imagemVO : imovelVO.getImagemVOs()){
                if(imagemVO.getImagePackage() != null && !imagemVO.getImagePackage().isEmpty()){
                    byte[] bytes = (byte[]) LocalCoreDataDAO.get(mContext, imagemVO.getImagePackage());
                    if(bytes != null && bytes.length > 0){
                        imagemVO.setImageBytes(bytes);
                        imagemVOs.add(imagemVO);
                    }
                }

                if(imagemVO.getImagePath()  != null && !imagemVO.getImagePath().isEmpty()){
                    imagemVOs.add(imagemVO);
                }
            }
            imovelVO.setImagemVOs(imagemVOs);
        }
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = null;
        try {
            json = ow.writeValueAsString(imovelVO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        PropertyInfo propertyInfo1 = new PropertyInfo();
        propertyInfo1.setName("json");
        propertyInfo1.setValue(json);
        propertyInfo1.setType(String.class);

        System.out.println(json);

        this.sendToServer(false, ConnectionType.TYPE_SOAP,
                mContext.getString(R.string.saveUnidade_soap_action),
                mContext.getString(R.string.saveUnidade_namespace),
                mContext.getString(R.string.saveUnidade_method_name),
                boolean.class,
                propertyInfo1);

        try{
            ringProgressDialog = ProgressDialog.show(mContext, "", "Aguarde...", true);
            ringProgressDialog.setCancelable(true);
        }catch(Exception e){
            e.printStackTrace();
        }    }

}
