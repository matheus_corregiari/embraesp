package br.com.matheusdenis.formulario;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Spinner;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.UnidadeEdificacaoManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.UnidadeEdificacaoVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentVisualUnidadeEdificacao extends DataBaseCoreFragment {

    private UnidadeEdificacaoVO unidadeEdificacaoVO;

    private TextView formUnidadeEdificacaoPavimentos;
    private TextView formUnidadeEdificacaoDataConstrucao;
    private TextView formUnidadeEdificacaoObservacoes;
    private Spinner  formUnidadeEdificacaoSpinnerTipoEstrutura;
    private Spinner  formUnidadeEdificacaoSpinnerTipoCobertura;
    private Spinner  formUnidadeEdificacaoSpinnerPadraoEconomico;
    private Spinner  formUnidadeEdificacaoSpinnerTipoConservacao;

    private TextView formUnidadeEdificacaoAreaTotal;
    private TextView formUnidadeEdificacaoAreaComum;
    private TextView formUnidadeEdificacaoAreaUtil;

    private NumberPickerBuilder numberPickerDialog;
    private DatePickerDialog    datePickerBuilder;

    private List<BasicValueObject> tipoEstruturas;
    private List<BasicValueObject> tipoCoberturas;
    private List<BasicValueObject> padraoEconomicos;
    private List<BasicValueObject> tipoConservacao;

    public static FragmentVisualUnidadeEdificacao newInstance(UnidadeEdificacaoVO unidadeEdificacaoVO){

        if(unidadeEdificacaoVO == null){
            unidadeEdificacaoVO = new UnidadeEdificacaoVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(UnidadeEdificacaoVO.DATAKEY, unidadeEdificacaoVO);
        FragmentVisualUnidadeEdificacao fragmentFromUnidadeEdificacao = new FragmentVisualUnidadeEdificacao();
        fragmentFromUnidadeEdificacao.setArguments(bundle);
        return fragmentFromUnidadeEdificacao;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setDataBaseManagement(new UnidadeEdificacaoManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.unidadeEdificacaoVO = (UnidadeEdificacaoVO) savedInstanceState.getSerializable(UnidadeEdificacaoVO.DATAKEY);
            this.tipoEstruturas      = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.EDIFICACAO_TIPO_ESTRUTURA   .name())).getList();
            this.tipoCoberturas      = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.EDIFICACAO_TIPO_COBERTURA  .name())).getList();
            this.padraoEconomicos    = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name())).getList();
            this.tipoConservacao     = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name())).getList();

            if(this.unidadeEdificacaoVO == null){
                this.unidadeEdificacaoVO = new UnidadeEdificacaoVO();
            }

            if(this.tipoEstruturas == null){
                this.tipoEstruturas = new ArrayList<BasicValueObject>();
            }

            if(this.tipoCoberturas == null){
                this.tipoCoberturas = new ArrayList<BasicValueObject>();
            }

            if(this.padraoEconomicos == null){
                this.padraoEconomicos = new ArrayList<BasicValueObject>();
            }

            if(this.tipoConservacao == null){
                this.tipoConservacao = new ArrayList<BasicValueObject>();
            }

        }else{
            this.unidadeEdificacaoVO = (UnidadeEdificacaoVO) getArguments().getSerializable(UnidadeEdificacaoVO.DATAKEY);
            this.tipoEstruturas   = new ArrayList<BasicValueObject>();
            this.tipoCoberturas   = new ArrayList<BasicValueObject>();
            this.padraoEconomicos = new ArrayList<BasicValueObject>();
            this.tipoConservacao  = new ArrayList<BasicValueObject>();
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.EDIFICACAO_TIPO_ESTRUTURA.name());
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.EDIFICACAO_TIPO_COBERTURA.name());
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name());
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name());
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(UnidadeEdificacaoVO.DATAKEY, this.unidadeEdificacaoVO);
        outState.putSerializable(DropDownKey.EDIFICACAO_TIPO_ESTRUTURA  .name(), new SerializableBundle<BasicValueObject>(this.tipoEstruturas));
        outState.putSerializable(DropDownKey.EDIFICACAO_TIPO_COBERTURA  .name(), new SerializableBundle<BasicValueObject>(this.tipoCoberturas));
        outState.putSerializable(DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name(), new SerializableBundle<BasicValueObject>(this.padraoEconomicos));
        outState.putSerializable(DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name(), new SerializableBundle<BasicValueObject>(this.tipoConservacao));
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visual_unidade_edificacao, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
            this.formUnidadeEdificacaoObservacoes.setText(this.unidadeEdificacaoVO.getObservacoes() == null ? "" : this.unidadeEdificacaoVO.getObservacoes().toUpperCase());
        }

        this.formUnidadeEdificacaoPavimentos.setText(this.unidadeEdificacaoVO.getPavimentos() < 0 ? 0 + "" : this.unidadeEdificacaoVO.getPavimentos() + "");
        this.formUnidadeEdificacaoPavimentos.setTag (this.unidadeEdificacaoVO.getPavimentos());
        this.formUnidadeEdificacaoDataConstrucao.setText(this.unidadeEdificacaoVO.getDataConstrucao() == null ? "" : checkDate(this.unidadeEdificacaoVO.getDataConstrucao()));
        this.formUnidadeEdificacaoDataConstrucao.setTag(this.unidadeEdificacaoVO.getDataConstrucao());
        this.formUnidadeEdificacaoAreaTotal.setText(String.format("%1$,.2f m²", this.unidadeEdificacaoVO.getAreaTotal()));
        this.formUnidadeEdificacaoAreaTotal.setTag(this.unidadeEdificacaoVO.getAreaTotal());
        this.formUnidadeEdificacaoAreaComum.setText(String.format("%1$,.2f m²", this.unidadeEdificacaoVO.getAreaComum()));
        this.formUnidadeEdificacaoAreaComum.setTag(this.unidadeEdificacaoVO.getAreaComum());
        this.formUnidadeEdificacaoAreaUtil.setText(String.format("%1$,.2f m²", this.unidadeEdificacaoVO.getAreaUtil()));
        this.formUnidadeEdificacaoAreaUtil.setTag(this.unidadeEdificacaoVO.getAreaUtil());

        int i = Utils.indexOf(tipoEstruturas, this.unidadeEdificacaoVO.getTipoEstrutura());
        i = i < 0? 0:i;

        this.formUnidadeEdificacaoSpinnerTipoEstrutura.setOnItemSelectedListener(null);
        this.formUnidadeEdificacaoSpinnerTipoEstrutura.setSelection(i);

        int j = Utils.indexOf(tipoCoberturas, this.unidadeEdificacaoVO.getTipoCobertura());
        j = j < 0? 0:j;

        this.formUnidadeEdificacaoSpinnerTipoCobertura.setOnItemSelectedListener(null);
        this.formUnidadeEdificacaoSpinnerTipoCobertura.setSelection(j);

        int k = Utils.indexOf(padraoEconomicos, this.unidadeEdificacaoVO.getPadraoEconomico());
        k = k < 0? 0:k;

        this.formUnidadeEdificacaoSpinnerPadraoEconomico.setOnItemSelectedListener(null);
        this.formUnidadeEdificacaoSpinnerPadraoEconomico.setSelection(k);

        int l = Utils.indexOf(tipoConservacao, this.unidadeEdificacaoVO.getTipoConservacao());
        l = l < 0? 0:l;

        this.formUnidadeEdificacaoSpinnerTipoConservacao.setOnItemSelectedListener(null);
        this.formUnidadeEdificacaoSpinnerTipoConservacao.setSelection(l);

    }

    private void initializeVariables(View view){

        if(view == null){
            return;
        }

        this.formUnidadeEdificacaoPavimentos             = (TextView) view.findViewById(R.id.formUnidadeEdificacaoPavimentos);
        this.formUnidadeEdificacaoDataConstrucao         = (TextView) view.findViewById(R.id.formUnidadeEdificacaoDataConstrucao);
        this.formUnidadeEdificacaoObservacoes            = (TextView) view.findViewById(R.id.formUnidadeEdificacaoObservacoes);
        this.formUnidadeEdificacaoSpinnerTipoEstrutura   = (Spinner)  view.findViewById(R.id.formUnidadeEdificacaoSpinnerTipoEstrutura);
        this.formUnidadeEdificacaoSpinnerTipoCobertura   = (Spinner)  view.findViewById(R.id.formUnidadeEdificacaoSpinnerTipoCobertura);
        this.formUnidadeEdificacaoSpinnerPadraoEconomico = (Spinner)  view.findViewById(R.id.formUnidadeEdificacaoSpinnerPadraoEconomico);
        this.formUnidadeEdificacaoSpinnerTipoConservacao = (Spinner)  view.findViewById(R.id.formUnidadeEdificacaoSpinnerTipoConservacao);
        this.formUnidadeEdificacaoAreaTotal              = (TextView) view.findViewById(R.id.formUnidadeEdificacaoAreaTotal);
        this.formUnidadeEdificacaoAreaComum              = (TextView) view.findViewById(R.id.formUnidadeEdificacaoAreaComum);
        this.formUnidadeEdificacaoAreaUtil               = (TextView) view.findViewById(R.id.formUnidadeEdificacaoAreaUtil);

        view.findViewById(R.id.formUnidadeEdificacaoPavimentos).setVisibility(this.unidadeEdificacaoVO.getPavimentos() <= 0 ? View.GONE : View.VISIBLE);;
        view.findViewById(R.id.formUnidadeEdificacaoDataConstrucao).setVisibility(this.unidadeEdificacaoVO.getDataConstrucao() == null ? View.GONE : View.VISIBLE);;
        view.findViewById(R.id.formUnidadeEdificacaoObservacoes).setVisibility(this.unidadeEdificacaoVO.getObservacoes() == null ||  this.unidadeEdificacaoVO.getObservacoes().equals("") ? View.GONE : View.VISIBLE);

        //view.findViewById(R.id.formUnidadeEdificacaoPavimentosLabel).setVisibility(this.unidadeEdificacaoVO.getPavimentos() <= 0 ? View.GONE : View.VISIBLE);;
        //view.findViewById(R.id.formUnidadeEdificacaoDataConstrucaoLabel).setVisibility(this.unidadeEdificacaoVO.getDataConstrucao() == null ? View.GONE : View.VISIBLE);;
        //view.findViewById(R.id.formUnidadeEdificacaoObservacoesLabel).setVisibility(this.unidadeEdificacaoVO.getObservacoes() == null ||  this.unidadeEdificacaoVO.getObservacoes().equals("") ? View.GONE : View.VISIBLE);


        this.formUnidadeEdificacaoSpinnerTipoConservacao.setEnabled(false);
        this.formUnidadeEdificacaoSpinnerTipoConservacao.setClickable(false);
        this.formUnidadeEdificacaoSpinnerPadraoEconomico.setEnabled(false);
        this.formUnidadeEdificacaoSpinnerPadraoEconomico.setClickable(false);
        this.formUnidadeEdificacaoSpinnerTipoCobertura.setEnabled(false);
        this.formUnidadeEdificacaoSpinnerTipoCobertura.setClickable(false);
        this.formUnidadeEdificacaoSpinnerTipoEstrutura.setEnabled(false);
        this.formUnidadeEdificacaoSpinnerTipoEstrutura.setClickable(false);

        if(this.unidadeEdificacaoVO == null){
            this.unidadeEdificacaoVO = new UnidadeEdificacaoVO();
        }

        if(this.tipoEstruturas == null){
            this.tipoEstruturas = new ArrayList<BasicValueObject>();
        }

        if(this.tipoCoberturas == null){
            this.tipoCoberturas = new ArrayList<BasicValueObject>();
        }

        if(this.padraoEconomicos == null){
            this.padraoEconomicos = new ArrayList<BasicValueObject>();
        }

        if(this.tipoConservacao == null){
            this.tipoConservacao = new ArrayList<BasicValueObject>();
        }

        this.formUnidadeEdificacaoSpinnerTipoEstrutura  .setAdapter(new BasicSpinnerAdapter(getActivity(), tipoEstruturas));
        this.formUnidadeEdificacaoSpinnerTipoCobertura  .setAdapter(new BasicSpinnerAdapter(getActivity(), tipoCoberturas));
        this.formUnidadeEdificacaoSpinnerPadraoEconomico.setAdapter(new BasicSpinnerAdapter(getActivity(), padraoEconomicos));
        this.formUnidadeEdificacaoSpinnerTipoConservacao.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoConservacao));

    }

    @Override
    public void save() {
        this.unidadeEdificacaoVO.setObservacoes(this.formUnidadeEdificacaoObservacoes.getText().toString());

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "UNIDADE_EDIFICACAO");
        intent.putExtra("RESULT_DATA", this.unidadeEdificacaoVO);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);

        if(method != null){
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.EDIFICACAO_TIPO_ESTRUTURA.name())){
                this.tipoEstruturas = (List<BasicValueObject>) object;
                this.formUnidadeEdificacaoSpinnerTipoEstrutura.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoEstruturas));
                updateUI(false);
            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.EDIFICACAO_TIPO_COBERTURA.name())){
                this.tipoCoberturas = (List<BasicValueObject>) object;
                this.formUnidadeEdificacaoSpinnerTipoCobertura.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoCoberturas));
                updateUI(false);
            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.EDIFICACAO_PADRAO_ECONOMICO.name())){
                this.padraoEconomicos = (List<BasicValueObject>) object;
                this.formUnidadeEdificacaoSpinnerPadraoEconomico.setAdapter(new BasicSpinnerAdapter(getActivity(), padraoEconomicos));
                updateUI(false);
            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.EDIFICACAO_TIPO_CONSERVACAO.name())){
                this.tipoConservacao = (List<BasicValueObject>) object;
                this.formUnidadeEdificacaoSpinnerTipoConservacao.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoConservacao));
                updateUI(false);
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }

    public String checkDate(Date date){

        if(date == null){
            return "";
        }

        String format = DateFormat.format("dd/MM/yyyy", date).toString();
        if(format.equals("01/01/1902")){
            return "";
        }

        return format;
    }
}
