package br.com.matheusdenis.formulario;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.UnidadeTerrenoManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.UnidadeTerrenoVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentFromUnidadeTerreno extends DataBaseCoreFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, View.OnTouchListener {

    private UnidadeTerrenoVO unidadeTerrenoVO;

    private EditText formUnidadeTerrenoNomeLoteamento;
    private EditText formUnidadeTerrenoZona;
    private EditText formUnidadeTerrenoSetor;
    private EditText formUnidadeTerrenoQuadra;
    private EditText formUnidadeTerrenoLote;
    private EditText formUnidadeTerrenoFrente;
    private EditText formUnidadeTerrenoFundos;
    private EditText formUnidadeTerrenoAreaTotal;
    private EditText formUnidadeTerrenoObservacoes;
    private Spinner  formUnidadeTerrenoSpinnerPotencialidade;

    private NumberPickerBuilder numberPickerDialog;

    private List<BasicValueObject> potencialidades;

    public static FragmentFromUnidadeTerreno newInstance(UnidadeTerrenoVO unidadeTerrenoVO){

        if(unidadeTerrenoVO == null){
            unidadeTerrenoVO = new UnidadeTerrenoVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(UnidadeTerrenoVO.DATAKEY, unidadeTerrenoVO);
        FragmentFromUnidadeTerreno fragmentFromUnidadeTerreno = new FragmentFromUnidadeTerreno();
        fragmentFromUnidadeTerreno.setArguments(bundle);
        return fragmentFromUnidadeTerreno;

    }

    @Override
    public void save() {
        this.unidadeTerrenoVO.setNomeLoteamento(this.formUnidadeTerrenoNomeLoteamento.getText().toString().toUpperCase());
        this.unidadeTerrenoVO.setZona(this.formUnidadeTerrenoZona.getText().toString().toUpperCase());
        this.unidadeTerrenoVO.setSetor(this.formUnidadeTerrenoSetor.getText().toString().toUpperCase());
        this.unidadeTerrenoVO.setQuadra(this.formUnidadeTerrenoQuadra.getText().toString().toUpperCase());
        this.unidadeTerrenoVO.setLote(this.formUnidadeTerrenoLote.getText().toString().toUpperCase());
        this.unidadeTerrenoVO.setObservacoes(this.formUnidadeTerrenoObservacoes.getText().toString().toUpperCase());

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "UNIDADE_TERRENO");
        intent.putExtra("RESULT_DATA", this.unidadeTerrenoVO);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    protected void disableElements() {

    }

    public void setCallback(DataBaseResultDelegate callback){
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setDataBaseManagement(new UnidadeTerrenoManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.unidadeTerrenoVO = (UnidadeTerrenoVO) savedInstanceState.getSerializable(UnidadeTerrenoVO.DATAKEY);
            this.potencialidades  = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.TERRENO_POTENCIALIDADE.name())).getList();

            if(this.unidadeTerrenoVO == null){
                this.unidadeTerrenoVO = new UnidadeTerrenoVO();
            }

            if(this.potencialidades == null){
                this.potencialidades = new ArrayList<BasicValueObject>();
            }

        }else{
            this.unidadeTerrenoVO = (UnidadeTerrenoVO) getArguments().getSerializable(UnidadeTerrenoVO.DATAKEY);
            this.potencialidades  = new ArrayList<BasicValueObject>();
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.TERRENO_POTENCIALIDADE.name());
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(UnidadeTerrenoVO.DATAKEY, this.unidadeTerrenoVO);
        outState.putSerializable(DropDownKey.TERRENO_POTENCIALIDADE.name(), new SerializableBundle<BasicValueObject>(this.potencialidades));
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_unidade_terreno, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
            this.formUnidadeTerrenoNomeLoteamento.setText(this.unidadeTerrenoVO.getNomeLoteamento() == null ? "" : this.unidadeTerrenoVO.getNomeLoteamento().toUpperCase());
            this.formUnidadeTerrenoZona          .setText(this.unidadeTerrenoVO.getZona()           == null ? "" : this.unidadeTerrenoVO.getZona().toUpperCase());
            this.formUnidadeTerrenoSetor         .setText(this.unidadeTerrenoVO.getSetor()          == null ? "" : this.unidadeTerrenoVO.getSetor().toUpperCase());
            this.formUnidadeTerrenoQuadra        .setText(this.unidadeTerrenoVO.getQuadra()         == null ? "" : this.unidadeTerrenoVO.getQuadra().toUpperCase());
            this.formUnidadeTerrenoLote          .setText(this.unidadeTerrenoVO.getLote()           == null ? "" : this.unidadeTerrenoVO.getLote().toUpperCase());
            this.formUnidadeTerrenoObservacoes   .setText(this.unidadeTerrenoVO.getObservacoes()    == null ? "" : this.unidadeTerrenoVO.getObservacoes().toUpperCase());
        }

        this.formUnidadeTerrenoFrente.setText(String.format("%1$,.2f m", this.unidadeTerrenoVO.getFrente()));
        this.formUnidadeTerrenoFrente.setTag (this.unidadeTerrenoVO.getFrente());
        this.formUnidadeTerrenoFundos.setText(String.format("%1$,.2f m", this.unidadeTerrenoVO.getFundos()));
        this.formUnidadeTerrenoFundos.setTag (this.unidadeTerrenoVO.getFundos());
        this.formUnidadeTerrenoAreaTotal.setText(String.format("%1$,.2f m²", this.unidadeTerrenoVO.getAreaTotal()));
        this.formUnidadeTerrenoAreaTotal.setTag (this.unidadeTerrenoVO.getAreaTotal());

        int i = Utils.indexOf(potencialidades, this.unidadeTerrenoVO.getPotencialidade());
        i = i < 0? 0:i;

        this.formUnidadeTerrenoSpinnerPotencialidade.setOnItemSelectedListener(null);
        this.formUnidadeTerrenoSpinnerPotencialidade.setSelection(i);
        this.formUnidadeTerrenoSpinnerPotencialidade.setOnItemSelectedListener(this);
    }

    @SuppressLint("WrongViewCast")
    private void initializeVariables(View view){

        if(view == null){
            return;
        }

        this.formUnidadeTerrenoNomeLoteamento        = (EditText) view.findViewById(R.id.formUnidadeTerrenoNomeLoteamento);
        this.formUnidadeTerrenoZona                  = (EditText) view.findViewById(R.id.formUnidadeTerrenoZona);
        this.formUnidadeTerrenoSetor                 = (EditText) view.findViewById(R.id.formUnidadeTerrenoSetor);
        this.formUnidadeTerrenoQuadra                = (EditText) view.findViewById(R.id.formUnidadeTerrenoQuadra);
        this.formUnidadeTerrenoLote                  = (EditText) view.findViewById(R.id.formUnidadeTerrenoLote);
        this.formUnidadeTerrenoFrente                = (EditText) view.findViewById(R.id.formUnidadeTerrenoFrente);
        this.formUnidadeTerrenoFundos                = (EditText) view.findViewById(R.id.formUnidadeTerrenoFundos);
        this.formUnidadeTerrenoAreaTotal             = (EditText) view.findViewById(R.id.formUnidadeTerrenoAreaTotal);
        this.formUnidadeTerrenoObservacoes           = (EditText) view.findViewById(R.id.formUnidadeTerrenoObservacoes);
        this.formUnidadeTerrenoSpinnerPotencialidade = (Spinner)  view.findViewById(R.id.formUnidadeTerrenoSpinnerPotencialidade);

        this.formUnidadeTerrenoObservacoes.setOnTouchListener(this);

        this.formUnidadeTerrenoFrente     .setOnClickListener(this);
        this.formUnidadeTerrenoFundos     .setOnClickListener(this);
        this.formUnidadeTerrenoAreaTotal  .setOnClickListener(this);

        if(this.potencialidades == null){
            this.potencialidades = new ArrayList<BasicValueObject>();
        }

        this.formUnidadeTerrenoSpinnerPotencialidade.setAdapter(new BasicSpinnerAdapter(getActivity(), potencialidades));

    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.formUnidadeTerrenoFrente:
                this.numberPickerDialog = new NumberPickerBuilder()
                        .setFragmentManager(this.getActivity().getSupportFragmentManager())
                        .setMinNumber(0)
                        .setPlusMinusVisibility(View.GONE)
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                    @Override
                    public void onDialogNumberSet(int reference, int number, double decimal,
                                                  boolean isNegative, double fullNumber) {
                        formUnidadeTerrenoFrente.setText(String.format("%1$,.2f m", fullNumber));
                        formUnidadeTerrenoFrente.setTag(fullNumber);
                        unidadeTerrenoVO.setFrente(fullNumber);
                    }
                });
                this.numberPickerDialog.show();
                break;
            case R.id.formUnidadeTerrenoFundos:
                this.numberPickerDialog = new NumberPickerBuilder()
                        .setFragmentManager(this.getActivity().getSupportFragmentManager())
                        .setMinNumber(0)
                        .setPlusMinusVisibility(View.GONE)
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                    @Override
                    public void onDialogNumberSet(int reference, int number, double decimal,
                                                  boolean isNegative, double fullNumber) {
                        formUnidadeTerrenoFundos.setText(String.format("%1$,.2f m", fullNumber));
                        formUnidadeTerrenoFundos.setTag(fullNumber);
                        unidadeTerrenoVO.setFundos(fullNumber);
                    }
                });
                this.numberPickerDialog.show();
                break;
            case R.id.formUnidadeTerrenoAreaTotal:
                this.numberPickerDialog = new NumberPickerBuilder()
                        .setFragmentManager(this.getActivity().getSupportFragmentManager())
                        .setMinNumber(0)
                        .setPlusMinusVisibility(View.GONE)
                        .setStyleResId(R.style.BetterPickersDialogFragment_Light);
                this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                    @Override
                    public void onDialogNumberSet(int reference, int number, double decimal,
                                                  boolean isNegative, double fullNumber) {
                        formUnidadeTerrenoAreaTotal.setText(String.format("%1$,.2f m²", fullNumber));
                        formUnidadeTerrenoAreaTotal.setTag(fullNumber);
                        unidadeTerrenoVO.setAreaTotal(fullNumber);
                    }
                });
                this.numberPickerDialog.show();
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch(adapterView.getId()){
            case R.id.formUnidadeTerrenoSpinnerPotencialidade:
                if(this.potencialidades == null || this.potencialidades.isEmpty()){
                    return;
                }
                this.unidadeTerrenoVO.setPotencialidade((DropDownItemVO) this.potencialidades.get(i));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);

        if(method != null){
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY) + "." + DropDownKey.TERRENO_POTENCIALIDADE.name())){
                this.potencialidades = (List<BasicValueObject>) object;
                this.formUnidadeTerrenoSpinnerPotencialidade.setAdapter(new BasicSpinnerAdapter(getActivity(), potencialidades));
                updateUI(false);
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (view.getId() == R.id.formUnidadeTerrenoObservacoes) {
            ScrollView parent = (ScrollView) getActivity().findViewById(R.id.places_layout).findViewById(R.id.scrollView);
            parent.requestDisallowInterceptTouchEvent(true);
            switch (motionEvent.getAction()&MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_UP:
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                case MotionEvent.ACTION_DOWN:
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
        }

        return false;
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
