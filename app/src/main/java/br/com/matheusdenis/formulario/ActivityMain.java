package br.com.matheusdenis.formulario;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.BubbleIconFactory;
import com.google.maps.android.ui.IconGenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import br.com.database.Activity.DataBaseCoreActivity;
import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.LocalCoreDataDAO;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.ImovelManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.genericdata.appmsg.AppMsg;
import br.com.genericdata.valueobjects.ValidatorVO;
import br.com.matheusdenis.formulario.ValueObjects.UnidadeGrid;
import br.com.matheusdenis.formulario.ValueObjects.UnidadeMapa;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;

public class ActivityMain extends DataBaseCoreActivity {

    private GoogleMap googleMap;
    private boolean showMenu = true;
    private boolean sync = false;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            googleMap.setOnMapClickListener(null);
            setOnMapChangeListener();
            setOnMarkerClick();
            showMenu = true;
            invalidateOptionsMenu();
            sendToDataBase(MethodTag.GETALL);


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(googleMap.getCameraPosition().target) // Center Set
                            .zoom(9.0F)//googleMap.getCameraPosition().zoom)                // Zoom
                            .build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            });
        }
    };

    private BroadcastReceiver broadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent == null || intent.getExtras() == null){
                return;
            }

            googleMap.setOnMapClickListener(null);
            unSetOnMapChangeListener();
            unSetOnMarkerClick();
            double lat = intent.getDoubleExtra("LATITUDE", 0D);
            double lon = intent.getDoubleExtra("LONGITUDE", 0D);
            marcarMapaPorCoordenada(lat, lon);
        }
    };

    private BroadcastReceiver broadcastReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent == null || intent.getExtras() == null){
                return;
            }

            googleMap.setOnMapClickListener(getOnMapClickListener());
            unSetOnMapChangeListener();
            unSetOnMarkerClick();
            double lat = intent.getDoubleExtra("LATITUDE", 0D);
            double lon = intent.getDoubleExtra("LONGITUDE", 0D);
            marcarMapaPorCoordenada(lat, lon);
        }
    };

    private BroadcastReceiver broadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent == null || intent.getExtras() == null){
                return;
            }

            EnderecoImovelVO enderecoImovelVO1 = (EnderecoImovelVO) intent.getExtras().get(EnderecoImovelVO.DATAKEY);

            if(enderecoImovelVO1 == null){
                return;
            }

            getLatitudeLongitudeFromLocationName(enderecoImovelVO1, intent.getExtras().getBoolean("onlyLatLong", false), context);

        }
    };
    private int unidadesEncontradas = 0;
    private int unidadesBuscadas = 0;
    private AppMsg appMsgCarregando;
    private AppMsg appMsgInternet;
    private List<ImovelVO> imoveis;
    private List<UnidadeGrid> grids;
    private HashMap<Marker, Object> markerHash;
    private Spinner filter;
    private List<BasicValueObject> filters;

    public void getLatitudeLongitudeFromLocationName(EnderecoImovelVO enderecoImovelVOInput, boolean onlyLatLong, final Context context){

        if(!isNetworkConnected()){
            Toast.makeText(getApplicationContext(), "Não é possível recuperar as informações do endereço, verifique a sua conexão", Toast.LENGTH_SHORT).show();
            return;
        }

        try {

            String endereco = enderecoImovelVOInput.getNumero()+ " " + enderecoImovelVOInput.getLogradouro() + "," + enderecoImovelVOInput.getMunicipio() + "," + enderecoImovelVOInput.getUF() +", Brazil" ;

            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocationName(endereco,1);
            StringBuilder sb = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);

                if(!onlyLatLong) {
                    EnderecoImovelVO enderecoImovelVO = new EnderecoImovelVO(0L, "", enderecoImovelVOInput.getTipoImovel(), address.getThoroughfare(), address.getSubThoroughfare(),
                            address.getPostalCode(), address.getSubLocality(), address.getSubAdminArea(), address.getAddressLine(1).substring(address.getAddressLine(1).length() - 2, address.getAddressLine(1).length()), address.getLatitude(),
                            address.getLongitude(), false);

                    Intent intent = new Intent("UPDATE_ENDERECO_VO");
                    intent.putExtra(EnderecoImovelVO.DATAKEY, enderecoImovelVO);
                    intent.putExtra("BOOL", false);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }else{

                    if((enderecoImovelVOInput.getNumero() != null && !enderecoImovelVOInput.getNumero().equals("")) && (address.getSubThoroughfare() == null || address.getSubThoroughfare().equals(""))){
                        AlertDialog alertDialog = new AlertDialog.Builder(ActivityMain.this).setTitle("Aviso!").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).setMessage("Endereço não localizado!").create();
                        alertDialog.show();
                    }

                    enderecoImovelVOInput.setLatitude(address.getLatitude());
                    enderecoImovelVOInput.setLongitude(address.getLongitude());

                    Intent intent = new Intent("UPDATE_ENDERECO_VO");
                    intent.putExtra(EnderecoImovelVO.DATAKEY, enderecoImovelVOInput);
                    intent.putExtra("BOOL", false);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

                marcarMapaPorCoordenada(address.getLatitude(), address.getLongitude());

            }else{
                AlertDialog alertDialog = new AlertDialog.Builder(ActivityMain.this).setTitle("Aviso!").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setMessage("Endereço não localizado!").create();
                alertDialog.show();
            }

        } catch (IOException e) {
            AlertDialog alertDialog = new AlertDialog.Builder(ActivityMain.this).setTitle("Aviso!").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setMessage("Não foi possível buscar os dados do endereço, verifique os dados da busca ou a conexão com a internet!").create();
            alertDialog.show();
        }
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver( broadcastReceiver, new IntentFilter("GETALLIMOVELVO"));
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver( broadcastReceiver2, new IntentFilter("MARKLOCATIONONMAP"));
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver( broadcastReceiver3, new IntentFilter("GETLATITUDEFROMANDDRESS"));
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver( broadcastReceiver4, new IntentFilter("MARKLOCATIONONMAP2"));
        super.onResume();
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver2);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver3);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver4);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setServerManagerDelegate(new ServerConnectManager(this, this));
        this.setDataBaseManagement(new ImovelManagement(this, this));
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        this.filter = (Spinner) findViewById(R.id.mainFilter);
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_layout);

        appMsgCarregando = AppMsg.makeText(ActivityMain.this, "Carregando...", new AppMsg.Style(AppMsg.LENGTH_STICKY, R.color.black_scale_tree));
        appMsgCarregando.setParent(R.id.map_layout);

        appMsgInternet = AppMsg.makeText(ActivityMain.this, "Não foi possível conectar com a internet.", new AppMsg.Style(AppMsg.LENGTH_STICKY, R.color.black_scale_tree));
        appMsgInternet.setParent(R.id.map_layout);

        googleMap = fm.getMap();
            if(googleMap != null){
                googleMap.setMyLocationEnabled(true);

                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(-23.5508811, -46.633113)) // Center Set
                                .zoom(11.0f)                // Zoom
                                .build();                   // Creates a CameraPosition from the builder
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        setOnMarkerClick();
                    }
                });
                sendToDataBase(MethodTag.GETALL);
                new DropDownItemManagement(this, this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.TIPO_IMOVEL.name());

        }

    }

    public void pontuarMapa(List<ImovelVO> imovelVO, boolean clear){
        try{
            if(clear) {
                googleMap.clear();
                markerHash = new HashMap<Marker, Object>();
            }

            if(markerHash == null){
                markerHash = new HashMap<Marker, Object>();
            }
            unidadesEncontradas = imovelVO.size();
            for (ImovelVO  imovelVO1:imovelVO)
            {
                Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(imovelVO1.getEnderecoImovelVO().getLatitude(), imovelVO1.getEnderecoImovelVO().getLongitude()))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.unidade)));
                markerHash.put(marker, imovelVO1);
            }
            invalidateOptionsMenu();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void marcarMapaPorCoordenada(double lat, double lon){
        showMenu = false;
        invalidateOptionsMenu();
        googleMap.clear();
        markerHash = new HashMap<Marker, Object>();
        googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.unidade)));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lon)) // Center Set
                .zoom(18.0F)//googleMap.getCameraPosition().zoom)                // Zoom
                .build();                   // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(
                //!showMenu ||
                googleMap == null){
            return false;
        }
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.unidadesEncontradas).setTitle("" + (this.unidadesEncontradas + this.unidadesBuscadas));
        menu.findItem(R.id.unidadesEncontradas).setVisible(showMenu);
        menu.findItem(R.id.add).setVisible(showMenu);
        menu.findItem(R.id.sync).setVisible(showMenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.add:

                invalidateOptionsMenu();
                if(googleMap == null){
                    return false;
                }

                final Location latitude = googleMap.getMyLocation();

                if(latitude == null){
                    return false;
                }

                final EnderecoImovelVO[] enderecoImovelVO = {new EnderecoImovelVO()};
                final ImovelVO imovelVO = new ImovelVO();

                AlertDialog alertDialog = new AlertDialog.Builder(ActivityMain.this).create();
                alertDialog.setTitle("Aviso!");
                alertDialog.setMessage("Deseja utilizar a localização atual?");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                try {


                                    if(isNetworkConnected()) {

                                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                        List<Address> addresses = geocoder.getFromLocation(latitude.getLatitude(), latitude.getLongitude(), 1);
                                        StringBuilder sb = new StringBuilder();
                                        if (addresses.size() > 0) {
                                            Address address = addresses.get(0);
                                            enderecoImovelVO[0] = new EnderecoImovelVO(0L, "", null, address.getThoroughfare(), address.getSubThoroughfare(),
                                                    address.getPostalCode(), address.getSubLocality(), address.getSubAdminArea(), address.getAddressLine(1).substring(address.getAddressLine(1).length() - 2, address.getAddressLine(1).length()), address.getLatitude(),
                                                    address.getLongitude(), false);

                                            Intent intent = new Intent("UPDATE_ENDERECO_VO");
                                            intent.putExtra(EnderecoImovelVO.DATAKEY, enderecoImovelVO[0]);
                                            intent.putExtra("BOOL", true);
                                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                                        }
                                    }else{
                                        enderecoImovelVO[0] = new EnderecoImovelVO(0L, "", null, "", "",
                                                "", "", "", "", latitude.getLatitude(), latitude.getLongitude(), false);
                                    }

                                } catch (IOException e) {
                                    enderecoImovelVO[0] = new EnderecoImovelVO(0L, "", null, "", "",
                                            "", "", "", "", latitude.getLatitude(), latitude.getLongitude(), false);
                                }

                                try {
                                    googleMap.setOnMapClickListener(getOnMapClickListener());
                                    unSetOnMapChangeListener();
                                    unSetOnMarkerClick();
                                }catch(Exception e){
                                    e.printStackTrace();
                                }

                                imovelVO.setEnderecoImovelVO(enderecoImovelVO[0]);
                                getSupportFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentFormSoEndereco.newInstance(imovelVO)).commit();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        marcarMapaPorCoordenada(latitude.getLatitude(), latitude.getLongitude());

                                        googleMap.setOnMapClickListener(getOnMapClickListener());
                                        unSetOnMapChangeListener();
                                        unSetOnMarkerClick();
                                    }
                                });
                            }
                        }).start();
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if(isNetworkConnected()){
                            enderecoImovelVO[0] = new EnderecoImovelVO(0L, "", null, "", "",
                                    "", "", "", "", latitude.getLatitude(), latitude.getLongitude(), false);

                            try{
                                googleMap.setOnMapClickListener(getOnMapClickListener());
                                unSetOnMapChangeListener();
                                unSetOnMarkerClick();
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            imovelVO.setEnderecoImovelVO(enderecoImovelVO[0]);
                            getSupportFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentFormSoEndereco.newInstance(imovelVO)).commit();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    marcarMapaPorCoordenada(latitude.getLatitude(), latitude.getLongitude());
                                    googleMap.setOnMapClickListener(getOnMapClickListener());
                                    unSetOnMapChangeListener();
                                    unSetOnMarkerClick();

                                }
                            });
                            dialogInterface.dismiss();
                        }else{
                            dialogInterface.dismiss();
                            AlertDialog alertDialog = new AlertDialog.Builder(ActivityMain.this).create();
                            alertDialog.setTitle("Aviso!");
                            alertDialog.setMessage("Problemas com a conexão com a internet\nVerifique a sua conexão e tente novamente");
                            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog.show();
                        }
                    }
                });
                alertDialog.show();
                break;
            case R.id.logout:
                LocalCoreDataDAO.save(this, null, "usuarioLoginOPASUIDHPOSUFHdfjghdifgh");
                Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                startActivity(intent);
                finish();
                break;
            case android.R.id.home:
                finish();
                break;
            case R.id.sync:
                sync = true;
                sendToDataBase(MethodTag.GETALL);
                break;
        }

        return true;
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.GETALL))){
                imoveis = (List<ImovelVO>) object;
                if(sync && imoveis != null && !imoveis.isEmpty()){
                    sync = false;
                    if(imoveis != null && !imoveis.isEmpty()){
                        new ServerConnectManager(getApplicationContext(), this).save(imoveis.get(0));
                    }
                }else{
                    if(imoveis == null || imoveis.isEmpty()){
                        sendGetToServer();
                    }
                    pontuarMapa((List<ImovelVO>) object, true);
                    this.getSupportFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentPlaces.newInstance(imoveis, grids)).commit();
                    setOnMapChangeListener();
                }

            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY) + "." + DropDownKey.TIPO_IMOVEL.name())){
                this.filters = (List<BasicValueObject>) object;
                ((DropDownItemVO)this.filters.get(0)).setValue("Todos os tipos");
                this.filter.setAdapter(new BasicSpinnerAdapter(getApplicationContext(), filters));
                this.filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        sendGetToServer();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }
        if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.DELETE))){
            if(sync){
                sync = true;
                sendToDataBase(MethodTag.GETALL);
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {

    }

    public GoogleMap.OnMapClickListener getOnMapClickListener(){
        return new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(final LatLng latLng) {
                googleMap.clear();
                markerHash = new HashMap<Marker, Object>();
                googleMap.addMarker(new MarkerOptions().position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.unidade)));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)
                         .zoom(googleMap.getCameraPosition().zoom)// Center Set
                        .build();                   // Creates a CameraPosition from the builder
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                            StringBuilder sb = new StringBuilder();
                            if (addresses.size() > 0) {
                                Address address = addresses.get(0);
                                EnderecoImovelVO enderecoImovelVO = new EnderecoImovelVO(0L, "", null, address.getThoroughfare(), address.getSubThoroughfare(),
                                        address.getPostalCode(), address.getSubLocality(), address.getSubAdminArea(), address.getAddressLine(1).substring(address.getAddressLine(1).length() - 2, address.getAddressLine(1).length()), address.getLatitude(),
                                        address.getLongitude(), false);

                                Intent intent = new Intent("UPDATE_ENDERECO_VO");
                                intent.putExtra(EnderecoImovelVO.DATAKEY, enderecoImovelVO);
                                intent.putExtra("BOOL", true);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                            }else{
                                EnderecoImovelVO enderecoImovelVO = new EnderecoImovelVO(0L, "", null, "", "",
                                        "", "", "", "", latLng.latitude,
                                        latLng.longitude, true);

                                Intent intent = new Intent("UPDATE_ENDERECO_VO");
                                intent.putExtra(EnderecoImovelVO.DATAKEY, enderecoImovelVO);
                                intent.putExtra("BOOL", true);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            }

                        } catch (IOException e) {
                            EnderecoImovelVO enderecoImovelVO = new EnderecoImovelVO(0L, "", null, "", "",
                                    "", "", "", "", latLng.latitude,
                                    latLng.longitude, true);

                            Intent intent = new Intent("UPDATE_ENDERECO_VO");
                            intent.putExtra("BOOL", true);
                            intent.putExtra(EnderecoImovelVO.DATAKEY, enderecoImovelVO);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        }
                    }
                }).start();
            }
        };
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, final List<?> resultList, Object result, Object... parameters) {
        if(appMsgCarregando != null && appMsgCarregando.isShowing()){
            appMsgCarregando.cancel();
        }
        if(serviceName.equals(getString(R.string.getUnidadesMapa_soap_action))){
            if(resultList != null){
                marcarComUnidadeMapaVO((List<UnidadeMapa>) resultList, imoveis);
            }
        }
        if(serviceName.equals(getString(R.string.getUnidadesGrid_soap_action))) {
            if (resultList != null) {
                grids = (List<UnidadeGrid>) resultList;
                unidadesBuscadas = resultList.size();
                invalidateOptionsMenu();
                this.getSupportFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentPlaces.newInstance(imoveis, grids)).commit();
            }
        }

        if(serviceName.equals(getString(R.string.getUnidade_soap_action))){
            if(result != null) {
                Intent intent = new Intent("MARKLOCATIONONMAP2");
                intent.putExtra("LATITUDE", ((ImovelVO) result).getEnderecoImovelVO() != null ? ((ImovelVO) result).getEnderecoImovelVO().getLatitude() : 0D);
                intent.putExtra("LONGITUDE", ((ImovelVO) result).getEnderecoImovelVO() != null ? ((ImovelVO) result).getEnderecoImovelVO().getLongitude() : 0D);
                getSupportFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentVisual.newInstance(((ImovelVO) result))).commit();
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                googleMap.setOnMapClickListener(null);
                unSetOnMarkerClick();
                unSetOnMapChangeListener();
            }
        }

        if(serviceName.equals(getString(R.string.saveUnidade_soap_action))){
            sync = true;

            if(result.equals("true")){
                if(imoveis != null && !imoveis.isEmpty()){

                    List<ImagemVO> imagemVOs = imoveis.get(0).getImagemVOs();

                    if(imagemVOs != null && !imagemVOs.isEmpty()){
                        for (ImagemVO imagemVO : imagemVOs){
                            if(imagemVO.getImagePath() != null && !imagemVO.getImagePath().equals("")){
                                uploadFile(new File(imagemVO.getImagePath()));
                            }
                        }
                    }

                    sendToDataBase(MethodTag.DELETE, imoveis.get(0).getId());
                }else{
                    sendToDataBase(MethodTag.GETALL);
                }
            }
        }
    }

    public void uploadFile(final File fileName){

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(fileName);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);

        if(!isNetworkConnected()){
            return;
        }

        if(!fileName.exists()){
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                FTPClient client = new FTPClient();

                try {

                    client.connect("ftpimg.geoembraesp.com.br",2211);
                    client.login("ftpimg", "ftp@geoimg");
                    client.setType(FTPClient.TYPE_BINARY);
                    client.setAutoNoopTimeout(0);
                    //client.changeDirectory("/upload/");

                    client.upload(fileName, new FTPDataTransferListener() {
                        @Override
                        public void started() {
                            Log.wtf("FTP_UPLOAD", "STARTED");
                        }

                        @Override
                        public void transferred(int i) {
                            Log.wtf("FTP_UPLOAD", "STATUS: " + i);
                        }

                        @Override
                        public void completed() {
                            Log.wtf("FTP_UPLOAD", "COMPLETED");
                        }

                        @Override
                        public void aborted() {
                            Log.wtf("FTP_UPLOAD", "ABORTED");
                        }

                        @Override
                        public void failed() {
                            Log.wtf("FTP_UPLOAD", "FAILED");
                            Crashlytics.log("UPLOAD FTP FALHOU");
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.wtf("DEU RUIM", e.toString());
                    Crashlytics.log(e.getMessage());
                    try {
                        client.disconnect(true);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {
        if(appMsgCarregando != null && appMsgCarregando.isShowing()){
            appMsgCarregando.cancel();
        }

        if(serviceName.equals(getString(R.string.getUnidadesMapa_soap_action))) {
            AppMsg.makeText(ActivityMain.this, "Não foi possível receber os Imóveis", AppMsg.STYLE_ALERT, AppMsg.LENGTH_SHORT).show();
        }

        if(serviceName.equals(getString(R.string.getUnidadesGrid_soap_action))) {
            AppMsg.makeText(ActivityMain.this, "Não foi possível receber os Imóveis", AppMsg.STYLE_ALERT, AppMsg.LENGTH_SHORT).show();
        }

        if(serviceName.equals(getString(R.string.saveUnidade_soap_action))){
            sync = false;
            AppMsg.makeText(ActivityMain.this, "Erro ao sincronizar, tente novamente!", AppMsg.STYLE_ALERT, AppMsg.LENGTH_SHORT).show();
        }

        if(serviceName.equals(getString(R.string.getUnidade_soap_action))){
            AppMsg.makeText(ActivityMain.this, "Erro ao capturar as informações do servidor, tente novamente mais tarde!", AppMsg.STYLE_ALERT, AppMsg.LENGTH_SHORT).show();
            setOnMapChangeListener();
        }
    }

    public void setOnMapChangeListener(){
        if(googleMap == null){
            return;
        }

        if(filter != null){
            filter.setVisibility(View.VISIBLE);
            findViewById(R.id.textFilter).setVisibility(View.VISIBLE);
        }

        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                sendGetToServer();
            }
        });
    }

    public void sendGetToServer(){

        if(!isNetworkConnected()){
            if(appMsgInternet != null && !appMsgInternet.isShowing()){
                appMsgInternet.show();
            }
        }else {

            if (!isNetworkConnected()) {
                if (appMsgInternet != null && !appMsgInternet.isShowing()) {
                    appMsgInternet.show();
                }
            } else {

                if (filters == null || filters.isEmpty()) {
                    return;
                }
                long filtro = 0;
                try {
                    filtro = ((DropDownItemVO) filters.get(filter.getSelectedItemPosition())).getIdServer();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                new ServerConnectManager(getApplicationContext(), ActivityMain.this).get(googleMap, filtro);
                if (appMsgCarregando != null && !appMsgCarregando.isShowing()) {
                    appMsgCarregando.show();
                }
                if (appMsgInternet != null && appMsgInternet.isShowing()) {
                    appMsgInternet.cancel();
                }
            }
        }
    }

    public void unSetOnMapChangeListener(){
        if(googleMap == null){
            return;
        }

        if(filter != null){
            filter.setVisibility(View.GONE);
            findViewById(R.id.textFilter).setVisibility(View.GONE);
        }

        if(appMsgInternet != null && appMsgInternet.isShowing()){
            appMsgInternet.cancel();
        }

        googleMap.setOnCameraChangeListener(null);
    }

    public void setOnMarkerClick(){
        if(googleMap == null){
            return;
        }

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if(markerHash == null){
                    return false;
                }

                Object o = markerHash.get(marker);

                if(o == null){
                    return false;
                }

                if(o instanceof ImovelVO){
                    try {
                        unSetOnMapChangeListener();
                        unSetOnMarkerClick();
                        googleMap.setOnMapClickListener(null);
                        marcarMapaPorCoordenada(((ImovelVO) o).getEnderecoImovelVO().getLatitude(), ((ImovelVO) o).getEnderecoImovelVO().getLongitude());
                        getSupportFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentVisual.newInstance((ImovelVO) o)).commit();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    return true;
                }

                if(o instanceof UnidadeMapa){
                    try {
                        UnidadeMapa unidadeMapa = (UnidadeMapa) o;
                        if(unidadeMapa.getId() > 0){
                            unSetOnMapChangeListener();
                            unSetOnMarkerClick();
                            googleMap.setOnMapClickListener(null);
                            new ServerConnectManager(getApplicationContext(), ActivityMain.this).set(unidadeMapa.getId());
                        }else{
                            setOnMapChangeListener();
                            float zoom = 1f;
                            if(googleMap.getCameraPosition().zoom < 7){
                                zoom = 7;
                            }else{
                                if(googleMap.getCameraPosition().zoom < 10){
                                    zoom = 10;
                                }else{
                                    if(googleMap.getCameraPosition().zoom < 13){
                                        zoom = 13;
                                    }else{
                                        if(googleMap.getCameraPosition().zoom <= 15){
                                            zoom = 15.1f;
                                        }else{
                                            zoom = googleMap.getCameraPosition().zoom;
                                        }
                                    }
                                }
                            }
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(marker.getPosition())
                                    .zoom(zoom)// Center Set
                                    .build();                   // Creates a CameraPosition from the builder
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void unSetOnMarkerClick(){
        if(googleMap == null){
            return;
        }
        googleMap.setOnMarkerClickListener(null);
    }

    public void marcarComUnidadeMapaVO(final List<UnidadeMapa> unidadeMapas, final List<ImovelVO> imovelVOList){
        new Thread(new Runnable() {
            @Override
            public void run() {

                final List<MarkerOptions> markerOptionses = new ArrayList<MarkerOptions>();
                for(UnidadeMapa unidadeMapa : unidadeMapas){

                    IconGenerator iconGenerator = new IconGenerator(getApplicationContext());
                    if(unidadeMapa.getBgColor() != null && !unidadeMapa.getBgColor().equals("")){
                        try {
                            switch (Integer.parseInt(unidadeMapa.getBgColor())) {
                                case IconGenerator.STYLE_DEFAULT:
                                    iconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
                                    break;
                                case IconGenerator.STYLE_WHITE:
                                    iconGenerator.setStyle(IconGenerator.STYLE_WHITE);
                                    break;
                                case IconGenerator.STYLE_RED:
                                    iconGenerator.setStyle(IconGenerator.STYLE_RED);
                                    break;
                                case IconGenerator.STYLE_BLUE:
                                    iconGenerator.setStyle(IconGenerator.STYLE_BLUE);
                                    break;
                                case IconGenerator.STYLE_GREEN:
                                    iconGenerator.setStyle(IconGenerator.STYLE_GREEN);
                                    break;
                                case IconGenerator.STYLE_PURPLE:
                                    iconGenerator.setStyle(IconGenerator.STYLE_PURPLE);
                                    break;
                                case IconGenerator.STYLE_ORANGE:
                                    iconGenerator.setStyle(IconGenerator.STYLE_ORANGE);
                                    break;
                                default:
                                    iconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
                                    break;
                            }
                        }catch(Exception e){
                            iconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
                        }
                    }

                    final MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(unidadeMapa.getLatitude(), unidadeMapa.getLongitude()))
                            .icon(unidadeMapa.getId() != 0 ?
                                    BitmapDescriptorFactory.fromResource(R.drawable.unidade) :
                                    BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon(unidadeMapa.getLabel())));

                    markerOptionses.add(markerOptions);


                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        googleMap.clear();
                        markerHash = new HashMap<Marker, Object>();
                        pontuarMapa(imovelVOList, false);
                        for (MarkerOptions markerOptions : markerOptionses){
                            Marker marker = googleMap.addMarker(markerOptions);
                            markerHash.put(marker, unidadeMapas.get(markerOptionses.indexOf(markerOptions)));
                        }
                    }
                });
            }
        }).start();
    }

}
