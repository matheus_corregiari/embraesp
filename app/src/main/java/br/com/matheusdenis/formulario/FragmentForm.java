package br.com.matheusdenis.formulario;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.ImovelManagement;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.database.ValueObjects.UnidadeComercializacaoVO;
import br.com.database.ValueObjects.UnidadeEdificacaoVO;
import br.com.database.ValueObjects.UnidadeTerrenoVO;
import br.com.database.ValueObjects.UnidadeVO;
import br.com.genericdata.valueobjects.ValidatorVO;

public class FragmentForm extends DataBaseCoreFragment implements View.OnClickListener {

    private View formUnidade;
    private View formEndereco;
    private View formUnidadeTerreno;
    private View formUnidadeComercializacao;
    private View formUnidadeEdificacao;
    private boolean jafoi = false;

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {
        if(serviceName.equals(getString(R.string.saveUnidade_soap_action))){
            jafoi = false;
            if(result.equals("true")){
                if(imovelVO.getId() != 0){
                    sendToDataBase(MethodTag.DELETE, imovelVO.getId());
                }
            }else{
                Toast.makeText(getActivity(), "Não foi possível enviar os dados para o Servidor, tente novamente.", Toast.LENGTH_SHORT).show();
            }


        }
    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {
        if(serviceName.equals(getString(R.string.saveUnidade_soap_action))){
            jafoi = false;
            Toast.makeText(getActivity(), "Não foi possível enviar os dados para o Servidor, tente novamente.", Toast.LENGTH_SHORT).show();
        }
    }

    public enum FragmentFormResult{
        ENDERECO_IMOVEL,
        UNIDADE,
        UNIDADE_COMERCIALIZACAO,
        UNIDADE_EDIFICACAO,
        IMAGEM,
        UNIDADE_TERRENO;
    };

    public enum FragmentFormStatus{
        RESULT_OK,
        RESULT_ERROR;
    };

    private ImovelVO imovelVO;
    private ImovelVO imovelVO2;

    private Button buttonVoltar;
    private Button buttonSalvar;
    private Button buttonExcluir;

    private int count = 0;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent == null || intent.getExtras() == null){
                return;
            }

            String status   = (String) intent.getExtras().get("STATUS");
            String fragment = (String) intent.getExtras().get("FRAGMENT");

            if(status == null && status.equals("")){
                return;
            }

            if(fragment == null && fragment.equals("")){
                return;
            }

            List<ImagemVO> imagemVOs = new ArrayList<ImagemVO>();
            BasicValueObject basicValueObject = null;
            if(fragment.equals(FragmentFormResult.IMAGEM.name())){
                imagemVOs = ((SerializableBundle<ImagemVO>)intent.getExtras().get("RESULT_DATA")).getList();
            }else{
                basicValueObject = (BasicValueObject) intent.getExtras().get("RESULT_DATA");
            }

            if(fragment.equals(FragmentFormResult.ENDERECO_IMOVEL.name())){

                EnderecoImovelVO basicValueObject1 = (EnderecoImovelVO) basicValueObject;

                if(basicValueObject1.getTipoImovel() != null && basicValueObject1.getTipoImovel().getIdServer() == 0L){
                    basicValueObject1.setTipoImovel(null);
                }

                imovelVO.setEnderecoImovelVO(status.equals(FragmentFormStatus.RESULT_ERROR.name()) ? null : basicValueObject1);
            }else {
                if (fragment.equals(FragmentFormResult.UNIDADE.name())) {

                    UnidadeVO basicValueObject1 = (UnidadeVO) basicValueObject;

                    if(basicValueObject1.getDadoOrigem() != null && basicValueObject1.getDadoOrigem().getIdServer() == 0L){
                        basicValueObject1.setDadoOrigem(null);
                    }

                    if(basicValueObject1.getDadoOrigem() == null || basicValueObject1.getDadoOrigem().getIdServer() == 0){
                        Toast.makeText(context, "Dado Origem é Obrigatório", Toast.LENGTH_SHORT).show();
                        count = 0;
                        return;
                    }

                    if(basicValueObject1.getSubTipoImovel() != null && basicValueObject1.getSubTipoImovel().getIdServer() == 0L){
                        basicValueObject1.setSubTipoImovel(null);
                    }

                    imovelVO.setUnidadeVO(status.equals(FragmentFormStatus.RESULT_ERROR.name()) ? null : basicValueObject1);
                } else {
                    if (fragment.equals(FragmentFormResult.UNIDADE_COMERCIALIZACAO.name())) {

                        UnidadeComercializacaoVO basicValueObject1 = (UnidadeComercializacaoVO) basicValueObject;

                        if(basicValueObject1.getFonte() != null && basicValueObject1.getFonte().getIdServer() == 0L){
                            basicValueObject1.setFonte(null);
                        }

                        if(basicValueObject1.getTipoLocacao() != null && basicValueObject1.getTipoLocacao().getIdServer() == 0L){
                            basicValueObject1.setTipoLocacao(null);
                        }

                        if(basicValueObject1.getTipoVenda() != null && basicValueObject1.getTipoVenda().getIdServer() == 0L){
                            basicValueObject1.setTipoVenda(null);
                        }

                        imovelVO.setUnidadeComercializacaoVO(status.equals(FragmentFormStatus.RESULT_ERROR.name()) ? null : basicValueObject1);
                    } else {
                        if (fragment.equals(FragmentFormResult.UNIDADE_EDIFICACAO.name())) {

                            UnidadeEdificacaoVO basicValueObject1 = (UnidadeEdificacaoVO) basicValueObject;

                            if(basicValueObject1.getPadraoEconomico() != null && basicValueObject1.getPadraoEconomico().getIdServer() == 0L){
                                basicValueObject1.setPadraoEconomico(null);
                            }

                            if(basicValueObject1.getTipo() != null && basicValueObject1.getTipo().getIdServer() == 0L){
                                basicValueObject1.setTipo(null);
                            }

                            if(basicValueObject1.getTipoCobertura() != null && basicValueObject1.getTipoCobertura().getIdServer() == 0L){
                                basicValueObject1.setTipoCobertura(null);
                            }

                            if(basicValueObject1.getTipoConservacao() != null && basicValueObject1.getTipoConservacao().getIdServer() == 0L){
                                basicValueObject1.setTipoConservacao(null);
                            }

                            if(basicValueObject1.getTipoEstrutura() != null && basicValueObject1.getTipoEstrutura().getIdServer() == 0L){
                                basicValueObject1.setTipoEstrutura(null);
                            }

                            imovelVO.setUnidadeEdificacaoVO(status.equals(FragmentFormStatus.RESULT_ERROR.name()) ? null : basicValueObject1);
                        } else {
                            if (fragment.equals(FragmentFormResult.UNIDADE_TERRENO.name())) {

                                UnidadeTerrenoVO basicValueObject1 = (UnidadeTerrenoVO) basicValueObject;

                                if(basicValueObject1.getPotencialidade() != null && basicValueObject1.getPotencialidade().getIdServer() == 0L){
                                    basicValueObject1.setPotencialidade(null);
                                }

                                imovelVO.setUnidadeTerrenoVO(status.equals(FragmentFormStatus.RESULT_ERROR.name()) ? null : basicValueObject1);
                            } else {
                                if (fragment.equals(FragmentFormResult.IMAGEM.name())) {
                                    imovelVO.setImagemVOs(status.equals(FragmentFormStatus.RESULT_ERROR.name()) ? new ArrayList<ImagemVO>() : imagemVOs);
                                }
                            }
                        }
                    }
                }
            }

            if(count < FragmentFormResult.values().length - 1){
                count ++;
            }else{
                save();
            }
        }
    };

    private BroadcastReceiver hideFragments = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent == null || intent.getExtras() == null){
                return;
            }

            boolean hideTerreno         = intent.getExtras().getBoolean("hideTerreno"        , false);
            boolean hideComercializacao = intent.getExtras().getBoolean("hideComercializacao", false);
            boolean hideUnidade         = intent.getExtras().getBoolean("hideUnidade"        , false);
            boolean hideEndereco        = intent.getExtras().getBoolean("hideEndereco"       , false);
            boolean hideEdificacao      = intent.getExtras().getBoolean("hideEdificacao"     , false);

            formUnidade.setVisibility(hideUnidade? View.GONE : View.VISIBLE);
            formEndereco.setVisibility(hideEndereco? View.GONE : View.VISIBLE);
            formUnidadeTerreno.setVisibility(hideTerreno? View.GONE : View.VISIBLE);
            formUnidadeComercializacao.setVisibility(hideComercializacao? View.GONE : View.VISIBLE);
            formUnidadeEdificacao.setVisibility(hideEdificacao? View.GONE : View.VISIBLE);

        }
    };


    @Override
    public void save() {
        this.imovelVO2 = this.imovelVO;
        this.sendToDataBase(MethodTag.SAVE, this.imovelVO);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver( broadcastReceiver, new IntentFilter("BROADCAST_FRAGMENT_FORM"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver( hideFragments, new IntentFilter("BROADCAST_HIDE_FRAGMENTS"));
        super.onResume();
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(hideFragments);
        super.onPause();
    }

    public static FragmentForm newInstance(ImovelVO imovelVO){

        if(imovelVO == null){
            imovelVO = new ImovelVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ImovelVO.DATAKEY, imovelVO);
        FragmentForm fragmentForm = new FragmentForm();
        fragmentForm.setArguments(bundle);
        return fragmentForm;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setUseSaveBroadcast(false);
        this.setDataBaseManagement(new ImovelManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.imovelVO = (ImovelVO) savedInstanceState.getSerializable(ImovelVO.DATAKEY);
            this.count = savedInstanceState.getInt("count");

            if(this.imovelVO == null){
                this.imovelVO = new ImovelVO();
            }

        }else{
            this.imovelVO = (ImovelVO) getArguments().getSerializable(ImovelVO.DATAKEY);
        }
        this.imovelVO2 = this.imovelVO;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImovelVO.DATAKEY, this.imovelVO);
        outState.putInt("count", this.count);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form, null);

        this.buttonVoltar = (Button) view.findViewById(R.id.buttonVoltar);
        this.buttonSalvar = (Button) view.findViewById(R.id.buttonSalvar);
        this.buttonExcluir = (Button) view.findViewById(R.id.buttonExcluir);

        this.formUnidade                = view.findViewById(R.id.formUnidade);
        this.formEndereco               = view.findViewById(R.id.formEndereco);
        this.formUnidadeTerreno         = view.findViewById(R.id.formUnidadeTerreno);
        this.formUnidadeComercializacao = view.findViewById(R.id.formUnidadeComercializacao);
        this.formUnidadeEdificacao      = view.findViewById(R.id.formUnidadeEdificacao);

        this.buttonVoltar.setOnClickListener(this);
        this.buttonSalvar.setOnClickListener(this);
        this.buttonExcluir.setOnClickListener(this);

        if(this.imovelVO.getId() > 0L){
            this.buttonSalvar.setText(R.string.finalizar_edicao);
            this.buttonExcluir.setVisibility(View.VISIBLE);
        }else{
            this.buttonSalvar.setText(R.string.salvar);
            this.buttonExcluir.setVisibility(View.GONE);
        }

        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormImagem                , FragmentFromImagem                .newInstance(this.imovelVO.getImagemVOs(), true))         .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormEndereco              , FragmentFromEnderecoImovel        .newInstance(this.imovelVO.getEnderecoImovelVO()))        .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidade               , FragmentFromUnidade               .newInstance(this.imovelVO.getUnidadeVO()))               .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidadeTerreno        , FragmentFromUnidadeTerreno        .newInstance(this.imovelVO.getUnidadeTerrenoVO()))        .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidadeEdificacao     , FragmentFromUnidadeEdificacao     .newInstance(this.imovelVO.getUnidadeEdificacaoVO()))     .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidadeComercializacao, FragmentFromUnidadeComercializacao.newInstance(this.imovelVO.getUnidadeComercializacaoVO())).commit();

        return view;
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.SAVE))){
                Toast.makeText(getActivity(), "Registro Salvo com Sucesso!", Toast.LENGTH_SHORT).show();
                if(!isNetworkConnected()){
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETALLIMOVELVO"));
                }else{
                    if(!jafoi) {
                        jafoi = true;
                        imovelVO = (ImovelVO) object;
                        imovelVO2 = imovelVO;
                        new ServerConnectManager(getActivity(), this).save(this.imovelVO);
                    }
                }

            }
            if(method.equals(this.getMethodName(getClassKey(), MethodTag.DELETE))){
                if(buttonExcluir.getVisibility() == View.VISIBLE){
                    Toast.makeText(getActivity(), "Imovel excluído com sucesso!", Toast.LENGTH_SHORT).show();
                }
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETALLIMOVELVO"));
            }
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.SAVE))){
                Toast.makeText(getActivity(), "Ocorreu um erro e o Imóvel não pode ser salvo, verifique os dados e tente novamente!", Toast.LENGTH_SHORT).show();
            }
            if(method.equals(this.getMethodName(getClassKey(), MethodTag.DELETE))){
                if(buttonExcluir.getVisibility() == View.VISIBLE){
                    Toast.makeText(getActivity(), "Ocorreu um erro!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.buttonVoltar:
                if(this.imovelVO.getId() == 0L && this.imovelVO.getIdServer() == 0L){
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETALLIMOVELVO"));
                }else{
                    this.getFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentVisual.newInstance(this.imovelVO2)).commit();
                }
                break;
            case R.id.buttonSalvar:

                if(imovelVO.getIdServer() > 0L){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setTitle("Atenção!").setCancelable(true).setPositiveButton("Correção", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            imovelVO.setTipoAtualizacao(1);
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("SAVE"));
                            dialogInterface.dismiss();
                        }
                    })
                            .setMessage("Esta é uma correção ou uma alteração?")
                            .setNegativeButton("Alteração", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            imovelVO.setTipoAtualizacao(2);
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("SAVE"));
                            dialog.dismiss();
                        }
                    }).create();
                    alertDialog.show();
                }else{
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("SAVE"));
                }

                break;
            case R.id.buttonExcluir:

                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Aviso!");
                alertDialog.setMessage("Deseja mesmo excluir esta Unidade?");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendToDataBase(MethodTag.DELETE, imovelVO.getId());
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();


                break;
        }
    }
}
