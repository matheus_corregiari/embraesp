package br.com.matheusdenis.formulario;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.EnderecoImovelManagement;
import br.com.database.Management.ImovelManagement;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.genericdata.valueobjects.ValidatorVO;
import br.com.matheusdenis.formulario.ValueObjects.UnidadeGrid;

public class FragmentPlaces extends DataBaseCoreFragment {

    private List<ImovelVO> imovelVOList;
    private List<UnidadeGrid> unidadeGridList;
    private ListView listView;

    public static FragmentPlaces newInstance(List<ImovelVO> imovelVOList, List<UnidadeGrid> unidadeGridList){

        if(imovelVOList == null){
            imovelVOList = new ArrayList<ImovelVO>();
        }

        if(unidadeGridList == null){
            unidadeGridList = new ArrayList<UnidadeGrid>();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ImovelVO.DATAKEY, new SerializableBundle<ImovelVO>(imovelVOList));
        bundle.putSerializable("GRID", new SerializableBundle<UnidadeGrid>(unidadeGridList));
        FragmentPlaces fragmentPlaces = new FragmentPlaces();
        fragmentPlaces.setArguments(bundle);
        return fragmentPlaces;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setUseSaveBroadcast(false);
        if(savedInstanceState == null){

            SerializableBundle<ImovelVO> imovelVOSerializableBundle = (SerializableBundle<ImovelVO>) getArguments().get(ImovelVO.DATAKEY);
            this.imovelVOList = (imovelVOSerializableBundle).getList();
            SerializableBundle<UnidadeGrid> unidadeGridSerializableBundle = (SerializableBundle<UnidadeGrid>) getArguments().get("GRID");
            this.unidadeGridList = (unidadeGridSerializableBundle).getList();

        }

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_places, null);

        this.listView = (ListView) view.findViewById(R.id.listView);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent("MARKLOCATIONONMAP2");

                if(i < imovelVOList.size()) {
                    intent.putExtra("LATITUDE", imovelVOList.get(i).getEnderecoImovelVO() != null ? imovelVOList.get(i).getEnderecoImovelVO().getLatitude() : 0D);
                    intent.putExtra("LONGITUDE", imovelVOList.get(i).getEnderecoImovelVO() != null ? imovelVOList.get(i).getEnderecoImovelVO().getLongitude() : 0D);
                    getFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentVisual.newInstance(imovelVOList.get(i))).commit();
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                }else{
                    new ServerConnectManager(getActivity(), FragmentPlaces.this).set(unidadeGridList.get(i - imovelVOList.size()).getId());
                }
            }
        });

        if(this.imovelVOList == null){
            this.imovelVOList = new ArrayList<ImovelVO>();
        }

        if(this.unidadeGridList == null){
            this.unidadeGridList = new ArrayList<UnidadeGrid>();
        }

        this.listView.setAdapter(new ImovelListAdapter(this.getActivity(), this.imovelVOList, this.unidadeGridList));
        return view;

    }

    @Override
    public void save() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onDataBaseResult(String method, Object object) {
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {

    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {
        if(serviceName.equals(getString(R.string.getUnidade_soap_action))){
            if(result != null) {
                Intent intent = new Intent("MARKLOCATIONONMAP2");
                intent.putExtra("LATITUDE", ((ImovelVO) result).getEnderecoImovelVO() != null ? ((ImovelVO) result).getEnderecoImovelVO().getLatitude() : 0D);
                intent.putExtra("LONGITUDE", ((ImovelVO) result).getEnderecoImovelVO() != null ? ((ImovelVO) result).getEnderecoImovelVO().getLongitude() : 0D);
                getFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentVisual.newInstance(((ImovelVO) result))).commit();
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            }
        }
    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {
        if(serviceName.equals(getString(R.string.getUnidade_soap_action))){
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Aviso!");
            alertDialog.setMessage("Não foi possível resgatar as informações no servidor, por favor, tente novamente.");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
        }
    }
}
