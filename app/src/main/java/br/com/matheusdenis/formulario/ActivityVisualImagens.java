package br.com.matheusdenis.formulario;

import android.os.Bundle;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Activity.DataBaseCoreActivity;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImagemVO;
import br.com.genericdata.valueobjects.ValidatorVO;

public class ActivityVisualImagens extends DataBaseCoreActivity {

    private ArrayList<ImagemVO> imagemVOs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_imagens);

        if(savedInstanceState != null){
            this.imagemVOs = ((SerializableBundle<ImagemVO>)savedInstanceState.getSerializable(ImagemVO.DATAKEY)).getList();
        }else{
            this.imagemVOs = ((SerializableBundle<ImagemVO>)getIntent().getExtras().getSerializable(ImagemVO.DATAKEY)).getList();
        }

        if(this.imagemVOs == null){
            this.imagemVOs = new ArrayList<ImagemVO>();
        }

        this.getSupportFragmentManager().beginTransaction().replace(R.id.places_imagens, FragmentVisualImagem.newInstance(this.imagemVOs, false)).commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImagemVO.DATAKEY, new SerializableBundle<ImagemVO>(this.imagemVOs));
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
