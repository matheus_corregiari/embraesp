package br.com.matheusdenis.formulario;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.UnidadeComercializacaoManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.UnidadeComercializacaoVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentFromUnidadeComercializacao extends DataBaseCoreFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener, View.OnTouchListener {

    private UnidadeComercializacaoVO unidadeComercializacaoVO;

    private CheckBox formUnidadeComercializacaoDisponivelVenda;
    private CheckBox formUnidadeComercializacaoDisponivelLocacao;
    private EditText formUnidadeComercializacaoValorVenda;
    private EditText formUnidadeComercializacaoValorLocacao;
    private EditText formUnidadeComercializacaoValorIptuLocacao;
    private EditText formUnidadeComercializacaoValorCondominioLocacao;
    private EditText formUnidadeComercializacaoObservacoes;
    private Spinner  formUnidadeComercializacaoSpinnerTipoVenda;
    private Spinner  formUnidadeComercializacaoSpinnerTipoLocacao;
    private Spinner  formUnidadeComercializacaoSpinnerFonte;
    
    private EditText formUnidadeComercializacaoCreationVenda;
    private EditText formUnidadeComercializacaoUpdateVenda;
    private EditText formUnidadeComercializacaoCreationLocacao;
    private EditText formUnidadeComercializacaoUpdateLocacao;
    private EditText formUnidadeComercializacaoNome;
    private EditText formUnidadeComercializacaoTelefone;
    private EditText formUnidadeComercializacaoEmail;

    private NumberPickerBuilder numberPickerDialog;
    private DatePickerDialog datePickerBuilder;

    private List<BasicValueObject> tipoVendas;
    private List<BasicValueObject> tipoLocacaos;
    private List<BasicValueObject> fontes;

    public static FragmentFromUnidadeComercializacao newInstance(UnidadeComercializacaoVO unidadeComercializacaoVO){

        if(unidadeComercializacaoVO == null){
            unidadeComercializacaoVO = new UnidadeComercializacaoVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(UnidadeComercializacaoVO.DATAKEY, unidadeComercializacaoVO);
        FragmentFromUnidadeComercializacao fragmentFromUnidadeComercializacao = new FragmentFromUnidadeComercializacao();
        fragmentFromUnidadeComercializacao.setArguments(bundle);
        return fragmentFromUnidadeComercializacao;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setDataBaseManagement(new UnidadeComercializacaoManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.unidadeComercializacaoVO = (UnidadeComercializacaoVO) savedInstanceState.getSerializable(UnidadeComercializacaoVO.DATAKEY);
            this.tipoVendas   = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.COMERCIALIZACAO_TIPO_VENDA  .name())).getList();
            this.tipoLocacaos = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name())).getList();
            this.fontes       = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.COMERCIALIZACAO_FONTE       .name())).getList();

            if(this.unidadeComercializacaoVO == null){
                this.unidadeComercializacaoVO = new UnidadeComercializacaoVO();
            }

            if(this.tipoVendas == null){
                this.tipoVendas = new ArrayList<BasicValueObject>();
            }

            if(this.tipoLocacaos == null){
                this.tipoLocacaos = new ArrayList<BasicValueObject>();
            }

            if(this.fontes == null){
                this.fontes = new ArrayList<BasicValueObject>();
            }

        }else{
            this.unidadeComercializacaoVO = (UnidadeComercializacaoVO) getArguments().getSerializable(UnidadeComercializacaoVO.DATAKEY);
            this.tipoVendas = new ArrayList<BasicValueObject>();
            this.tipoLocacaos = new ArrayList<BasicValueObject>();
            this.fontes = new ArrayList<BasicValueObject>();

            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name());
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name());
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.COMERCIALIZACAO_FONTE.name());

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(UnidadeComercializacaoVO.DATAKEY, this.unidadeComercializacaoVO);
        outState.putSerializable(DropDownKey.COMERCIALIZACAO_TIPO_VENDA  .name(), new SerializableBundle<BasicValueObject>(this.tipoVendas));
        outState.putSerializable(DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name(), new SerializableBundle<BasicValueObject>(this.tipoLocacaos));
        outState.putSerializable(DropDownKey.COMERCIALIZACAO_FONTE       .name(), new SerializableBundle<BasicValueObject>(this.fontes));
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_unidade_comercializacao, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
            this.formUnidadeComercializacaoObservacoes.setText(this.unidadeComercializacaoVO.getObservacoes() == null ? "" : this.unidadeComercializacaoVO.getObservacoes().toUpperCase());
            this.formUnidadeComercializacaoNome.setText(this.unidadeComercializacaoVO.getNome() == null ? "" : this.unidadeComercializacaoVO.getNome().toUpperCase());
            this.formUnidadeComercializacaoTelefone.setText(this.unidadeComercializacaoVO.getTelefone() == null ? "" : this.unidadeComercializacaoVO.getTelefone().toUpperCase());
            this.formUnidadeComercializacaoEmail.setText(this.unidadeComercializacaoVO.getEmail() == null ? "" : this.unidadeComercializacaoVO.getEmail().toUpperCase());
        }

        this.formUnidadeComercializacaoDisponivelVenda.setChecked(this.unidadeComercializacaoVO.isDisponivelVenda());
        this.formUnidadeComercializacaoDisponivelLocacao.setChecked(this.unidadeComercializacaoVO.isDisponivelLocacao());

        this.formUnidadeComercializacaoValorVenda            .setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorVenda()));
        this.formUnidadeComercializacaoValorVenda            .setTag (this.unidadeComercializacaoVO.getValorVenda());
        this.formUnidadeComercializacaoValorLocacao          .setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorLocacao()));
        this.formUnidadeComercializacaoValorLocacao          .setTag (this.unidadeComercializacaoVO.getValorLocacao());
        this.formUnidadeComercializacaoValorIptuLocacao      .setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorIptuLocacao()));
        this.formUnidadeComercializacaoValorIptuLocacao      .setTag (this.unidadeComercializacaoVO.getValorIptuLocacao());
        this.formUnidadeComercializacaoValorCondominioLocacao.setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorCondominioLocacao()));
        this.formUnidadeComercializacaoValorCondominioLocacao.setTag (this.unidadeComercializacaoVO.getValorCondominioLocacao());

        this.formUnidadeComercializacaoCreationVenda  .setText(this.unidadeComercializacaoVO.getDateCreationVenda() == null ? "" :checkDate(this.unidadeComercializacaoVO.getDateCreationVenda()));
        this.formUnidadeComercializacaoCreationVenda  .setTag(this.unidadeComercializacaoVO.getDateCreationVenda());
        this.formUnidadeComercializacaoUpdateVenda    .setText(this.unidadeComercializacaoVO.getDateUpdateVenda() == null ? "" :checkDate(this.unidadeComercializacaoVO.getDateUpdateVenda()));
        this.formUnidadeComercializacaoUpdateVenda    .setTag(this.unidadeComercializacaoVO.getDateUpdateVenda());
        this.formUnidadeComercializacaoCreationLocacao.setText(this.unidadeComercializacaoVO.getDateCreationLocacao() == null ? "" :checkDate(this.unidadeComercializacaoVO.getDateCreationLocacao()));
        this.formUnidadeComercializacaoCreationLocacao.setTag(this.unidadeComercializacaoVO.getDateCreationLocacao());
        this.formUnidadeComercializacaoUpdateLocacao.setText(this.unidadeComercializacaoVO.getDateUpdateLocacao() == null ? "" :checkDate(this.unidadeComercializacaoVO.getDateUpdateLocacao()));
        this.formUnidadeComercializacaoUpdateLocacao.setTag (this.unidadeComercializacaoVO.getDateUpdateLocacao());

        int i = Utils.indexOf(tipoVendas, this.unidadeComercializacaoVO.getTipoVenda());
        i = i < 0? 0:i;

        this.formUnidadeComercializacaoSpinnerTipoVenda.setOnItemSelectedListener(null);
        this.formUnidadeComercializacaoSpinnerTipoVenda.setSelection(i);
        this.formUnidadeComercializacaoSpinnerTipoVenda.setOnItemSelectedListener(this);

        int j = Utils.indexOf(tipoLocacaos, this.unidadeComercializacaoVO.getTipoLocacao());
        j = j < 0? 0:j;

        this.formUnidadeComercializacaoSpinnerTipoLocacao.setOnItemSelectedListener(null);
        this.formUnidadeComercializacaoSpinnerTipoLocacao.setSelection(j);
        this.formUnidadeComercializacaoSpinnerTipoLocacao.setOnItemSelectedListener(this);

        int k = Utils.indexOf(fontes, this.unidadeComercializacaoVO.getFonte());
        k = k < 0? 0:k;

        this.formUnidadeComercializacaoSpinnerFonte.setOnItemSelectedListener(null);
        this.formUnidadeComercializacaoSpinnerFonte.setSelection(k);
        this.formUnidadeComercializacaoSpinnerFonte.setOnItemSelectedListener(this);

    }

    public String checkDate(Date date){

        if(date == null){
            return "";
        }

        String format = DateFormat.format("dd/MM/yyyy", date).toString();
        if(format.equals("01/01/1902")){
            return "";
        }

        return format;
    }

    @SuppressLint("WrongViewCast")
    private void initializeVariables(View view){

        if(view == null){
            return;
        }
        
        this.formUnidadeComercializacaoDisponivelVenda        = (CheckBox) view.findViewById(R.id.formUnidadeComercializacaoDisponivelVenda);
        this.formUnidadeComercializacaoDisponivelLocacao      = (CheckBox) view.findViewById(R.id.formUnidadeComercializacaoDisponivelLocacao);
        this.formUnidadeComercializacaoValorVenda             = (EditText) view.findViewById(R.id.formUnidadeComercializacaoValorVenda);
        this.formUnidadeComercializacaoValorLocacao           = (EditText) view.findViewById(R.id.formUnidadeComercializacaoValorLocacao);
        this.formUnidadeComercializacaoValorIptuLocacao       = (EditText) view.findViewById(R.id.formUnidadeComercializacaoValorIptuLocacao);
        this.formUnidadeComercializacaoValorCondominioLocacao = (EditText) view.findViewById(R.id.formUnidadeComercializacaoValorCondominioLocacao);
        this.formUnidadeComercializacaoObservacoes            = (EditText) view.findViewById(R.id.formUnidadeComercializacaoObservacoes);
        this.formUnidadeComercializacaoSpinnerTipoVenda       = (Spinner)  view.findViewById(R.id.formUnidadeComercializacaoSpinnerTipoVenda);
        this.formUnidadeComercializacaoSpinnerTipoLocacao     = (Spinner)  view.findViewById(R.id.formUnidadeComercializacaoSpinnerTipoLocacao);
        this.formUnidadeComercializacaoSpinnerFonte           = (Spinner)  view.findViewById(R.id.formUnidadeComercializacaoSpinnerFonte);

        this.formUnidadeComercializacaoCreationVenda             = (EditText) view.findViewById(R.id.formUnidadeComercializacaoCreationVenda);
        this.formUnidadeComercializacaoUpdateVenda           = (EditText) view.findViewById(R.id.formUnidadeComercializacaoUpdateVenda);
        this.formUnidadeComercializacaoCreationLocacao       = (EditText) view.findViewById(R.id.formUnidadeComercializacaoCreationLocacao);
        this.formUnidadeComercializacaoUpdateLocacao = (EditText) view.findViewById(R.id.formUnidadeComercializacaoUpdateLocacao);
        this.formUnidadeComercializacaoNome            = (EditText) view.findViewById(R.id.formUnidadeComercializacaoNome);
        this.formUnidadeComercializacaoTelefone             = (EditText) view.findViewById(R.id.formUnidadeComercializacaoTelefone);
        this.formUnidadeComercializacaoEmail           = (EditText) view.findViewById(R.id.formUnidadeComercializacaoEmail);

        this.formUnidadeComercializacaoDisponivelVenda  .setOnCheckedChangeListener(this);
        this.formUnidadeComercializacaoDisponivelLocacao.setOnCheckedChangeListener(this);

        this.formUnidadeComercializacaoValorVenda            .setOnClickListener(this);
        this.formUnidadeComercializacaoValorLocacao          .setOnClickListener(this);
        this.formUnidadeComercializacaoValorIptuLocacao      .setOnClickListener(this);
        this.formUnidadeComercializacaoValorCondominioLocacao.setOnClickListener(this);
        this.formUnidadeComercializacaoCreationVenda         .setOnClickListener(this);
        this.formUnidadeComercializacaoUpdateVenda           .setOnClickListener(this);
        this.formUnidadeComercializacaoCreationLocacao       .setOnClickListener(this);
        this.formUnidadeComercializacaoUpdateLocacao         .setOnClickListener(this);

        this.formUnidadeComercializacaoObservacoes.setOnTouchListener(this);

        if(this.unidadeComercializacaoVO == null){
            this.unidadeComercializacaoVO = new UnidadeComercializacaoVO();
        }

        if(this.tipoVendas == null){
            this.tipoVendas = new ArrayList<BasicValueObject>();
        }

        if(this.tipoLocacaos == null){
            this.tipoLocacaos = new ArrayList<BasicValueObject>();
        }

        if(this.fontes == null){
            this.fontes = new ArrayList<BasicValueObject>();
        }

        this.formUnidadeComercializacaoSpinnerTipoVenda  .setAdapter(new BasicSpinnerAdapter(getActivity(), tipoVendas));
        this.formUnidadeComercializacaoSpinnerTipoLocacao.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoLocacaos));
        this.formUnidadeComercializacaoSpinnerFonte      .setAdapter(new BasicSpinnerAdapter(getActivity(), fontes));

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.formUnidadeComercializacaoValorVenda){
            this.numberPickerDialog = new NumberPickerBuilder()
                    .setFragmentManager(this.getActivity().getSupportFragmentManager())
                    .setMinNumber(0)
                    .setPlusMinusVisibility(View.GONE)
                    .setStyleResId(R.style.BetterPickersDialogFragment_Light);
            this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                @Override
                public void onDialogNumberSet(int reference, int number, double decimal,
                                              boolean isNegative, double fullNumber) {
                    formUnidadeComercializacaoValorVenda.setText(String.format("R$ %1$,.2f", fullNumber));
                    formUnidadeComercializacaoValorVenda.setTag(fullNumber);
                    unidadeComercializacaoVO.setValorVenda(fullNumber);
                }
            });
            this.numberPickerDialog.show();
        }

        if(v.getId() == R.id.formUnidadeComercializacaoValorLocacao){
            this.numberPickerDialog = new NumberPickerBuilder()
                    .setFragmentManager(this.getActivity().getSupportFragmentManager())
                    .setMinNumber(0)
                    .setPlusMinusVisibility(View.GONE)
                    .setStyleResId(R.style.BetterPickersDialogFragment_Light);
            this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                @Override
                public void onDialogNumberSet(int reference, int number, double decimal,
                                              boolean isNegative, double fullNumber) {
                    formUnidadeComercializacaoValorLocacao.setText(String.format("R$ %1$,.2f", fullNumber));
                    formUnidadeComercializacaoValorLocacao.setTag(fullNumber);
                    unidadeComercializacaoVO.setValorLocacao(fullNumber);
                }
            });
            this.numberPickerDialog.show();
        }

        if(v.getId() == R.id.formUnidadeComercializacaoValorCondominioLocacao){
            this.numberPickerDialog = new NumberPickerBuilder()
                    .setFragmentManager(this.getActivity().getSupportFragmentManager())
                    .setMinNumber(0)
                    .setPlusMinusVisibility(View.GONE)
                    .setStyleResId(R.style.BetterPickersDialogFragment_Light);
            this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                @Override
                public void onDialogNumberSet(int reference, int number, double decimal,
                                              boolean isNegative, double fullNumber) {
                    formUnidadeComercializacaoValorCondominioLocacao.setText(String.format("R$ %1$,.2f", fullNumber));
                    formUnidadeComercializacaoValorCondominioLocacao.setTag(fullNumber);
                    unidadeComercializacaoVO.setValorCondominioLocacao(fullNumber);
                }
            });
            this.numberPickerDialog.show();
        }

        if(v.getId() == R.id.formUnidadeComercializacaoValorIptuLocacao){
            this.numberPickerDialog = new NumberPickerBuilder()
                    .setFragmentManager(this.getActivity().getSupportFragmentManager())
                    .setMinNumber(0)
                    .setPlusMinusVisibility(View.GONE)
                    .setStyleResId(R.style.BetterPickersDialogFragment_Light);
            this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                @Override
                public void onDialogNumberSet(int reference, int number, double decimal,
                                              boolean isNegative, double fullNumber) {
                    formUnidadeComercializacaoValorIptuLocacao.setText(String.format("R$ %1$,.2f", fullNumber));
                    formUnidadeComercializacaoValorIptuLocacao.setTag(fullNumber);
                    unidadeComercializacaoVO.setValorIptuLocacao(fullNumber);
                }
            });
            this.numberPickerDialog.show();
        }

        if(v.getId() == R.id.formUnidadeComercializacaoValorCondominioLocacao){
            this.numberPickerDialog = new NumberPickerBuilder()
                    .setFragmentManager(this.getActivity().getSupportFragmentManager())
                    .setMinNumber(0)
                    .setPlusMinusVisibility(View.GONE)
                    .setStyleResId(R.style.BetterPickersDialogFragment_Light);
            this.numberPickerDialog.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandler() {

                @Override
                public void onDialogNumberSet(int reference, int number, double decimal,
                                              boolean isNegative, double fullNumber) {
                    formUnidadeComercializacaoValorCondominioLocacao.setText(String.format("R$ %1$,.2f", fullNumber));
                    formUnidadeComercializacaoValorCondominioLocacao.setTag(fullNumber);
                    unidadeComercializacaoVO.setValorCondominioLocacao(fullNumber);
                }
            });
            this.numberPickerDialog.show();
        }

        if(v.getId() == R.id.formUnidadeComercializacaoCreationVenda){

            Calendar calendarAux = Calendar.getInstance();

            if(formUnidadeComercializacaoCreationVenda.getTag() != null){
                calendarAux.setTime((Date) formUnidadeComercializacaoCreationVenda.getTag());
            }

            this.datePickerBuilder = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                            Calendar calendarAux = Calendar.getInstance();

                            if (formUnidadeComercializacaoCreationVenda.getTag() != null) {
                                calendarAux.setTime((Date) formUnidadeComercializacaoCreationVenda.getTag());
                            }

                            calendarAux.set(year, month, day, 0, 0, 0);

                            formUnidadeComercializacaoCreationVenda.setTag(calendarAux.getTime());
                            formUnidadeComercializacaoCreationVenda.setText(DateFormat.format("dd/MM/yyyy", calendarAux));

                            unidadeComercializacaoVO.setDateCreationVenda(calendarAux.getTime());

                        }
                    },
                    calendarAux.get(Calendar.YEAR),
                    calendarAux.get(Calendar.MONTH),
                    calendarAux.get(Calendar.DAY_OF_MONTH), true);

            this.datePickerBuilder.setVibrate(true);
            this.datePickerBuilder.setYearRange(1903, 2036);
            this.datePickerBuilder.show(this.getActivity().getSupportFragmentManager(), "TAG");
        }

        if(v.getId() == R.id.formUnidadeComercializacaoCreationLocacao){

            Calendar calendarAux = Calendar.getInstance();

            if(formUnidadeComercializacaoCreationLocacao.getTag() != null){
                calendarAux.setTime((Date) formUnidadeComercializacaoCreationLocacao.getTag());
            }

            this.datePickerBuilder = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                            Calendar calendarAux = Calendar.getInstance();

                            if (formUnidadeComercializacaoCreationLocacao.getTag() != null) {
                                calendarAux.setTime((Date) formUnidadeComercializacaoCreationLocacao.getTag());
                            }

                            calendarAux.set(year, month, day, 0, 0, 0);

                            formUnidadeComercializacaoCreationLocacao.setTag(calendarAux.getTime());
                            formUnidadeComercializacaoCreationLocacao.setText(DateFormat.format("dd/MM/yyyy", calendarAux));

                            unidadeComercializacaoVO.setDateCreationLocacao(calendarAux.getTime());

                        }
                    },
                    calendarAux.get(Calendar.YEAR),
                    calendarAux.get(Calendar.MONTH),
                    calendarAux.get(Calendar.DAY_OF_MONTH), true);

            this.datePickerBuilder.setVibrate(true);
            this.datePickerBuilder.setYearRange(1903, 2036);
            this.datePickerBuilder.show(this.getActivity().getSupportFragmentManager(), "TAG");
        }

        if(v.getId() == R.id.formUnidadeComercializacaoUpdateVenda){

            Calendar calendarAux = Calendar.getInstance();

            if(formUnidadeComercializacaoUpdateVenda.getTag() != null){
                calendarAux.setTime((Date) formUnidadeComercializacaoUpdateVenda.getTag());
            }

            this.datePickerBuilder = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                            Calendar calendarAux = Calendar.getInstance();

                            if (formUnidadeComercializacaoUpdateVenda.getTag() != null) {
                                calendarAux.setTime((Date) formUnidadeComercializacaoUpdateVenda.getTag());
                            }

                            calendarAux.set(year, month, day, 0, 0, 0);

                            formUnidadeComercializacaoUpdateVenda.setTag(calendarAux.getTime());
                            formUnidadeComercializacaoUpdateVenda.setText(DateFormat.format("dd/MM/yyyy", calendarAux));

                            unidadeComercializacaoVO.setDateUpdateVenda(calendarAux.getTime());

                        }
                    },
                    calendarAux.get(Calendar.YEAR),
                    calendarAux.get(Calendar.MONTH),
                    calendarAux.get(Calendar.DAY_OF_MONTH), true);

            this.datePickerBuilder.setVibrate(true);
            this.datePickerBuilder.setYearRange(1903, 2036);
            this.datePickerBuilder.show(this.getActivity().getSupportFragmentManager(), "TAG");
        }

        if(v.getId() == R.id.formUnidadeComercializacaoUpdateLocacao){

            Calendar calendarAux = Calendar.getInstance();

            if(formUnidadeComercializacaoUpdateLocacao.getTag() != null){
                calendarAux.setTime((Date) formUnidadeComercializacaoUpdateLocacao.getTag());
            }

            this.datePickerBuilder = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                            Calendar calendarAux = Calendar.getInstance();

                            if (formUnidadeComercializacaoUpdateLocacao.getTag() != null) {
                                calendarAux.setTime((Date) formUnidadeComercializacaoUpdateLocacao.getTag());
                            }

                            calendarAux.set(year, month, day, 0, 0, 0);

                            formUnidadeComercializacaoUpdateLocacao.setTag(calendarAux.getTime());
                            formUnidadeComercializacaoUpdateLocacao.setText(DateFormat.format("dd/MM/yyyy", calendarAux));

                            unidadeComercializacaoVO.setDateUpdateLocacao(calendarAux.getTime());

                        }
                    },
                    calendarAux.get(Calendar.YEAR),
                    calendarAux.get(Calendar.MONTH),
                    calendarAux.get(Calendar.DAY_OF_MONTH), true);

            this.datePickerBuilder.setVibrate(true);
            this.datePickerBuilder.setYearRange(1903, 2036);
            this.datePickerBuilder.show(this.getActivity().getSupportFragmentManager(), "TAG");
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch(adapterView.getId()){
            case R.id.formUnidadeComercializacaoSpinnerTipoVenda:
                if(this.tipoVendas == null || this.tipoVendas.isEmpty()){
                    return;
                }
                this.unidadeComercializacaoVO.setTipoVenda((DropDownItemVO) this.tipoVendas.get(i));
                break;
            case R.id.formUnidadeComercializacaoSpinnerTipoLocacao:
                if(this.tipoLocacaos == null || this.tipoLocacaos.isEmpty()){
                    return;
                }
                this.unidadeComercializacaoVO.setTipoLocacao((DropDownItemVO) this.tipoLocacaos.get(i));
                break;
            case R.id.formUnidadeComercializacaoSpinnerFonte:
                if(this.fontes == null || this.fontes.isEmpty()){
                    return;
                }
                this.unidadeComercializacaoVO.setFonte((DropDownItemVO) this.fontes.get(i));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.formUnidadeComercializacaoDisponivelVenda:
                this.unidadeComercializacaoVO.setDisponivelVenda(b);
                break;
            case R.id.formUnidadeComercializacaoDisponivelLocacao:
                this.unidadeComercializacaoVO.setDisponivelLocacao(b);
                break;
        }
    }

    @Override
    public void save() {
        this.unidadeComercializacaoVO.setObservacoes(this.formUnidadeComercializacaoObservacoes.getText().toString().toUpperCase());
        this.unidadeComercializacaoVO.setNome(this.formUnidadeComercializacaoNome.getText().toString().toUpperCase());
        this.unidadeComercializacaoVO.setTelefone(this.formUnidadeComercializacaoTelefone.getText().toString().toUpperCase());
        this.unidadeComercializacaoVO.setEmail(this.formUnidadeComercializacaoEmail.getText().toString().toUpperCase());

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "UNIDADE_COMERCIALIZACAO");
        intent.putExtra("RESULT_DATA", this.unidadeComercializacaoVO);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);

        if(method != null){
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name())){
                this.tipoVendas = (List<BasicValueObject>) object;
                this.formUnidadeComercializacaoSpinnerTipoVenda.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoVendas));
                updateUI(false);
            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name())){
                this.tipoLocacaos = (List<BasicValueObject>) object;
                this.formUnidadeComercializacaoSpinnerTipoLocacao.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoLocacaos));
                updateUI(false);
            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.COMERCIALIZACAO_FONTE.name())){
                this.fontes = (List<BasicValueObject>) object;
                this.formUnidadeComercializacaoSpinnerFonte.setAdapter(new BasicSpinnerAdapter(getActivity(), fontes));
                updateUI(false);
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (view.getId() == R.id.formUnidadeComercializacaoObservacoes) {
            ScrollView parent = (ScrollView) getActivity().findViewById(R.id.places_layout).findViewById(R.id.scrollView);
            parent.requestDisallowInterceptTouchEvent(true);
            switch (motionEvent.getAction()&MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_UP:
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                case MotionEvent.ACTION_DOWN:
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
        }

        return false;
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
