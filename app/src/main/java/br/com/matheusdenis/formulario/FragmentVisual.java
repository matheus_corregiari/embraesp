package br.com.matheusdenis.formulario;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.ImovelManagement;
import br.com.database.ValueObjects.ImovelVO;
import br.com.genericdata.valueobjects.ValidatorVO;

public class FragmentVisual extends DataBaseCoreFragment implements View.OnClickListener {

    private View formUnidade;
    private View formEndereco;
    private View formUnidadeTerreno;
    private View formUnidadeComercializacao;
    private View formUnidadeEdificacao;

    private ImovelVO imovelVO;

    private Button buttonVoltar;
    private Button buttonEditar;

    private BroadcastReceiver hideFragments = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent == null || intent.getExtras() == null){
                return;
            }

            boolean hideTerreno         = intent.getExtras().getBoolean("hideTerreno"        , false);
            boolean hideComercializacao = intent.getExtras().getBoolean("hideComercializacao", false);
            boolean hideUnidade         = intent.getExtras().getBoolean("hideUnidade"        , false);
            boolean hideEndereco        = intent.getExtras().getBoolean("hideEndereco"       , false);
            boolean hideEdificacao      = intent.getExtras().getBoolean("hideEdificacao"     , false);

            formUnidade.setVisibility(hideUnidade? View.GONE : View.VISIBLE);
            formEndereco.setVisibility(hideEndereco? View.GONE : View.VISIBLE);
            formUnidadeTerreno.setVisibility(hideTerreno? View.GONE : View.VISIBLE);
            formUnidadeComercializacao.setVisibility(hideComercializacao? View.GONE : View.VISIBLE);
            formUnidadeEdificacao.setVisibility(hideEdificacao? View.GONE : View.VISIBLE);

        }
    };


    @Override
    public void save() {
        this.sendToDataBase(MethodTag.SAVE, this.imovelVO);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver( hideFragments, new IntentFilter("BROADCAST_HIDE_FRAGMENTS"));
        super.onResume();
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(hideFragments);
        super.onPause();
    }

    public static FragmentVisual newInstance(ImovelVO imovelVO){

        if(imovelVO == null){
            imovelVO = new ImovelVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ImovelVO.DATAKEY, imovelVO);
        FragmentVisual fragmentForm = new FragmentVisual();
        fragmentForm.setArguments(bundle);
        return fragmentForm;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setUseSaveBroadcast(false);
        this.setDataBaseManagement(new ImovelManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.imovelVO = (ImovelVO) savedInstanceState.getSerializable(ImovelVO.DATAKEY);

            if(this.imovelVO == null){
                this.imovelVO = new ImovelVO();
            }

        }else{
            this.imovelVO = (ImovelVO) getArguments().getSerializable(ImovelVO.DATAKEY);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImovelVO.DATAKEY, this.imovelVO);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visual, null);

        this.buttonVoltar  = (Button) view.findViewById(R.id.buttonVoltar);
        this.buttonEditar  = (Button) view.findViewById(R.id.buttonEditar);

        this.formUnidade                = view.findViewById(R.id.formUnidade);
        this.formEndereco               = view.findViewById(R.id.formEndereco);
        this.formUnidadeTerreno         = view.findViewById(R.id.formUnidadeTerreno);
        this.formUnidadeComercializacao = view.findViewById(R.id.formUnidadeComercializacao);
        this.formUnidadeEdificacao      = view.findViewById(R.id.formUnidadeEdificacao);

        this.buttonVoltar.setOnClickListener(this);
        this.buttonEditar.setOnClickListener(this);

        this.buttonEditar.setText(R.string.editar);

        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormImagem                , FragmentVisualImagem                .newInstance(this.imovelVO.getImagemVOs(), true))         .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormEndereco              , FragmentVisualEnderecoImovel        .newInstance(this.imovelVO.getEnderecoImovelVO()))        .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidade               , FragmentVisualUnidade               .newInstance(this.imovelVO.getUnidadeVO()))               .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidadeTerreno        , FragmentVisualUnidadeTerreno        .newInstance(this.imovelVO.getUnidadeTerrenoVO()))        .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidadeEdificacao     , FragmentVisualUnidadeEdificacao     .newInstance(this.imovelVO.getUnidadeEdificacaoVO()))     .commit();
        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormUnidadeComercializacao, FragmentVisualUnidadeComercializacao.newInstance(this.imovelVO.getUnidadeComercializacaoVO())).commit();

        return view;
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        if(method.equals(this.getMethodName(getClassKey(), MethodTag.DELETE))){
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETALLIMOVELVO"));
            Toast.makeText(getActivity(), "Imovel excluído com sucesso!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        if(method.equals(this.getMethodName(getClassKey(), MethodTag.DELETE))){
            Toast.makeText(getActivity(), "Ocorreu um erro!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.buttonVoltar:
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETALLIMOVELVO"));
                break;
            case R.id.buttonEditar:

                if(!isNetworkConnected()){

                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Aviso!");
                    alertDialog.setMessage("Problemas com a conexão com a internet\nVerifique a sua conexão e tente novamente");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    alertDialog.show();

                    return;
                }

                Intent intent = new Intent("MARKLOCATIONONMAP");
                intent.putExtra("LATITUDE", imovelVO.getEnderecoImovelVO() != null? imovelVO.getEnderecoImovelVO().getLatitude():0D );
                intent.putExtra("LONGITUDE", imovelVO.getEnderecoImovelVO() != null? imovelVO.getEnderecoImovelVO().getLongitude():0D);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                this.getFragmentManager().beginTransaction().replace(R.id.places_layout, FragmentForm.newInstance(this.imovelVO)).commit();
                break;
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
