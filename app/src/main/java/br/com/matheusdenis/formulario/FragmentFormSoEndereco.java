package br.com.matheusdenis.formulario;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.ImovelManagement;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImovelVO;
import br.com.database.ValueObjects.UnidadeComercializacaoVO;
import br.com.database.ValueObjects.UnidadeEdificacaoVO;
import br.com.database.ValueObjects.UnidadeTerrenoVO;
import br.com.database.ValueObjects.UnidadeVO;
import br.com.genericdata.valueobjects.ValidatorVO;

public class FragmentFormSoEndereco extends DataBaseCoreFragment implements View.OnClickListener {

    private View formUnidade;
    private View formEndereco;
    private View formUnidadeTerreno;
    private View formUnidadeComercializacao;
    private View formUnidadeEdificacao;

    private ImovelVO imovelVO;

    private Button buttonVoltar;
    private Button buttonSalvar;

    @Override
    public void save() {
        this.sendToDataBase(MethodTag.SAVE, this.imovelVO);
    }

    @Override
    protected void disableElements() {

    }

    public static FragmentFormSoEndereco newInstance(ImovelVO imovelVO){

        if(imovelVO == null){
            imovelVO = new ImovelVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ImovelVO.DATAKEY, imovelVO);
        FragmentFormSoEndereco fragmentForm = new FragmentFormSoEndereco();
        fragmentForm.setArguments(bundle);
        return fragmentForm;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setUseSaveBroadcast(false);
        this.setDataBaseManagement(new ImovelManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.imovelVO = (ImovelVO) savedInstanceState.getSerializable(ImovelVO.DATAKEY);

            if(this.imovelVO == null){
                this.imovelVO = new ImovelVO();
            }

        }else{
            this.imovelVO = (ImovelVO) getArguments().getSerializable(ImovelVO.DATAKEY);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ImovelVO.DATAKEY, this.imovelVO);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form, null);

        this.buttonVoltar = (Button) view.findViewById(R.id.buttonVoltar);
        this.buttonSalvar = (Button) view.findViewById(R.id.buttonSalvar);

        this.formUnidade                = view.findViewById(R.id.formUnidade);
        this.formEndereco               = view.findViewById(R.id.formEndereco);
        this.formUnidadeTerreno         = view.findViewById(R.id.formUnidadeTerreno);
        this.formUnidadeComercializacao = view.findViewById(R.id.formUnidadeComercializacao);
        this.formUnidadeEdificacao      = view.findViewById(R.id.formUnidadeEdificacao);

        this.formUnidade               .setVisibility(View.GONE);
        this.formUnidadeTerreno        .setVisibility(View.GONE);
        this.formUnidadeComercializacao.setVisibility(View.GONE);
        this.formUnidadeEdificacao     .setVisibility(View.GONE);
        view.findViewById(R.id.fragmentFormImagem).setVisibility(View.GONE);

        this.buttonVoltar.setOnClickListener(this);
        this.buttonSalvar.setOnClickListener(this);

        this.buttonSalvar.setText(R.string.continuar);

        view.findViewById(R.id.buttonExcluir).setVisibility(View.GONE);

        this.getFragmentManager().beginTransaction().replace(R.id.fragmentFormEndereco , FragmentFromEnderecoImovel.newInstance(this.imovelVO.getEnderecoImovelVO()))        .commit();

        return view;
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.SAVE))){
                Toast.makeText(getActivity(), "Registro Salvo com Sucesso!", Toast.LENGTH_SHORT).show();
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETALLIMOVELVO"));
            }
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        if(method != null){
            if(method.equals(this.getMethodName(this.getClassKey(), MethodTag.SAVE))){
                Toast.makeText(getActivity(), "Ocorreu um erro e o Imóvel não pode ser salvo, verifique os dados e tente novamente!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.buttonVoltar:
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETALLIMOVELVO"));
                break;
            case R.id.buttonSalvar:
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("GETIMOVELVO"));
                break;
        }
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
