package br.com.matheusdenis.formulario;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.ValueObjects.DropDownItemVO;

/**
 * Created by Matheus on 18/08/2014.
 */
public class BasicSpinnerAdapter implements SpinnerAdapter {

    private List<?> mList;
    private Context mContext;

    public BasicSpinnerAdapter(Context context, List<?> list) {
        this.mList = list;
        this.mContext = context;
    }

    public BasicSpinnerAdapter(Context context, DropDownKey[] values) {
        List<DropDownKey> mList = new ArrayList<DropDownKey>();

        for (int i = 0; i < values.length; i++) {
            mList.add(values[i]);
        }

        this.mList = mList;

        this.mContext = context;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return this.mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = LayoutInflater.from(mContext).inflate(R.layout.lala_spinner_list_item, null);
        TextView textView = (TextView) view.findViewById(R.id.textView);

        Object o = getItem(position);

        if(o instanceof DropDownKey){
            textView.setText(((DropDownKey) o).name());
        }

        if(o instanceof DropDownItemVO){
            textView.setText(((DropDownItemVO) o).getValue());
        }

        if(o instanceof String){
            textView.setText(((String) o));
        }

        return view;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return mList != null && mList.size() == 0;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {}

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {}

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.lala_spinner_list_item, null);
        TextView textView = (TextView) view.findViewById(R.id.textView);

        Object o = getItem(position);

        if(o instanceof DropDownKey){
            textView.setText(((DropDownKey) o).name());
        }

        if(o instanceof DropDownItemVO){
            textView.setText(((DropDownItemVO) o).getValue());
        }

        if(o instanceof String){
            textView.setText(((String) o));
        }

        return view;
    }

    class ViewHolder {
        public TextView title;
    }
}