package br.com.matheusdenis.formulario.ValueObjects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

import br.com.database.ValueObjects.ImagemVO;

/**
 * Created by Matheus on 22/10/2014.
 */
public class UnidadeGrid implements Serializable {

    @JsonProperty("Id")
    private long   id;
    @JsonProperty("ImgUrl")
    private String imgUrl;
    @JsonProperty("Label01")
    private String  label01;
    @JsonProperty("Label02")
    private String label02;
    @JsonProperty("Label03")
    private String label03;
    @JsonProperty("Imagem")
    private ImagemVO imagemVO;
    @JsonProperty("ImageUrl")
    private String imageUrl;

    public UnidadeGrid() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getLabel01() {
        return label01;
    }

    public void setLabel01(String label01) {
        this.label01 = label01;
    }

    public String getLabel02() {
        return label02;
    }

    public void setLabel02(String label02) {
        this.label02 = label02;
    }

    public ImagemVO getImagemVO() {
        return imagemVO;
    }

    public void setImagemVO(ImagemVO imagemVO) {
        this.imagemVO = imagemVO;
    }

    public String getLabel03() {
        return label03;
    }

    public void setLabel03(String label03) {
        this.label03 = label03;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
