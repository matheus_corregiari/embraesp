package br.com.matheusdenis.formulario;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Spinner;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.database.Enum.DropDownKey;
import br.com.database.Enum.MethodTag;
import br.com.database.Fragment.DataBaseCoreFragment;
import br.com.database.Management.DropDownItemManagement;
import br.com.database.Management.UnidadeComercializacaoManagement;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;
import br.com.database.ValueObjects.UnidadeComercializacaoVO;
import br.com.genericdata.valueobjects.ValidatorVO;

/**
 * Created by Matheus on 25/08/2014.
 */
public class FragmentVisualUnidadeComercializacao extends DataBaseCoreFragment {

    private UnidadeComercializacaoVO unidadeComercializacaoVO;

    private CheckBox formUnidadeComercializacaoDisponivelVenda;
    private CheckBox formUnidadeComercializacaoDisponivelLocacao;
    private TextView formUnidadeComercializacaoValorVenda;
    private TextView formUnidadeComercializacaoValorLocacao;
    private TextView formUnidadeComercializacaoValorIptuLocacao;
    private TextView formUnidadeComercializacaoValorCondominioLocacao;
    private TextView formUnidadeComercializacaoObservacoes;
    private Spinner  formUnidadeComercializacaoSpinnerTipoVenda;
    private Spinner  formUnidadeComercializacaoSpinnerTipoLocacao;
    private Spinner  formUnidadeComercializacaoSpinnerFonte;
    
    private TextView formUnidadeComercializacaoCreationVenda;
    private TextView formUnidadeComercializacaoUpdateVenda;
    private TextView formUnidadeComercializacaoCreationLocacao;
    private TextView formUnidadeComercializacaoUpdateLocacao;
    private TextView formUnidadeComercializacaoNome;
    private TextView formUnidadeComercializacaoTelefone;
    private TextView formUnidadeComercializacaoEmail;

    private NumberPickerBuilder numberPickerDialog;
    private DatePickerDialog datePickerBuilder;

    private List<BasicValueObject> tipoVendas;
    private List<BasicValueObject> tipoLocacaos;
    private List<BasicValueObject> fontes;

    public static FragmentVisualUnidadeComercializacao newInstance(UnidadeComercializacaoVO unidadeComercializacaoVO){

        if(unidadeComercializacaoVO == null){
            unidadeComercializacaoVO = new UnidadeComercializacaoVO();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(UnidadeComercializacaoVO.DATAKEY, unidadeComercializacaoVO);
        FragmentVisualUnidadeComercializacao fragmentFromUnidadeComercializacao = new FragmentVisualUnidadeComercializacao();
        fragmentFromUnidadeComercializacao.setArguments(bundle);
        return fragmentFromUnidadeComercializacao;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setDataBaseManagement(new UnidadeComercializacaoManagement(this.getActivity(), this));
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            this.unidadeComercializacaoVO = (UnidadeComercializacaoVO) savedInstanceState.getSerializable(UnidadeComercializacaoVO.DATAKEY);
            this.tipoVendas   = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.COMERCIALIZACAO_TIPO_VENDA  .name())).getList();
            this.tipoLocacaos = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name())).getList();
            this.fontes       = ((SerializableBundle<BasicValueObject>) savedInstanceState.getSerializable(DropDownKey.COMERCIALIZACAO_FONTE       .name())).getList();

            if(this.unidadeComercializacaoVO == null){
                this.unidadeComercializacaoVO = new UnidadeComercializacaoVO();
            }

            if(this.tipoVendas == null){
                this.tipoVendas = new ArrayList<BasicValueObject>();
            }

            if(this.tipoLocacaos == null){
                this.tipoLocacaos = new ArrayList<BasicValueObject>();
            }

            if(this.fontes == null){
                this.fontes = new ArrayList<BasicValueObject>();
            }

        }else{
            this.unidadeComercializacaoVO = (UnidadeComercializacaoVO) getArguments().getSerializable(UnidadeComercializacaoVO.DATAKEY);
            this.tipoVendas = new ArrayList<BasicValueObject>();
            this.tipoLocacaos = new ArrayList<BasicValueObject>();
            this.fontes = new ArrayList<BasicValueObject>();

            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name());
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name());
            new DropDownItemManagement(this.getActivity(), this).sendToDataBase(MethodTag.GETDROPDOWNITENSBYKEY, DropDownKey.COMERCIALIZACAO_FONTE.name());

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(UnidadeComercializacaoVO.DATAKEY, this.unidadeComercializacaoVO);
        outState.putSerializable(DropDownKey.COMERCIALIZACAO_TIPO_VENDA  .name(), new SerializableBundle<BasicValueObject>(this.tipoVendas));
        outState.putSerializable(DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name(), new SerializableBundle<BasicValueObject>(this.tipoLocacaos));
        outState.putSerializable(DropDownKey.COMERCIALIZACAO_FONTE       .name(), new SerializableBundle<BasicValueObject>(this.fontes));
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visual_unidade_comercializacao, null);
        this.initializeVariables(view);
        this.updateUI(savedInstanceState == null);
        return view;
    }

    private void updateUI(boolean updateAll) {

        if(updateAll) {
            this.formUnidadeComercializacaoObservacoes.setText(this.unidadeComercializacaoVO.getObservacoes() == null ? "" : this.unidadeComercializacaoVO.getObservacoes().toUpperCase());
            this.formUnidadeComercializacaoNome.setText(this.unidadeComercializacaoVO.getNome() == null ? "" : this.unidadeComercializacaoVO.getNome().toUpperCase());
            this.formUnidadeComercializacaoTelefone.setText(this.unidadeComercializacaoVO.getTelefone() == null ? "" : this.unidadeComercializacaoVO.getTelefone().toUpperCase());
            this.formUnidadeComercializacaoEmail.setText(this.unidadeComercializacaoVO.getEmail() == null ? "" : this.unidadeComercializacaoVO.getEmail().toUpperCase());
        }

        this.formUnidadeComercializacaoDisponivelVenda.setChecked(this.unidadeComercializacaoVO.isDisponivelVenda());
        this.formUnidadeComercializacaoDisponivelLocacao.setChecked(this.unidadeComercializacaoVO.isDisponivelLocacao());

        this.formUnidadeComercializacaoValorVenda            .setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorVenda()));
        this.formUnidadeComercializacaoValorVenda            .setTag (this.unidadeComercializacaoVO.getValorVenda());
        this.formUnidadeComercializacaoValorLocacao          .setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorLocacao()));
        this.formUnidadeComercializacaoValorLocacao          .setTag (this.unidadeComercializacaoVO.getValorLocacao());
        this.formUnidadeComercializacaoValorIptuLocacao      .setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorIptuLocacao()));
        this.formUnidadeComercializacaoValorIptuLocacao      .setTag (this.unidadeComercializacaoVO.getValorIptuLocacao());
        this.formUnidadeComercializacaoValorCondominioLocacao.setText(String.format("R$ %1$,.2f", this.unidadeComercializacaoVO.getValorCondominioLocacao()));
        this.formUnidadeComercializacaoValorCondominioLocacao.setTag (this.unidadeComercializacaoVO.getValorCondominioLocacao());

        this.formUnidadeComercializacaoCreationVenda  .setText(this.unidadeComercializacaoVO.getDateCreationVenda() == null ? "" : checkDate(this.unidadeComercializacaoVO.getDateCreationVenda()));
        this.formUnidadeComercializacaoCreationVenda  .setTag(this.unidadeComercializacaoVO.getDateCreationVenda());
        this.formUnidadeComercializacaoUpdateVenda    .setText(this.unidadeComercializacaoVO.getDateUpdateVenda() == null ? "" : checkDate(this.unidadeComercializacaoVO.getDateUpdateVenda()));
        this.formUnidadeComercializacaoUpdateVenda    .setTag(this.unidadeComercializacaoVO.getDateUpdateVenda());
        this.formUnidadeComercializacaoCreationLocacao.setText(this.unidadeComercializacaoVO.getDateCreationLocacao() == null ? "" : checkDate(this.unidadeComercializacaoVO.getDateCreationLocacao()));
        this.formUnidadeComercializacaoCreationLocacao.setTag(this.unidadeComercializacaoVO.getDateCreationLocacao());
        this.formUnidadeComercializacaoUpdateLocacao.setText(this.unidadeComercializacaoVO.getDateUpdateLocacao() == null ? "" : checkDate(this.unidadeComercializacaoVO.getDateUpdateLocacao()));
        this.formUnidadeComercializacaoUpdateLocacao.setTag (this.unidadeComercializacaoVO.getDateUpdateLocacao());

        int i = Utils.indexOf(tipoVendas, this.unidadeComercializacaoVO.getTipoVenda());
        i = i < 0? 0:i;

        this.formUnidadeComercializacaoSpinnerTipoVenda.setOnItemSelectedListener(null);
        this.formUnidadeComercializacaoSpinnerTipoVenda.setSelection(i);

        int j = Utils.indexOf(tipoLocacaos, this.unidadeComercializacaoVO.getTipoLocacao());
        j = j < 0? 0:j;

        this.formUnidadeComercializacaoSpinnerTipoLocacao.setOnItemSelectedListener(null);
        this.formUnidadeComercializacaoSpinnerTipoLocacao.setSelection(j);

        int k = Utils.indexOf(fontes, this.unidadeComercializacaoVO.getFonte());
        k = k < 0? 0:k;

        this.formUnidadeComercializacaoSpinnerFonte.setOnItemSelectedListener(null);
        this.formUnidadeComercializacaoSpinnerFonte.setSelection(k);

    }

    private void initializeVariables(View view){

        if(view == null){
            return;
        }
        
        this.formUnidadeComercializacaoDisponivelVenda        = (CheckBox) view.findViewById(R.id.formUnidadeComercializacaoDisponivelVenda);
        this.formUnidadeComercializacaoDisponivelLocacao      = (CheckBox) view.findViewById(R.id.formUnidadeComercializacaoDisponivelLocacao);
        this.formUnidadeComercializacaoSpinnerTipoVenda       = (Spinner)  view.findViewById(R.id.formUnidadeComercializacaoSpinnerTipoVenda);
        this.formUnidadeComercializacaoSpinnerTipoLocacao     = (Spinner)  view.findViewById(R.id.formUnidadeComercializacaoSpinnerTipoLocacao);
        this.formUnidadeComercializacaoSpinnerFonte           = (Spinner)  view.findViewById(R.id.formUnidadeComercializacaoSpinnerFonte);
        this.formUnidadeComercializacaoValorVenda             = (TextView) view.findViewById(R.id.formUnidadeComercializacaoValorVenda);
        this.formUnidadeComercializacaoValorLocacao           = (TextView) view.findViewById(R.id.formUnidadeComercializacaoValorLocacao);
        this.formUnidadeComercializacaoValorIptuLocacao       = (TextView) view.findViewById(R.id.formUnidadeComercializacaoValorIptuLocacao);
        this.formUnidadeComercializacaoValorCondominioLocacao = (TextView) view.findViewById(R.id.formUnidadeComercializacaoValorCondominioLocacao);
        this.formUnidadeComercializacaoObservacoes            = (TextView) view.findViewById(R.id.formUnidadeComercializacaoObservacoes);
        this.formUnidadeComercializacaoCreationVenda          = (TextView) view.findViewById(R.id.formUnidadeComercializacaoCreationVenda);
        this.formUnidadeComercializacaoUpdateVenda            = (TextView) view.findViewById(R.id.formUnidadeComercializacaoUpdateVenda);
        this.formUnidadeComercializacaoCreationLocacao        = (TextView) view.findViewById(R.id.formUnidadeComercializacaoCreationLocacao);
        this.formUnidadeComercializacaoUpdateLocacao          = (TextView) view.findViewById(R.id.formUnidadeComercializacaoUpdateLocacao);
        this.formUnidadeComercializacaoNome                   = (TextView) view.findViewById(R.id.formUnidadeComercializacaoNome);
        this.formUnidadeComercializacaoTelefone               = (TextView) view.findViewById(R.id.formUnidadeComercializacaoTelefone);
        this.formUnidadeComercializacaoEmail                  = (TextView) view.findViewById(R.id.formUnidadeComercializacaoEmail);

        view.findViewById(R.id.formUnidadeComercializacaoValorVenda).setVisibility(this.unidadeComercializacaoVO.getValorVenda() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoValorLocacao).setVisibility(this.unidadeComercializacaoVO.getValorLocacao() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoValorIptuLocacao).setVisibility(this.unidadeComercializacaoVO.getValorIptuLocacao() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoValorCondominioLocacao).setVisibility(this.unidadeComercializacaoVO.getValorCondominioLocacao() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoObservacoes).setVisibility(this.unidadeComercializacaoVO.getObservacoes() == null ||  this.unidadeComercializacaoVO.getObservacoes().equals("") ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoCreationVenda).setVisibility(this.unidadeComercializacaoVO.getDateCreationVenda() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoUpdateVenda).setVisibility(this.unidadeComercializacaoVO.getDateCreationLocacao() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoCreationLocacao).setVisibility(this.unidadeComercializacaoVO.getDateUpdateVenda() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoUpdateLocacao).setVisibility(this.unidadeComercializacaoVO.getDateUpdateLocacao() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoNome).setVisibility(this.unidadeComercializacaoVO.getNome() == null ||  this.unidadeComercializacaoVO.getNome().equals("") ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoTelefone).setVisibility(this.unidadeComercializacaoVO.getTelefone() == null ||  this.unidadeComercializacaoVO.getTelefone().equals("") ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoEmail).setVisibility(this.unidadeComercializacaoVO.getEmail() == null ||  this.unidadeComercializacaoVO.getEmail().equals("") ? View.GONE : View.VISIBLE);

        /*view.findViewById(R.id.formUnidadeComercializacaoValorVendaLabel).setVisibility(this.unidadeComercializacaoVO.getValorVenda() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoValorLocacaoLabel).setVisibility(this.unidadeComercializacaoVO.getValorLocacao() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoValorIptuLocacaoLabel).setVisibility(this.unidadeComercializacaoVO.getValorIptuLocacao() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoValorCondominioLocacaoLabel).setVisibility(this.unidadeComercializacaoVO.getValorCondominioLocacao() <= 0 ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoObservacoesLabel).setVisibility(this.unidadeComercializacaoVO.getObservacoes() == null ||  this.unidadeComercializacaoVO.getObservacoes().equals("") ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoCreationVendaLabel).setVisibility(this.unidadeComercializacaoVO.getDateCreationVenda() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoUpdateVendaLabel).setVisibility(this.unidadeComercializacaoVO.getDateCreationLocacao() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoCreationLocacaoLabel).setVisibility(this.unidadeComercializacaoVO.getDateUpdateVenda() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoUpdateLocacaoLabel).setVisibility(this.unidadeComercializacaoVO.getDateUpdateLocacao() == null ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoNomeLabel).setVisibility(this.unidadeComercializacaoVO.getNome() == null ||  this.unidadeComercializacaoVO.getNome().equals("") ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoTelefoneLabel).setVisibility(this.unidadeComercializacaoVO.getTelefone() == null ||  this.unidadeComercializacaoVO.getTelefone().equals("") ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.formUnidadeComercializacaoEmailLabel).setVisibility(this.unidadeComercializacaoVO.getEmail() == null ||  this.unidadeComercializacaoVO.getEmail().equals("") ? View.GONE : View.VISIBLE);
*/

        this.formUnidadeComercializacaoSpinnerTipoVenda.setEnabled(false);
        this.formUnidadeComercializacaoSpinnerTipoVenda.setClickable(false);
        this.formUnidadeComercializacaoSpinnerTipoLocacao.setEnabled(false);
        this.formUnidadeComercializacaoSpinnerTipoLocacao.setClickable(false);
        this.formUnidadeComercializacaoSpinnerFonte.setEnabled(false);
        this.formUnidadeComercializacaoSpinnerFonte.setClickable(false);


        if(this.unidadeComercializacaoVO == null){
            this.unidadeComercializacaoVO = new UnidadeComercializacaoVO();
        }

        if(this.tipoVendas == null){
            this.tipoVendas = new ArrayList<BasicValueObject>();
        }

        if(this.tipoLocacaos == null){
            this.tipoLocacaos = new ArrayList<BasicValueObject>();
        }

        if(this.fontes == null){
            this.fontes = new ArrayList<BasicValueObject>();
        }

        this.formUnidadeComercializacaoSpinnerTipoVenda  .setAdapter(new BasicSpinnerAdapter(getActivity(), tipoVendas));
        this.formUnidadeComercializacaoSpinnerTipoLocacao.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoLocacaos));
        this.formUnidadeComercializacaoSpinnerFonte      .setAdapter(new BasicSpinnerAdapter(getActivity(), fontes));

    }

    @Override
    public void save() {
        this.unidadeComercializacaoVO.setObservacoes(this.formUnidadeComercializacaoObservacoes.getText().toString());
        this.unidadeComercializacaoVO.setNome(this.formUnidadeComercializacaoNome.getText().toString());
        this.unidadeComercializacaoVO.setTelefone(this.formUnidadeComercializacaoTelefone.getText().toString());
        this.unidadeComercializacaoVO.setEmail(this.formUnidadeComercializacaoEmail.getText().toString());

        Intent intent = new Intent("BROADCAST_FRAGMENT_FORM");
        intent.putExtra("STATUS", "RESULT_OK");
        intent.putExtra("FRAGMENT", "UNIDADE_COMERCIALIZACAO");
        intent.putExtra("RESULT_DATA", this.unidadeComercializacaoVO);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    protected void disableElements() {

    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        super.onDataBaseResult(method, object);

        if(method != null){
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.COMERCIALIZACAO_TIPO_VENDA.name())){
                this.tipoVendas = (List<BasicValueObject>) object;
                this.formUnidadeComercializacaoSpinnerTipoVenda.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoVendas));
                updateUI(false);
            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.COMERCIALIZACAO_TIPO_LOCACAO.name())){
                this.tipoLocacaos = (List<BasicValueObject>) object;
                this.formUnidadeComercializacaoSpinnerTipoLocacao.setAdapter(new BasicSpinnerAdapter(getActivity(), tipoLocacaos));
                updateUI(false);
            }
            if(method.equals(this.getMethodName(DropDownItensMethods.class.getSimpleName(), MethodTag.GETDROPDOWNITENSBYKEY)+"."+DropDownKey.COMERCIALIZACAO_FONTE.name())){
                this.fontes = (List<BasicValueObject>) object;
                this.formUnidadeComercializacaoSpinnerFonte.setAdapter(new BasicSpinnerAdapter(getActivity(), fontes));
                updateUI(false);
            }
        }
    }

    public String checkDate(Date date){

        if(date == null){
            return "";
        }

        String format = DateFormat.format("dd/MM/yyyy", date).toString();
        if(format.equals("01/01/1902")){
            return "";
        }

        return format;
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        super.onDataBaseError(method, exception);
    }

    @Override
    public void onServerResult(String serviceName, boolean hasErrors, List<?> resultList, Object result, Object... parameters) {

    }

    @Override
    public void onServerError(String serviceName, Exception exception, List<ValidatorVO> validators, Object... parameters) {

    }
}
