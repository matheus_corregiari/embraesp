package br.com.matheusdenis.formulario.ValueObjects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Matheus on 22/10/2014.
 */
public class UnidadeMapa implements Serializable {

    @JsonProperty("Id")
    private long   id;
    @JsonProperty("Latitude")
    private float  latitude;
    @JsonProperty("Longitude")
    private float  longitude;
    @JsonProperty("Label")
    private String label;
    @JsonProperty("BgColor")
    private String bgColor;

    public UnidadeMapa() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }
}
