package br.com.database.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;

import br.com.database.Enum.MethodTag;
import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.TableMethods.GenericMethods;
import br.com.database.TableMethods.ImagemMethods;
import br.com.database.ValueObjects.BasicValueObject;

import static br.com.database.Enum.MethodTag.*;

/**
 * Created by matheus on 19/08/2014.
 */
public class DataBaseAsyncTask extends AsyncTask <Object, Object, Object>{

    private Context mContext;
    private DataBaseResultDelegate dataBaseResultDelegate;
    private GenericMethods         genericMethods;
    private String method = "";

    public DataBaseAsyncTask(Context mContext, DataBaseResultDelegate dataBaseResultDelegate, GenericMethods genericMethods) {

        if (mContext == null) {
            throw new IllegalStateException("<<CONTEXT_CANNOT_BE_NULL>>");
        }

        if (dataBaseResultDelegate == null) {
            throw new IllegalStateException("<<DATABASE_RESULT_DELEGATE_CANNOT_BE_NULL>>");
        }

        if (genericMethods == null) {
            throw new IllegalStateException("<<GENERIC_METHODS_CANNOT_BE_NULL>>");
        }
        
        this.mContext               = mContext;
        this.dataBaseResultDelegate = dataBaseResultDelegate;
        this.genericMethods         = genericMethods;
    }

    @Override
    protected Object doInBackground(Object... objects) {

        try {

            if(objects == null || objects.length == 0){
                throw new IllegalArgumentException("<<PARAMETERS_CANNOT_BE_NULL_OR_EMPTY>>");
            }

            if(!(objects[0] instanceof MethodTag)){
                throw new IllegalArgumentException("<<FIRST_PARAMETER_MUST_BE_INSTANSE_OF_METHODTAG>>");
            }

            MethodTag methodTag = (MethodTag) objects[0];

            if (methodTag == null) {
                throw new IllegalStateException("<<METHOD_TAG_CANNOT_BE_NULL>>");
            }

            if (this.genericMethods == null) {
                throw new IllegalStateException("<<GENERIC_METHODS_CANNOT_BE_NULL>>");
            }

            this.method = this.genericMethods.getClassKey()+"."+methodTag.toString();

            switch (methodTag) {

                /*
                 * BASIC METHODS
                 */
                case GETALL:
                    return this.genericMethods.getAll ();
                case GET:
                    return this.genericMethods.get    ((Long) objects[1]);
                case SAVE:
                    return this.genericMethods.save   ((BasicValueObject) objects[1]);
                case DELETE:
                    return this.genericMethods.delete((Long) objects[1]);
                case UPDATE:
                    return this.genericMethods.update ((BasicValueObject) objects[1]);
                case EXECSQL:
                    return this.genericMethods.execSQL((String) objects[1]);

                /*
                 * ESPECIAL METHODS
                 */
                case GETBYSERVERID:
                    return this.genericMethods.getByServerId   ((Long) objects[1]);
                case SAVEFROMSERVER:
                    return this.genericMethods.saveFromServer  ((BasicValueObject) objects[1]);
                case UPDATEFROMSERVER:
                    return this.genericMethods.updateFromServer((BasicValueObject) objects[1]);
                case DELETEBYSERVERID:
                    return this.genericMethods.deleteByServerId((Long) objects[1]);

                /*
                 * EXTRA METHODS DROPDOWNITENS
                 */
                case GETDROPDOWNITENSBYKEY:
                    this.method += "."+((String) objects[1]);
                    return ((DropDownItensMethods) this.genericMethods).getDropDownItensByKey   ((String) objects[1]);
                case GETDROPDOWNITENSBYPARENT:
                    return ((DropDownItensMethods) this.genericMethods).getDropDownItensByParent((Long) objects[1]);
                /*
                 * EXTRA METHODS DROPDOWNITENS
                 */
                case GETIMAGENSBYUNIDADEID:
                    return ((ImagemMethods) this.genericMethods).getImagensByUnidadeId((Long) objects[1]);
            }
        }catch(Exception e){
            return e;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        try {
            if (this.dataBaseResultDelegate != null) {
                if (result == null) {
                    this.dataBaseResultDelegate.onDataBaseResult(method, result);
                } else {
                    if (result instanceof Exception) {
                        this.dataBaseResultDelegate.onDataBaseError(method, (Exception) result);
                    } else {
                        this.dataBaseResultDelegate.onDataBaseResult(method, result);
                    }
                }
            }
        }catch(Exception e){
            try{
                this.dataBaseResultDelegate.onDataBaseError(method, e);
            }catch(Exception e2){
                e2.printStackTrace();
            }
        }
    }

}
