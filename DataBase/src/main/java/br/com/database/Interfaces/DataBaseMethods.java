package br.com.database.Interfaces;

import java.util.List;

import br.com.database.ValueObjects.BasicValueObject;

/**
 * Created by Matheus on 12/08/2014.
 */
public interface DataBaseMethods<T extends BasicValueObject> {

    //Basic Methods
    public T get(long id);

    public List<T> getAll();

    public T save(T objectToSave);

    public T update(T objectToUpdate);

    public T delete(long id);

    public List<T> execSQL(String query);

    //Especial Methods
    public T getByServerId(long serverId);

    public T updateFromServer(T objectToUpdate);

    public T saveFromServer(T objectToSave);

    public T deleteByServerId(long serverId);

}
