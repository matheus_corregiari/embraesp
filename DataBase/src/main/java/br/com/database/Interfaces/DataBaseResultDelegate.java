package br.com.database.Interfaces;

/**
 * Created by Matheus on 12/08/2014.
 */
public interface DataBaseResultDelegate {

    public void onDataBaseResult(String method, Object object);

    public void onDataBaseError(String method, Exception exception);

}
