package br.com.database.Enum;

/**
 * Created by matheus on 19/08/2014.
 */
public enum MethodTag {

    //DEFAULT
    GET          (0, "get"    ),
    GETALL       (1, "getAll" ),
    SAVE         (2, "save"   ),
    UPDATE       (3, "update" ),
    DELETE       (4, "delete" ),
    EXECSQL      (5, "execSQL"),

    //ESPECIAL DEFAULT
    GETBYSERVERID   (6, "getByServerId"   ),
    SAVEFROMSERVER  (7, "saveFromServer"  ),
    UPDATEFROMSERVER(8, "updateFromServer"),
    DELETEBYSERVERID(9, "deleteByServerId"),

    //EXTRA DROPDOWNITENS
    GETDROPDOWNITENSBYKEY   (10, "getDropDownItensByKey"),
    GETDROPDOWNITENSBYPARENT(11, "getDropDownItensByParent"),

    //EXTRA DROPDOWNITENS
    GETIMAGENSBYUNIDADEID   (12, "getImagensByUnidadeId");

    private int value;
    private String name;

    MethodTag(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
