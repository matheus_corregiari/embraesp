package br.com.database.Enum;

/**
 * Created by Matheus on 18/08/2014.
 */
public enum DropDownKey {

    DADO_ORIGEM                 ("Dado Origem"),
    TIPO_IMOVEL                 ("Tipo Imovel"),
    SUB_TIPO_IMOVEL             ("Sub Tipo Imovel"),
    TERRENO_POTENCIALIDADE      ("Potencialidade Terreno"),
    EDIFICACAO_TIPO_ESTRUTURA   ("Edificacao Estrutura"),
    EDIFICACAO_TIPO_COBERTURA   ("Edificacao Cobertura"),
    EDIFICACAO_TIPO             ("Edificacao Tipo"),
    EDIFICACAO_TIPO_CONSERVACAO ("Edificacao Conservacao"),
    EDIFICACAO_PADRAO_ECONOMICO ("Padrao Economico"),
    COMERCIALIZACAO_TIPO_VENDA  ("Comercializacao Venda"),
    COMERCIALIZACAO_TIPO_LOCACAO("Comercializacao Locacao"),
    COMERCIALIZACAO_FONTE       ("Comercializacao Fonte");

    private String title;

    DropDownKey(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
