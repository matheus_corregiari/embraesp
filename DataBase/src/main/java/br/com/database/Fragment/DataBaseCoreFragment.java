package br.com.database.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;

import br.com.database.Enum.MethodTag;
import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.Management.DataBaseManagement;
import br.com.genericdata.coredata.activity.CoreFragment;

/**
 * Created by Matheus on 19/08/2014.
 */
public abstract class DataBaseCoreFragment extends CoreFragment implements DataBaseResultDelegate {

    private DataBaseManagement dataBaseManagement;
    public DataBaseResultDelegate callback;
    private boolean useSaveBroadcast = true;
    private boolean disable = false;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            save();
        }
    };

    public abstract void save();

    @Override
    public void onResume() {

        if(disable){
            this.disableElements();
        }

        if(useSaveBroadcast)
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver( broadcastReceiver, new IntentFilter("SAVE"));
        super.onResume();
    }

    protected abstract void disableElements();

    @Override
    public void onPause() {
        if(useSaveBroadcast)
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }


    public DataBaseManagement getDataBaseManagement() {
        return dataBaseManagement;
    }

    public void setDataBaseManagement(DataBaseManagement dataBaseManagement) {
        this.dataBaseManagement = dataBaseManagement;
    }

    public void sendToDataBase(MethodTag methodTag, Object... parameters){
        if(this.dataBaseManagement != null){
            this.dataBaseManagement.sendToDataBase(methodTag, parameters);
        }
    }



    public void setCallback(DataBaseResultDelegate callback){
        this.callback = callback;
    }

    public String getClassKey(){
        if(this.dataBaseManagement != null){
           return this.dataBaseManagement.getClassKey();
        }
        return "";
    }

    public static String getMethodName(String classKey, MethodTag methodTag){

        if(classKey == null && methodTag != null){
            return methodTag.getName();
        }

        if(classKey != null && methodTag == null){
            return classKey;
        }

        if(classKey != null && methodTag != null){
            return classKey + "." + methodTag.getName();
        }

        return "";
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        if(this.callback != null){
            this.callback.onDataBaseResult(method, object);
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        if(this.callback != null){
            this.callback.onDataBaseError(method, exception);
        }
    }

    public boolean isUseSaveBroadcast() {
        return useSaveBroadcast;
    }

    public void setUseSaveBroadcast(boolean useSaveBroadcast) {
        this.useSaveBroadcast = useSaveBroadcast;
    }
}
