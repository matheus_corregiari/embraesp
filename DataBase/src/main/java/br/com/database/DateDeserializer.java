package br.com.database;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Matheus on 28/10/2014.
 */
public class DateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        System.out.println(jp.getText());

        String date = jp.getText();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(date);
            System.out.println(convertedDate);
            return convertedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        try {
//            System.out.println(jp.getText());
//
//            String date = jp.getText();
//
//            date = date.replace("(", "");
//            date = date.replace(")", "");
//            date = date.replace("/", "");
//            date = date.replace("Date", "");
//
//            System.out.println(date);
//
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTimeInMillis(Long.parseLong(date));
//
//            if (calendar == null) {
//                return null;
//            }
//
//            return calendar.getTime();
//        }catch(Exception e){
//            e.printStackTrace();
//        }
        
        return null;
    }
}
