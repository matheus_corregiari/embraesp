package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Address;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.database.DataBase;
import br.com.database.TableMethods.DropDownItensMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class EnderecoImovelVO extends BasicValueObject{

    @JsonIgnore
    public static String DATAKEY = EnderecoImovelVO.class.getSimpleName();

    @JsonProperty("tipoImovel")
    private DropDownItemVO tipoImovel;
    @JsonProperty("logradouro")
    private String         logradouro;
    @JsonProperty("numero")
    private String         numero;
    @JsonProperty("cep")
    private String         cep;
    @JsonProperty("bairro")
    private String         bairro;
    @JsonProperty("municipio")
    private String         municipio;
    @JsonProperty("uf")
    private String         UF;
    @JsonProperty("latitude")
    private double         latitude;
    @JsonProperty("longitude")
    private double         longitude;
    @JsonProperty("enderecoAproximado")
    private boolean        enderecoAproximado;
    @JsonIgnore
    private boolean        municipiooo;

    public EnderecoImovelVO() {
        super(0L, "");
        this.tipoImovel         = null;
        this.logradouro         = "";
        this.numero             = "";
        this.cep                = "";
        this.bairro             = "";
        this.municipio          = "";
        this.UF                 = "";
        this.latitude           = 0D;
        this.longitude          = 0D;
        this.enderecoAproximado = false;
        this.municipiooo = false;
    }

    public EnderecoImovelVO(long idServer, String token, DropDownItemVO tipoImovel, String logradouro, String numero,
                            String cep, String bairro, String municipio, String UF, double latitude,
                            double longitude, boolean enderecoAproximado) {
        super(idServer, token);

        this.tipoImovel         = tipoImovel;
        this.logradouro         = logradouro;
        this.numero             = numero;
        this.cep                = cep;
        this.bairro             = bairro;
        this.municipio          = municipio;
        this.UF                 = UF;
        this.latitude           = latitude;
        this.longitude          = longitude;
        this.enderecoAproximado = enderecoAproximado;
        this.municipiooo = false;
    }

    public EnderecoImovelVO(Address address) {
        super(0L, "");
        if(address == null){
            return;
        }

        this.tipoImovel         = null;
        this.enderecoAproximado = false;

        this.logradouro         = address.getThoroughfare() != null? address.getThoroughfare() : "";
        this.numero             = address.getSubThoroughfare() != null?  address.getSubThoroughfare() : "";
        this.cep                = address.getPostalCode() != null?  address.getPostalCode() : "";
        this.bairro             = address.getSubLocality() != null?  address.getSubLocality() : "";
        this.municipio          = address.getSubAdminArea() != null?  address.getSubAdminArea() : "";
        this.UF                 = address.getAddressLine(1).substring(address.getAddressLine(1).length() - 2, address.getAddressLine(1).length());
        this.latitude           = address.getLatitude();
        this.longitude          = address.getLongitude();
        this.municipiooo = false;

    }

    public EnderecoImovelVO(Context context, Cursor cursor) {
        super(context, cursor);

        if(cursor.getInt(5) <= 0){
            this.tipoImovel = null;
        }else{
            this.tipoImovel = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(5));
        }

        this.logradouro         = cursor.getString(6);
        this.numero             = cursor.getString(7);
        this.cep                = cursor.getString(8);
        this.bairro             = cursor.getString(9);
        this.municipio          = cursor.getString(10);
        this.UF                 = cursor.getString(11);
        this.latitude           = cursor.getDouble(12);
        this.longitude          = cursor.getDouble(13);
        this.enderecoAproximado = cursor.getInt(14) == 1;
    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        /*
        TODO VALIDATORS DE INSERCAO COLOCAR NOS GETTERS E SETTERS E MUDAR CONSTRUTORES PARA USAR OS SETS
         */

        if(this.tipoImovel != null && this.tipoImovel.getId() > 0){
            values.put(DataBase.ENDERECO_TIPO_IMOVEL, this.tipoImovel.getId());
        }else{
            values.put(DataBase.ENDERECO_TIPO_IMOVEL, 0);
        }

        values.put(DataBase.ENDERECO_LOGRADOURO         , this.logradouro);
        values.put(DataBase.ENDERECO_NUMERO             , this.numero);
        values.put(DataBase.ENDERECO_CEP                , this.cep);
        values.put(DataBase.ENDERECO_BAIRRO             , this.bairro);
        values.put(DataBase.ENDERECO_MUNICIPIO          , this.municipio);
        values.put(DataBase.ENDERECO_UF                 , this.UF);
        values.put(DataBase.ENDERECO_LATITUDE           , this.latitude);
        values.put(DataBase.ENDERECO_LONGITUDE          , this.longitude);
        values.put(DataBase.ENDERECO_ENDERECO_APROXIMADO, this.enderecoAproximado? 1:0);

        return values;
    }

    @Override
    public void removeChildren(Context context) {
        /*
        TODO FALTA DAR UPDATE NOS CASOS ONDE ESTA ENTIDADE ESTA RELACIONADA

        CRIAR METODOS USANDO EXECSQL
         */
    }

    public DropDownItemVO getTipoImovel() {
        return tipoImovel;
    }

    public void setTipoImovel(DropDownItemVO tipoImovel) {
        this.tipoImovel = tipoImovel;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getUF() {
        return UF;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isEnderecoAproximado() {
        return enderecoAproximado;
    }

    public void setEnderecoAproximado(boolean enderecoAproximado) {
        this.enderecoAproximado = enderecoAproximado;
    }

    @Override
    public String toString() {

        if(this.municipiooo){
            return (getMunicipio() == null ? "" : getMunicipio().toUpperCase() + " - ") + (getUF() == null ? "" : getUF().toUpperCase());
        }else{
            return (getLogradouro() == null ? "" : getLogradouro().toUpperCase() + ", ") + (getBairro() == null ? "" : getBairro().toUpperCase() + ", ") + (getMunicipio() == null ? "" : getMunicipio().toUpperCase() + " - ") + (getUF() == null ? "" : getUF().toUpperCase());
        }
    }

    @Override
    public boolean equals(Object o) {
        EnderecoImovelVO enderecoImovelVO = (EnderecoImovelVO) o;
        return this.getLogradouro().toUpperCase().equals(enderecoImovelVO.getLogradouro().toUpperCase());
    }

    public boolean isMunicipiooo() {
        return municipiooo;
    }

    public void setMunicipiooo(boolean municipiooo) {
        this.municipiooo = municipiooo;
    }
}
