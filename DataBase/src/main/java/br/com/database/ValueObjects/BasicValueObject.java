package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import br.com.database.DataBase;

/**
 * Created by Matheus on 12/08/2014.
 */
public abstract class BasicValueObject implements Serializable{

    @JsonIgnore
    private long   id;
    @JsonProperty("Id")
    private long   idServer;
    @JsonIgnore
    private Date   creationDate;
    @JsonIgnore
    private Date   lastUpdateDate;
    @JsonIgnore
    private String token;

    public BasicValueObject() {}

    public BasicValueObject(long idServer, String token) {
        this.id             = 0L;
        this.idServer       = idServer;
        this.creationDate   = Calendar.getInstance().getTime();
        this.lastUpdateDate = Calendar.getInstance().getTime();
        this.token          = token;
    }

    public BasicValueObject(Context context, Cursor cursor){

        Calendar creationDateCalendar = Calendar.getInstance();
        creationDateCalendar.setTimeInMillis(cursor.getLong(2));

        Calendar lastUpdateDateCalendar = Calendar.getInstance();
        lastUpdateDateCalendar.setTimeInMillis(cursor.getLong(3));

        this.id             = cursor.getLong(0);
        this.idServer       = cursor.getLong(1);
        this.creationDate   = creationDateCalendar  .getTime();
        this.lastUpdateDate = lastUpdateDateCalendar.getTime();
        this.token          = cursor.getString(4);
    }

    public ContentValues getContentValues(Context context){
        ContentValues values = new ContentValues();

        values.put(DataBase.ID_SERVER       , this.idServer);
        values.put(DataBase.CREATION_DATE   , this.creationDate.getTime());
        values.put(DataBase.LAST_UPDATE_DATE, this.lastUpdateDate.getTime());
        values.put(DataBase.TOKEN           , this.token);

        return values;
    }

    public abstract void removeChildren(Context context);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdServer() {
        return idServer;
    }

    public void setIdServer(long idServer) {
        this.idServer = idServer;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
