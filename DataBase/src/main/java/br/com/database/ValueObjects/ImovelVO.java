package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import br.com.database.DataBase;
import br.com.database.TableMethods.EnderecoImovelMethods;
import br.com.database.TableMethods.ImagemMethods;
import br.com.database.TableMethods.UnidadeComercializacaoMethods;
import br.com.database.TableMethods.UnidadeEdificacaoMethods;
import br.com.database.TableMethods.UnidadeMethods;
import br.com.database.TableMethods.UnidadeTerrenoMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class ImovelVO extends BasicValueObject{

    @JsonIgnore
    public static String DATAKEY = ImovelVO.class.getSimpleName();

    @JsonProperty("enderecoImovelVO")
    private EnderecoImovelVO         enderecoImovelVO;
    @JsonProperty("unidadeVO")
    private UnidadeVO                unidadeVO;
    @JsonProperty("unidadeComercializacaoVO")
    private UnidadeComercializacaoVO unidadeComercializacaoVO;
    @JsonProperty("unidadeEdificacaoVO")
    private UnidadeEdificacaoVO      unidadeEdificacaoVO;
    @JsonProperty("unidadeTerrenoVO")
    private UnidadeTerrenoVO         unidadeTerrenoVO;
    @JsonProperty("Imagem")
    private List<ImagemVO>   imagemVOs;
    @JsonProperty("tipoAtualizacao")
    private int tipoAtualizacao;

    public ImovelVO() {
        super(0L, "");

        this.enderecoImovelVO         = new EnderecoImovelVO();
        this.unidadeVO                = new UnidadeVO();
        this.unidadeComercializacaoVO = new UnidadeComercializacaoVO();
        this.unidadeEdificacaoVO      = new UnidadeEdificacaoVO();
        this.unidadeTerrenoVO         = new UnidadeTerrenoVO();
        this.imagemVOs                = new ArrayList<ImagemVO>();
        this.tipoAtualizacao = 0;

    }

    public ImovelVO(long idServer, String token, EnderecoImovelVO enderecoImovelVO, UnidadeVO unidadeVO, UnidadeComercializacaoVO unidadeComercializacaoVO, UnidadeEdificacaoVO unidadeEdificacaoVO, UnidadeTerrenoVO unidadeTerrenoVO) {
        super(idServer, token);
        this.enderecoImovelVO         = enderecoImovelVO;
        this.unidadeVO                = unidadeVO;
        this.unidadeComercializacaoVO = unidadeComercializacaoVO;
        this.unidadeEdificacaoVO      = unidadeEdificacaoVO;
        this.unidadeTerrenoVO         = unidadeTerrenoVO;
        this.imagemVOs                = new ArrayList<ImagemVO>();
        this.tipoAtualizacao = 0;
    }

    public ImovelVO(Context context, Cursor cursor) {
        super(context, cursor);

        if(cursor.getInt(5) <= 0){
            this.enderecoImovelVO = null;
        }else{
            this.enderecoImovelVO = (EnderecoImovelVO) new EnderecoImovelMethods(context).get(cursor.getInt(5));
        }

        if(cursor.getInt(6) <= 0){
            this.unidadeVO = null;
        }else{
            this.unidadeVO = (UnidadeVO) new UnidadeMethods(context).get(cursor.getInt(6));
        }

        if(cursor.getInt(7) <= 0){
            this.unidadeComercializacaoVO = null;
        }else{
            this.unidadeComercializacaoVO = (UnidadeComercializacaoVO) new UnidadeComercializacaoMethods(context).get(cursor.getInt(7));
        }

        if(cursor.getInt(8) <= 0){
            this.unidadeEdificacaoVO = null;
        }else{
            this.unidadeEdificacaoVO = (UnidadeEdificacaoVO) new UnidadeEdificacaoMethods(context).get(cursor.getInt(8));
        }

        if(cursor.getInt(9) <= 0){
            this.unidadeTerrenoVO = null;
        }else{
            this.unidadeTerrenoVO = (UnidadeTerrenoVO) new UnidadeTerrenoMethods(context).get(cursor.getInt(9));
        }

        if(this.unidadeVO != null){
            this.imagemVOs = new ImagemMethods(context).getImagensByUnidadeId(this.getId());
        }

        this.tipoAtualizacao = cursor.getInt(10);
    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        if(this.enderecoImovelVO != null){

            if(!this.enderecoImovelVO.isEnderecoAproximado()){
                if(this.enderecoImovelVO.getLogradouro() == null || this.enderecoImovelVO.getLogradouro().equals("")){
                    Toast.makeText(context, "Preencha todas as informações do endereço!", Toast.LENGTH_SHORT).show();
                    return null;
                }

                if(this.enderecoImovelVO.getNumero() == null || this.enderecoImovelVO.getNumero().equals("")){
                    Toast.makeText(context, "Preencha todas as informações do endereço!", Toast.LENGTH_SHORT).show();
                    return null;
                }

                if(this.enderecoImovelVO.getCep() == null || this.enderecoImovelVO.getCep().equals("")){
                    Toast.makeText(context, "Preencha todas as informações do endereço!", Toast.LENGTH_SHORT).show();
                    return null;
                }
            }


            if(this.enderecoImovelVO.getBairro() == null || this.enderecoImovelVO.getBairro().equals("")){
                Toast.makeText(context, "Preencha todas as informações do endereço!", Toast.LENGTH_SHORT).show();
                return null;
            }

            if(this.enderecoImovelVO.getMunicipio() == null || this.enderecoImovelVO.getMunicipio().equals("")){
                Toast.makeText(context, "Preencha todas as informações do endereço!", Toast.LENGTH_SHORT).show();
                return null;
            }

            if(this.enderecoImovelVO.getUF() == null || this.enderecoImovelVO.getUF().equals("") || this.enderecoImovelVO.getUF().equals("-")){
                Toast.makeText(context, "Preencha todas as informações do endereço!", Toast.LENGTH_SHORT).show();
                return null;
            }

            if(this.enderecoImovelVO.getTipoImovel() == null || this.enderecoImovelVO.getTipoImovel().getIdServer() == 0L){
                Toast.makeText(context, "Preencha todas as informações do endereço!", Toast.LENGTH_SHORT).show();
                return null;
            }

            this.enderecoImovelVO = (EnderecoImovelVO) new EnderecoImovelMethods(context).save(this.enderecoImovelVO);
            values.put(DataBase.IMOVEL_ENDERECO_IMOVEL, this.enderecoImovelVO.getId());
        }else{
            values.put(DataBase.IMOVEL_ENDERECO_IMOVEL, 0L);
        }

        if(this.unidadeVO != null){
            this.unidadeVO.setEnderecoImovelVO(this.enderecoImovelVO);

            if(this.unidadeVO.getDadoOrigem() == null || this.unidadeVO.getDadoOrigem().getIdServer() == 0){
                Toast.makeText(context, "Dado Origem é Obrigatório", Toast.LENGTH_SHORT).show();
                return null;
            }

            this.unidadeVO = (UnidadeVO) new UnidadeMethods(context).save(this.unidadeVO);
            values.put(DataBase.IMOVEL_UNIDADE, this.unidadeVO.getId());
        }else{
            values.put(DataBase.IMOVEL_UNIDADE, 0L);
        }

        if(this.unidadeComercializacaoVO != null){
            this.unidadeComercializacaoVO.setUnidadeVO(this.unidadeVO);
            this.unidadeComercializacaoVO = (UnidadeComercializacaoVO) new UnidadeComercializacaoMethods(context).save(this.unidadeComercializacaoVO);
            values.put(DataBase.IMOVEL_UNIDADE_COMERCIALIZACAO, this.unidadeComercializacaoVO.getId());
        }else{
            values.put(DataBase.IMOVEL_UNIDADE_COMERCIALIZACAO, 0L);
        }

        if(this.unidadeEdificacaoVO != null){
            this.unidadeEdificacaoVO.setUnidadeVO(this.unidadeVO);
            this.unidadeEdificacaoVO = (UnidadeEdificacaoVO) new UnidadeEdificacaoMethods(context).save(this.unidadeEdificacaoVO);
            values.put(DataBase.IMOVEL_UNIDADE_EDIFICACAO, this.unidadeEdificacaoVO.getId());
        }else{
            values.put(DataBase.IMOVEL_UNIDADE_EDIFICACAO, 0L);
        }

        if(this.unidadeTerrenoVO != null){
            this.unidadeTerrenoVO.setUnidadeVO(this.unidadeVO);
            this.unidadeTerrenoVO = (UnidadeTerrenoVO) new UnidadeTerrenoMethods(context).save(this.unidadeTerrenoVO);
            values.put(DataBase.IMOVEL_UNIDADE_TERRENO, this.unidadeTerrenoVO.getId());
        }else{
            values.put(DataBase.IMOVEL_UNIDADE_TERRENO, 0L);
        }

        if(this.imagemVOs != null){
            for (BasicValueObject basicValueObject : this.imagemVOs){
                ImagemVO imagemVO = (ImagemVO) basicValueObject;
                if(unidadeVO != null && unidadeVO.getId() > 0L){
                    imagemVO.setUnidadeVO(unidadeVO);
                }
                new ImagemMethods(context).save(imagemVO);
            }
        }

        values.put(DataBase.IMOVEL_UNIDADE_TERRENO, this.unidadeTerrenoVO.getId());

        return values;
    }

    @Override
    public void removeChildren(Context context) {

    }

    public EnderecoImovelVO getEnderecoImovelVO() {
        return enderecoImovelVO;
    }

    public void setEnderecoImovelVO(EnderecoImovelVO enderecoImovelVO) {
        this.enderecoImovelVO = enderecoImovelVO;
    }

    public UnidadeVO getUnidadeVO() {
        return unidadeVO;
    }

    public void setUnidadeVO(UnidadeVO unidadeVO) {
        this.unidadeVO = unidadeVO;
        this.unidadeVO.setEnderecoImovelVO(this.enderecoImovelVO);
    }

    public UnidadeComercializacaoVO getUnidadeComercializacaoVO() {
        return unidadeComercializacaoVO;
    }

    public void setUnidadeComercializacaoVO(UnidadeComercializacaoVO unidadeComercializacaoVO) {
        this.unidadeComercializacaoVO = unidadeComercializacaoVO;
        this.unidadeComercializacaoVO.setUnidadeVO(this.unidadeVO);
    }

    public UnidadeEdificacaoVO getUnidadeEdificacaoVO() {
        return unidadeEdificacaoVO;
    }

    public void setUnidadeEdificacaoVO(UnidadeEdificacaoVO unidadeEdificacaoVO) {
        this.unidadeEdificacaoVO = unidadeEdificacaoVO;
        this.unidadeEdificacaoVO.setUnidadeVO(this.unidadeVO);
    }

    public UnidadeTerrenoVO getUnidadeTerrenoVO() {
        return unidadeTerrenoVO;
    }

    public void setUnidadeTerrenoVO(UnidadeTerrenoVO unidadeTerrenoVO) {
        this.unidadeTerrenoVO = unidadeTerrenoVO;
        this.unidadeTerrenoVO.setUnidadeVO(this.unidadeVO);
    }

    public List<ImagemVO> getImagemVOs() {
        return imagemVOs;
    }

    public void setImagemVOs(List<ImagemVO> imagemVOs) {
        this.imagemVOs = imagemVOs;
    }

    public int getTipoAtualizacao() {
        return tipoAtualizacao;
    }

    public void setTipoAtualizacao(int tipoAtualizacao) {
        this.tipoAtualizacao = tipoAtualizacao;
    }
}
