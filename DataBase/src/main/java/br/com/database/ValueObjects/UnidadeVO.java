package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

import br.com.database.DataBase;
import br.com.database.LocalCoreDataDAO;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.TableMethods.EnderecoImovelMethods;
import br.com.database.TableMethods.ImagemMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeVO extends BasicValueObject{

    @JsonIgnore
    public static String DATAKEY = UnidadeVO.class.getSimpleName();

    @JsonProperty("subTipoImovel")
    private DropDownItemVO   subTipoImovel;
    @JsonProperty("enderecoImovelVO")
    private EnderecoImovelVO enderecoImovelVO;
    @JsonProperty("dadoOrigem")
    private DropDownItemVO   dadoOrigem;
    @JsonProperty("codigoEmbraesp")
    private String codigoEmbraesp;
    @JsonProperty("informacaoAuditada")
    private String informacaoAuditada;
    @JsonProperty("nomeUsuario")
    private String nomeUsuario;

    public UnidadeVO(){
        super(0L, "");
        this.subTipoImovel      = null;
        this.enderecoImovelVO   = null;
        this.dadoOrigem         = null;
        this.codigoEmbraesp     = "";
        this.informacaoAuditada = "";
        this.nomeUsuario        = "";
    }

    public UnidadeVO(long idServer, String token, DropDownItemVO subTipoImovel, EnderecoImovelVO enderecoImovelVO, DropDownItemVO dadoOrigem, String codigoEmbraesp, String informacaoAuditada, String nomeUsuario) {
        super(idServer, token);
        this.subTipoImovel      = subTipoImovel;
        this.enderecoImovelVO   = enderecoImovelVO;
        this.dadoOrigem         = dadoOrigem;
        this.codigoEmbraesp     = codigoEmbraesp;
        this.informacaoAuditada = informacaoAuditada;
        this.nomeUsuario        = nomeUsuario;
    }

    public UnidadeVO(Context context, Cursor cursor) {
        super(context, cursor);

        if(cursor.getInt(5) <= 0){
            this.subTipoImovel = null;
        }else{
            this.subTipoImovel = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(5));
        }

        if(cursor.getInt(6) <= 0){
            this.enderecoImovelVO = null;
        }else{
            this.enderecoImovelVO = (EnderecoImovelVO) new EnderecoImovelMethods(context).get(cursor.getInt(6));
        }

        if(cursor.getInt(7) <= 0){
            this.dadoOrigem = null;
        }else{
            this.dadoOrigem = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(7));
        }

        this.codigoEmbraesp     = cursor.getString(8);
        this.informacaoAuditada = cursor.getString(9);
        this.nomeUsuario        = cursor.getString(10);
    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        if(this.subTipoImovel != null && this.subTipoImovel.getId() > 0){
            values.put(DataBase.UNIDADE_SUB_TIPO_IMOVEL, this.subTipoImovel.getId());
        }else{
            values.put(DataBase.UNIDADE_SUB_TIPO_IMOVEL, 0);
        }

        if(this.enderecoImovelVO != null && this.enderecoImovelVO.getId() > 0){
            values.put(DataBase.UNIDADE_ENDERECO_IMOVEL, this.enderecoImovelVO.getId());
        }else{
            values.put(DataBase.UNIDADE_ENDERECO_IMOVEL, 0);
        }

        if(this.dadoOrigem != null && this.dadoOrigem.getId() > 0){
            values.put(DataBase.UNIDADE_DADO_ORIGEM, this.dadoOrigem.getId());
        }else{
            values.put(DataBase.UNIDADE_DADO_ORIGEM, 0);
        }

        if(this.dadoOrigem == null || this.dadoOrigem.getIdServer() == 0){
            Toast.makeText(context, "Dado Origem é Obrigatório", Toast.LENGTH_SHORT).show();
            return null;
        }

        values.put(DataBase.UNIDADE_CODIGO_EMBRAESP    , this.codigoEmbraesp);
        values.put(DataBase.UNIDADE_INFORMACAO_AUDITADA, this.informacaoAuditada);
        values.put(DataBase.UNIDADE_NOME_USUARIO       , this.nomeUsuario);

        return values;
    }

    @Override
    public void removeChildren(Context context) {
        /*
        TODO FALTA DAR UPDATE NOS CASOS ONDE ESTA ENTIDADE ESTA RELACIONADA

        CRIAR METODOS USANDO EXECSQL
         */
    }

    public DropDownItemVO getSubTipoImovel() {
        return subTipoImovel;
    }

    public void setSubTipoImovel(DropDownItemVO subTipoImovel) {
        this.subTipoImovel = subTipoImovel;
    }

    public EnderecoImovelVO getEnderecoImovelVO() {
        return enderecoImovelVO;
    }

    public void setEnderecoImovelVO(EnderecoImovelVO enderecoImovelVO) {
        this.enderecoImovelVO = enderecoImovelVO;
    }

    public DropDownItemVO getDadoOrigem() {
        return dadoOrigem;
    }

    public void setDadoOrigem(DropDownItemVO dadoOrigem) {
        this.dadoOrigem = dadoOrigem;
    }

    public String getCodigoEmbraesp() {
        return codigoEmbraesp;
    }

    public void setCodigoEmbraesp(String codigoEmbraesp) {
        this.codigoEmbraesp = codigoEmbraesp;
    }

    public String getInformacaoAuditada() {
        return informacaoAuditada;
    }

    public void setInformacaoAuditada(String informacaoAuditada) {
        this.informacaoAuditada = informacaoAuditada;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

}
