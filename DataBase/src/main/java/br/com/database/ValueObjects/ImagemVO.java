package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.RandomStringUtils;

import br.com.database.DataBase;
import br.com.database.LocalCoreDataDAO;
import br.com.database.TableMethods.UnidadeMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class ImagemVO extends BasicValueObject{

    public static String DATAKEY = ImagemVO.class.getSimpleName();

    @JsonProperty("ImagePackage")
    private String    imagePackage;
    @JsonProperty("ImageBytes")
    private byte[]    imageBytes;
    @JsonProperty("unidadeVO")
    private UnidadeVO unidadeVO;
    @JsonProperty("ImageUrl")
    private String imageUrl;
    @JsonIgnore
    private String imagePath;
    @JsonProperty("ImagePath")
    private String imageName;

    public ImagemVO() {
        super(0L, "");

        this.imagePackage = RandomStringUtils.random(32, true, true);
        this.unidadeVO    = new UnidadeVO();

    }

    public ImagemVO(long idServer, String token, UnidadeVO unidadeVO) {
        super(idServer, token);
        this.imagePackage = RandomStringUtils.random(32, true, true);
        this.unidadeVO    = unidadeVO;
    }

    public ImagemVO(Context context, Cursor cursor) {
        super(context, cursor);

        this.imagePackage = cursor.getString(5);

        if(this.imagePackage != null && !this.imagePackage.equals("")){
            this.imageBytes = (byte[]) LocalCoreDataDAO.get(context, imagePackage);
        }

        if(cursor.getInt(6) <= 0){
            this.unidadeVO = null;
        }else{
            this.unidadeVO = (UnidadeVO) new UnidadeMethods(context).get(cursor.getInt(6));
        }
        this.imageUrl = cursor.getString(7);
        this.imagePath = cursor.getString(8);

    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        if(this.unidadeVO != null){
            this.unidadeVO = (UnidadeVO) new UnidadeMethods(context).save(this.unidadeVO);
            values.put(DataBase.IMAGENS_UNIDADE, this.unidadeVO.getId());
        }else{
            values.put(DataBase.IMAGENS_UNIDADE, 0L);
        }

        if(this.imagePackage != null && !this.imagePackage.equals("")){
            this.imageBytes = (byte[]) LocalCoreDataDAO.get(context, imagePackage);
        }

        if(this.imageBytes != null && this.imageBytes.length > 0){
            LocalCoreDataDAO.save(context, this.imageBytes , this.imagePackage);
            values.put(DataBase.IMAGENS_PACKAGE, this.imagePackage);
        }else{
            values.put(DataBase.IMAGENS_PACKAGE, "");
        }

        values.put(DataBase.IMAGENS_URL, this.imageUrl);
        values.put(DataBase.IMAGENS_PATH, this.imagePath);

        return values;
    }

    @Override
    public void removeChildren(Context context) {
        if(this.imageBytes != null && this.imageBytes.length > 0){
            LocalCoreDataDAO.save(context, null , this.imagePackage);
        }
    }

    public String getImagePackage() {
        return imagePackage;
    }

    public void setImagePackage(String imagePackage) {
        this.imagePackage = imagePackage;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public UnidadeVO getUnidadeVO() {
        return unidadeVO;
    }

    public void setUnidadeVO(UnidadeVO unidadeVO) {
        this.unidadeVO = unidadeVO;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;

        if(imagePath != null && !imagePath.equals("")){
            String[] split = imagePath.split("/");
            if(split.length > 0){
                imageName = split[split.length - 1];
            }
        }

    }

    public String getImageName() {

        if(imagePath != null && !imagePath.equals("")){
            String[] split = imagePath.split("/");
            if(split.length > 0){
                imageName = split[split.length - 1];
            }
        }

        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
