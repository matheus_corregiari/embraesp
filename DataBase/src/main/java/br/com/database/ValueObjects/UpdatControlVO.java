package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import br.com.database.DataBase;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UpdatControlVO extends BasicValueObject{

    public static String DATAKEY = UpdatControlVO.class.getSimpleName();

    private String tableName;

    public UpdatControlVO() {}

    public UpdatControlVO(long idServer, String token, String tableName) {
        super(idServer, token);
        this.tableName = tableName;
    }

    public UpdatControlVO(Context context, Cursor cursor) {
        super(context, cursor);
        this.tableName = cursor.getString(5);
    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        values.put(DataBase.UPDATE_CONTROL_TABLE_NAME , this.tableName);

        return values;
    }

    @Override
    public void removeChildren(Context context) {

    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
