package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import br.com.database.DataBase;
import br.com.database.TableMethods.DropDownItensMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class DropDownItemVO extends BasicValueObject{

    public static String DATAKEY = DropDownItemVO.class.getSimpleName();

    @JsonProperty("code")
    private String code;
    @JsonProperty("value")
    private String value;
    @JsonIgnore
    private DropDownItemVO dropDownItemVO;

    public DropDownItemVO() {
        super(0L, "");
        this.code = "";
    }

    public DropDownItemVO(long idServer, String token, String code, String value, DropDownItemVO dropDownItemVO) {
        super(idServer, token);
        this.code           = code;
        this.value          = value;
        this.dropDownItemVO = dropDownItemVO;
    }

    public DropDownItemVO(Context context, Cursor cursor) {
        super(context, cursor);
        this.code  = cursor.getString(5);
        this.value = cursor.getString(6);

        if(cursor.getInt(7) <= 0){
            this.dropDownItemVO = null;
        }else{
            this.dropDownItemVO = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(7));
        }
    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        values.put(DataBase.DROPDOWN_ITENS_CODE , this.code);
        values.put(DataBase.DROPDOWN_ITENS_VALUE, this.value);

        if(this.dropDownItemVO != null && this.dropDownItemVO.getId() > 0){
            values.put(DataBase.DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM, this.dropDownItemVO.getId());
        }else{
            values.put(DataBase.DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM, 0);
        }

        return values;
    }

    @Override
    public void removeChildren(Context context) {
        DropDownItensMethods dropDownItensMethods = new DropDownItensMethods(context);
        List<BasicValueObject> basicValueObjects = new DropDownItensMethods(context).getDropDownItensByParent(this.getId());

        if(basicValueObjects != null){
            for (BasicValueObject basicValueObject : basicValueObjects){
                DropDownItemVO dropDownItemVO = (DropDownItemVO) basicValueObject;
                new DropDownItensMethods(context).delete(dropDownItemVO.getId());
            }
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DropDownItemVO getDropDownItemVO() {
        return dropDownItemVO;
    }

    public void setDropDownItemVO(DropDownItemVO dropDownItemVO) {
        this.dropDownItemVO = dropDownItemVO;
    }
}
