package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;

import java.util.Calendar;
import java.util.Date;

import br.com.database.DataBase;
import br.com.database.DateDeserializer;
import br.com.database.DateSerializer;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.TableMethods.UnidadeMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeComercializacaoVO extends BasicValueObject{

    @JsonIgnore
    public static String DATAKEY = UnidadeComercializacaoVO.class.getSimpleName();

    @JsonProperty("unidadeVO")
    private UnidadeVO      unidadeVO;
    @JsonProperty("tipoVenda")
    private DropDownItemVO tipoVenda;
    @JsonProperty("tipoLocacao")
    private DropDownItemVO tipoLocacao;
    @JsonProperty("fonte")
    private DropDownItemVO fonte;
    @JsonProperty("disponivelVenda")
    private boolean        disponivelVenda;
    @JsonProperty("disponivelLocacao")
    private boolean        disponivelLocacao;
    @JsonProperty("valorVenda")
    private double         valorVenda;
    @JsonProperty("valorLocacao")
    private double         valorLocacao;
    @JsonProperty("valorCondominioLocacao")
    private double         valorCondominioLocacao;
    @JsonProperty("valorIptuLocacao")
    private double         valorIptuLocacao;
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    @JsonProperty("dateCreationVenda")
    private Date           dateCreationVenda;
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    @JsonProperty("dateUpdateVenda")
    private Date           dateUpdateVenda;
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    @JsonProperty("dateCreationLocacao")
    private Date           dateCreationLocacao;
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    @JsonProperty("dateUpdateLocacao")
    private Date           dateUpdateLocacao;
    @JsonProperty("nome")
    private String         nome;
    @JsonProperty("telefone")
    private String         telefone;
    @JsonProperty("email")
    private String         email;
    @JsonProperty("observacoes")
    private String         observacoes;

    public UnidadeComercializacaoVO(){
        super(0L, "");

        Calendar calendarAux = Calendar.getInstance();
        calendarAux.set(1902, Calendar.JANUARY, 1, 0, 0, 0);

        this.unidadeVO              = null;
        this.tipoVenda              = null;
        this.tipoLocacao            = null;
        this.fonte                  = null;
        this.disponivelVenda        = false;
        this.disponivelLocacao      = false;
        this.valorVenda             = 0D;
        this.valorLocacao           = 0D;
        this.valorCondominioLocacao = 0D;
        this.valorIptuLocacao       = 0D;
        this.dateCreationVenda      = calendarAux.getTime();
        this.dateUpdateVenda        = calendarAux.getTime();
        this.dateCreationLocacao    = calendarAux.getTime();
        this.dateUpdateLocacao      = calendarAux.getTime();
        this.nome                   = "";
        this.telefone               = "";
        this.email                  = "";
        this.observacoes            = "";
    }

    public UnidadeComercializacaoVO(long idServer, String token, UnidadeVO unidadeVO, DropDownItemVO tipoVenda, DropDownItemVO tipoLocacao, DropDownItemVO fonte, boolean disponivelVenda, boolean disponivelLocacao, double valorVenda, double valorLocacao, double valorCondominioLocacao, double valorIptuLocacao, Date dateCreationVenda, Date dateUpdateVenda, Date dateCreationLocacao, Date dateUpdateLocacao, String nome, String telefone, String email, String observacoes) {
        super(idServer, token);
        this.unidadeVO              = unidadeVO;
        this.tipoVenda              = tipoVenda;
        this.tipoLocacao            = tipoLocacao;
        this.fonte                  = fonte;
        this.disponivelVenda        = disponivelVenda;
        this.disponivelLocacao      = disponivelLocacao;
        this.valorVenda             = valorVenda;
        this.valorLocacao           = valorLocacao;
        this.valorCondominioLocacao = valorCondominioLocacao;
        this.valorIptuLocacao       = valorIptuLocacao;
        this.dateCreationVenda      = dateCreationVenda;
        this.dateUpdateVenda        = dateUpdateVenda;
        this.dateCreationLocacao    = dateCreationLocacao;
        this.dateUpdateLocacao      = dateUpdateLocacao;
        this.nome                   = nome;
        this.telefone               = telefone;
        this.email                  = email;
        this.observacoes            = observacoes;
    }

    public UnidadeComercializacaoVO(Context context, Cursor cursor) {
        super(context, cursor);

        Calendar creationDateCalendarVenda = Calendar.getInstance();
        creationDateCalendarVenda.setTimeInMillis(cursor.getLong(15));
        Calendar updateDateCalendarVenda = Calendar.getInstance();
        updateDateCalendarVenda.setTimeInMillis(cursor.getLong(16));
        Calendar creationDateCalendarLocacao = Calendar.getInstance();
        creationDateCalendarLocacao.setTimeInMillis(cursor.getLong(17));
        Calendar updateDateCalendarLocacao = Calendar.getInstance();
        updateDateCalendarLocacao.setTimeInMillis(cursor.getLong(18));

        if(cursor.getInt(5) <= 0){
            this.unidadeVO = null;
        }else{
            this.unidadeVO = (UnidadeVO) new UnidadeMethods(context).get(cursor.getInt(5));
        }

        if(cursor.getInt(6) <= 0){
            this.tipoVenda = null;
        }else{
            this.tipoVenda = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(6));
        }

        if(cursor.getInt(7) <= 0){
            this.tipoLocacao = null;
        }else{
            this.tipoLocacao = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(7));
        }

        if(cursor.getInt(8) <= 0){
            this.fonte = null;
        }else{
            this.fonte = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(8));
        }

        this.disponivelVenda        = cursor.getInt(9)  == 1;
        this.disponivelLocacao      = cursor.getInt(10) == 1;
        this.valorVenda             = cursor.getDouble(11);
        this.valorLocacao           = cursor.getDouble(12);
        this.valorCondominioLocacao = cursor.getDouble(13);
        this.valorIptuLocacao       = cursor.getDouble(14);
        this.dateCreationVenda      = creationDateCalendarVenda.getTime();
        this.dateUpdateVenda        = updateDateCalendarVenda.getTime();
        this.dateCreationLocacao    = creationDateCalendarLocacao.getTime();
        this.dateUpdateLocacao      = updateDateCalendarLocacao.getTime();
        this.nome                   = cursor.getString(19);
        this.telefone               = cursor.getString(20);
        this.email                  = cursor.getString(21);
        this.observacoes            = cursor.getString(22);

    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        if(this.unidadeVO != null && this.unidadeVO.getId() > 0){
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_UNIDADE, this.unidadeVO.getId());
        }else{
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_UNIDADE, 0);
        }

        if(this.tipoVenda != null && this.tipoVenda.getId() > 0){
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_TIPO_VENDA, this.tipoVenda.getId());
        }else{
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_TIPO_VENDA, 0);
        }

        if(this.tipoLocacao != null && this.tipoLocacao.getId() > 0){
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_TIPO_LOCACAO, this.tipoLocacao.getId());
        }else{
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_TIPO_LOCACAO, 0);
        }

        if(this.fonte != null && this.fonte.getId() > 0){
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_FONTE, this.fonte.getId());
        }else{
            values.put(DataBase.UNIDADE_COMERCIALIZACAO_FONTE, 0);
        }

        /*
        TODO VALIDATORS DE INSERCAO COLOCAR NOS GETTERS E SETTERS E MUDAR CONSTRUTORES PARA USAR OS SETS
         */

        values.put(DataBase.UNIDADE_COMERCIALIZACAO_DISPONIVEL_VENDA        , this.disponivelVenda? 1:0);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_DISPONIVEL_LOCACAO      , this.disponivelLocacao? 1:0);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_VALOR_VENDA             , this.valorVenda);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_VALOR_LOCACAO           , this.valorLocacao);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_VALOR_CONDOMINIO_LOCACAO, this.valorCondominioLocacao);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_VALOR_IPTU_LOCACAO      , this.valorIptuLocacao);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_CREATION_DATE_VENDA     , this.dateCreationVenda.getTime());
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_LAST_UPDATE_VENDA       , this.dateUpdateVenda.getTime());
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_CREATION_DATE_LOCACAO   , this.dateCreationLocacao.getTime());
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_LAST_UPDATE_LOCACAO     , this.dateUpdateLocacao.getTime());
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_NOME                    , this.nome);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_TELEFONE                , this.telefone);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_EMAIL                   , this.email);
        values.put(DataBase.UNIDADE_COMERCIALIZACAO_OBSERVACOES             , this.observacoes);

        return values;
    }

    @Override
    public void removeChildren(Context context) {
        /*
        TODO FALTA DAR UPDATE NOS CASOS ONDE ESTA ENTIDADE ESTA RELACIONADA
        CRIAR METODOS USANDO EXECSQL
         */
    }

    public UnidadeVO getUnidadeVO() {
        return unidadeVO;
    }

    public void setUnidadeVO(UnidadeVO unidadeVO) {
        this.unidadeVO = unidadeVO;
    }

    public DropDownItemVO getTipoVenda() {
        return tipoVenda;
    }

    public void setTipoVenda(DropDownItemVO tipoVenda) {
        this.tipoVenda = tipoVenda;
    }

    public DropDownItemVO getTipoLocacao() {
        return tipoLocacao;
    }

    public void setTipoLocacao(DropDownItemVO tipoLocacao) {
        this.tipoLocacao = tipoLocacao;
    }

    public DropDownItemVO getFonte() {
        return fonte;
    }

    public void setFonte(DropDownItemVO fonte) {
        this.fonte = fonte;
    }

    public boolean isDisponivelVenda() {
        return disponivelVenda;
    }

    public void setDisponivelVenda(boolean disponivelVenda) {
        this.disponivelVenda = disponivelVenda;
    }

    public boolean isDisponivelLocacao() {
        return disponivelLocacao;
    }

    public void setDisponivelLocacao(boolean disponivelLocacao) {
        this.disponivelLocacao = disponivelLocacao;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(double valorVenda) {
        this.valorVenda = valorVenda;
    }

    public double getValorLocacao() {
        return valorLocacao;
    }

    public void setValorLocacao(double valorLocacao) {
        this.valorLocacao = valorLocacao;
    }

    public double getValorCondominioLocacao() {
        return valorCondominioLocacao;
    }

    public void setValorCondominioLocacao(double valorCondominioLocacao) {
        this.valorCondominioLocacao = valorCondominioLocacao;
    }

    public double getValorIptuLocacao() {
        return valorIptuLocacao;
    }

    public void setValorIptuLocacao(double valorIptuLocacao) {
        this.valorIptuLocacao = valorIptuLocacao;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Date getDateCreationVenda() {
        return dateCreationVenda;
    }

    public void setDateCreationVenda(Date dateCreationVenda) {
        this.dateCreationVenda = dateCreationVenda;
    }

    public Date getDateUpdateVenda() {
        return dateUpdateVenda;
    }

    public void setDateUpdateVenda(Date dateUpdateVenda) {
        this.dateUpdateVenda = dateUpdateVenda;
    }

    public Date getDateCreationLocacao() {
        return dateCreationLocacao;
    }

    public void setDateCreationLocacao(Date dateCreationLocacao) {
        this.dateCreationLocacao = dateCreationLocacao;
    }

    public Date getDateUpdateLocacao() {
        return dateUpdateLocacao;
    }

    public void setDateUpdateLocacao(Date dateUpdateLocacao) {
        this.dateUpdateLocacao = dateUpdateLocacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
