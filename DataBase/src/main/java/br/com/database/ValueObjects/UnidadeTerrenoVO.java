package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.database.DataBase;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.TableMethods.UnidadeMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeTerrenoVO extends BasicValueObject{

    @JsonIgnore
    public static String DATAKEY = UnidadeTerrenoVO.class.getSimpleName();

    @JsonProperty("unidadeVO")
    private UnidadeVO      unidadeVO;
    @JsonProperty("potencialidade")
    private DropDownItemVO potencialidade;
    @JsonProperty("nomeLoteamento")
    private String         nomeLoteamento;
    @JsonProperty("zona")
    private String         zona;
    @JsonProperty("setor")
    private String         setor;
    @JsonProperty("quadra")
    private String         quadra;
    @JsonProperty("lote")
    private String         lote;
    @JsonProperty("frente")
    private double         frente;
    @JsonProperty("fundos")
    private double         fundos;
    @JsonProperty("areaTotal")
    private double         areaTotal;
    @JsonProperty("observacoes")
    private String         observacoes;

    public UnidadeTerrenoVO(){
        super(0L, "");
        this.unidadeVO      = null;
        this.potencialidade = null;
        this.nomeLoteamento = "";
        this.zona           = "";
        this.setor          = "";
        this.quadra         = "";
        this.lote           = "";
        this.frente         = 0D;
        this.fundos         = 0D;
        this.areaTotal      = 0D;
        this.observacoes    = "";
    }

    public UnidadeTerrenoVO(long idServer, String token, UnidadeVO unidadeVO, DropDownItemVO potencialidade,
                            String nomeLoteamento, String zona, String setor, String quadra, String lote,
                            double frente, double fundos, double areaTotal, String observacoes) {
        super(idServer, token);
        this.unidadeVO      = unidadeVO;
        this.potencialidade = potencialidade;
        this.nomeLoteamento = nomeLoteamento;
        this.zona           = zona;
        this.setor          = setor;
        this.quadra         = quadra;
        this.lote           = lote;
        this.frente         = frente;
        this.fundos         = fundos;
        this.areaTotal      = areaTotal;
        this.observacoes    = observacoes;
    }

    public UnidadeTerrenoVO(Context context, Cursor cursor) {
        super(context, cursor);

        if(cursor.getInt(5) <= 0){
            this.unidadeVO = null;
        }else{
            this.unidadeVO = (UnidadeVO) new UnidadeMethods(context).get(cursor.getInt(5));
        }

        if(cursor.getInt(6) <= 0){
            this.potencialidade = null;
        }else{
            this.potencialidade = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(6));
        }

        this.nomeLoteamento = cursor.getString(7);
        this.zona           = cursor.getString(8);
        this.setor          = cursor.getString(9);
        this.quadra         = cursor.getString(10);
        this.lote           = cursor.getString(11);
        this.frente         = cursor.getDouble(12);
        this.fundos         = cursor.getDouble(13);
        this.fundos         = cursor.getDouble(14);
        this.observacoes    = cursor.getString(15);

    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        if(this.unidadeVO != null && this.unidadeVO.getId() > 0){
            values.put(DataBase.UNIDADE_TERRENO_UNIDADE, this.unidadeVO.getId());
        }else{
            values.put(DataBase.UNIDADE_TERRENO_UNIDADE, 0);
        }

        if(this.potencialidade != null && this.potencialidade.getId() > 0){
            values.put(DataBase.UNIDADE_TERRENO_POTENCIALIDADE, this.potencialidade.getId());
        }else{
            values.put(DataBase.UNIDADE_TERRENO_POTENCIALIDADE, 0);
        }


        /*
        TODO VALIDATORS DE INSERCAO COLOCAR NOS GETTERS E SETTERS E MUDAR CONSTRUTORES PARA USAR OS SETS
         */

        values.put(DataBase.UNIDADE_TERRENO_NOME_LOTEAMENTO, this.nomeLoteamento);
        values.put(DataBase.UNIDADE_TERRENO_ZONA           , this.zona);
        values.put(DataBase.UNIDADE_TERRENO_SETOR          , this.setor);
        values.put(DataBase.UNIDADE_TERRENO_QUADRA         , this.quadra);
        values.put(DataBase.UNIDADE_TERRENO_LOTE           , this.lote);
        values.put(DataBase.UNIDADE_TERRENO_FRENTE         , this.frente);
        values.put(DataBase.UNIDADE_TERRENO_FUNDOS         , this.fundos);
        values.put(DataBase.UNIDADE_TERRENO_AREA_TOTAL     , this.areaTotal);
        values.put(DataBase.UNIDADE_TERRENO_OBSERVACOES    , this.observacoes);

        return values;
    }

    @Override
    public void removeChildren(Context context) {
        /*
        TODO FALTA DAR UPDATE NOS CASOS ONDE ESTA ENTIDADE ESTA RELACIONADA

        CRIAR METODOS USANDO EXECSQL
         */
    }

    public UnidadeVO getUnidadeVO() {
        return unidadeVO;
    }

    public void setUnidadeVO(UnidadeVO unidadeVO) {
        this.unidadeVO = unidadeVO;
    }

    public DropDownItemVO getPotencialidade() {
        return potencialidade;
    }

    public void setPotencialidade(DropDownItemVO potencialidade) {
        this.potencialidade = potencialidade;
    }

    public String getNomeLoteamento() {
        return nomeLoteamento;
    }

    public void setNomeLoteamento(String nomeLoteamento) {
        this.nomeLoteamento = nomeLoteamento;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getQuadra() {
        return quadra;
    }

    public void setQuadra(String quadra) {
        this.quadra = quadra;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public double getFrente() {
        return frente;
    }

    public void setFrente(double frente) {
        this.frente = frente;
    }

    public double getFundos() {
        return fundos;
    }

    public void setFundos(double fundos) {
        this.fundos = fundos;
    }

    public double getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }
}
