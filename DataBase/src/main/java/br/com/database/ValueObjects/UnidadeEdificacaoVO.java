package br.com.database.ValueObjects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;

import java.util.Calendar;
import java.util.Date;

import br.com.database.DataBase;
import br.com.database.DateDeserializer;
import br.com.database.DateSerializer;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.TableMethods.UnidadeMethods;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeEdificacaoVO extends BasicValueObject{

    @JsonIgnore
    public static String DATAKEY = UnidadeEdificacaoVO.class.getSimpleName();

    @JsonProperty("unidadeVO")
    private UnidadeVO unidadeVO;
    @JsonProperty("tipoEstrutura")
    private DropDownItemVO tipoEstrutura;
    @JsonProperty("tipoCobertura")
    private DropDownItemVO tipoCobertura;
    @JsonProperty("tipoConservacao")
    private DropDownItemVO tipoConservacao;
    @JsonProperty("padraoEconomico")
    private DropDownItemVO padraoEconomico;
    @JsonProperty("tipo")
    private DropDownItemVO tipo;
    @JsonProperty("pavimentos")
    private int pavimentos;
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    @JsonProperty("dataConstrucao")
    private Date dataConstrucao;
    @JsonProperty("areaUtil" )
    private double areaUtil;
    @JsonProperty("areaComum")
    private double areaComum;
    @JsonProperty("areaTotal")
    private double areaTotal;
    @JsonProperty("observacoes")
    private String observacoes;

    public UnidadeEdificacaoVO(){
        super(0L, "");

        Calendar calendarAux = Calendar.getInstance();
        calendarAux.set(1902, Calendar.JANUARY, 1, 0, 0, 0);

        this.unidadeVO       = null;
        this.tipoEstrutura   = null;
        this.tipoCobertura   = null;
        this.tipoConservacao = null;
        this.padraoEconomico = null;
        this.tipo            = null;
        this.pavimentos      = 0;
        this.dataConstrucao  = calendarAux.getTime();
        this.areaUtil        = 0D;
        this.areaComum       = 0D;
        this.areaTotal       = 0D;
        this.observacoes     = "";
    }

    public UnidadeEdificacaoVO(long idServer, String token, UnidadeVO unidadeVO, DropDownItemVO tipoEstrutura, DropDownItemVO tipoCobertura, DropDownItemVO tipoConservacao, DropDownItemVO padraoEconomico, DropDownItemVO tipo, int pavimentos, Date dataConstrucao, int vagasGaragem, int dormitorios, int banheiros, int dormitoriosEmpregada, int salasEstar, int salasTv, int salasJantar, int lavabos, int lavanderias, int cozinhas, int dispensas, double areaUtil, double areaComum, double areaTotal, String observacoes) {
        super(idServer, token);
        this.unidadeVO       = unidadeVO;
        this.tipoEstrutura   = tipoEstrutura;
        this.tipoCobertura   = tipoCobertura;
        this.tipoConservacao = tipoConservacao;
        this.padraoEconomico = padraoEconomico;
        this.tipo            = tipo;
        this.pavimentos      = pavimentos;
        this.dataConstrucao  = dataConstrucao;
        this.areaUtil        = areaUtil;
        this.areaComum       = areaComum;
        this.areaTotal       = areaTotal;
        this.observacoes     = observacoes;
    }

    public UnidadeEdificacaoVO(Context context, Cursor cursor) {
        super(context, cursor);

        Calendar creationDateCalendar = Calendar.getInstance();
        creationDateCalendar.setTimeInMillis(cursor.getLong(12));

        if(cursor.getInt(5) <= 0){
            this.unidadeVO = null;
        }else{
            this.unidadeVO = (UnidadeVO) new UnidadeMethods(context).get(cursor.getInt(5));
        }

        if(cursor.getInt(6) <= 0){
            this.tipoEstrutura = null;
        }else{
            this.tipoEstrutura = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(6));
        }

        if(cursor.getInt(7) <= 0){
            this.tipoCobertura = null;
        }else{
            this.tipoCobertura = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(7));
        }

        if(cursor.getInt(8) <= 0){
            this.tipoConservacao = null;
        }else{
            this.tipoConservacao = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(8));
        }

        if(cursor.getInt(9) <= 0){
            this.padraoEconomico = null;
        }else{
            this.padraoEconomico = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(9));
        }

        if(cursor.getInt(10) <= 0){
            this.tipo = null;
        }else{
            this.tipo = (DropDownItemVO) new DropDownItensMethods(context).get(cursor.getInt(10));
        }

        this.pavimentos      = cursor.getInt(11);
        this.dataConstrucao  = creationDateCalendar.getTime();
        this.areaUtil        = cursor.getDouble(13);
        this.areaComum       = cursor.getDouble(14);
        this.areaTotal       = cursor.getDouble(15);
        this.observacoes     = cursor.getString(16);

    }

    @Override
    public ContentValues getContentValues(Context context) {

        ContentValues values = super.getContentValues(context);

        if(this.unidadeVO != null && this.unidadeVO.getId() > 0){
            values.put(DataBase.UNIDADE_EDIFICACAO_UNIDADE, this.unidadeVO.getId());
        }else{
            values.put(DataBase.UNIDADE_EDIFICACAO_UNIDADE, 0);
        }

        if(this.tipoEstrutura != null && this.tipoEstrutura.getId() > 0){
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO_ESTRUTURA, this.tipoEstrutura.getId());
        }else{
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO_ESTRUTURA, 0);
        }

        if(this.tipoCobertura != null && this.tipoCobertura.getId() > 0){
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO_COBERTURA, this.tipoCobertura.getId());
        }else{
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO_COBERTURA, 0);
        }

        if(this.tipoConservacao != null && this.tipoConservacao.getId() > 0){
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO_CONSERVACAO, this.tipoConservacao.getId());
        }else{
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO_CONSERVACAO, 0);
        }

        if(this.padraoEconomico != null && this.padraoEconomico.getId() > 0){
            values.put(DataBase.UNIDADE_EDIFICACAO_PADRAO_ECONOMICO, this.padraoEconomico.getId());
        }else{
            values.put(DataBase.UNIDADE_EDIFICACAO_PADRAO_ECONOMICO, 0);
        }

        if(this.tipo != null && this.tipo.getId() > 0){
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO, this.tipo.getId());
        }else{
            values.put(DataBase.UNIDADE_EDIFICACAO_TIPO, 0);
        }

        /*
        TODO VALIDATORS DE INSERCAO COLOCAR NOS GETTERS E SETTERS E MUDAR CONSTRUTORES PARA USAR OS SETS
         */

        values.put(DataBase.UNIDADE_EDIFICACAO_PAVIMENTOS      , this.pavimentos);
        values.put(DataBase.UNIDADE_EDIFICACAO_DATA_CONSTRUCAO , this.dataConstrucao.getTime());
        values.put(DataBase.UNIDADE_EDIFICACAO_AREA_UTIL       , this.areaUtil);
        values.put(DataBase.UNIDADE_EDIFICACAO_AREA_COMUM      , this.areaComum);
        values.put(DataBase.UNIDADE_EDIFICACAO_AREA_TOTAL      , this.areaTotal);
        values.put(DataBase.UNIDADE_EDIFICACAO_OBSERVACOES     , this.observacoes);

        return values;
    }

    @Override
    public void removeChildren(Context context) {
        /*
        TODO FALTA DAR UPDATE NOS CASOS ONDE ESTA ENTIDADE ESTA RELACIONADA
        CRIAR METODOS USANDO EXECSQL
         */
    }

    public UnidadeVO getUnidadeVO() {
        return unidadeVO;
    }

    public void setUnidadeVO(UnidadeVO unidadeVO) {
        this.unidadeVO = unidadeVO;
    }

    public DropDownItemVO getTipoEstrutura() {
        return tipoEstrutura;
    }

    public void setTipoEstrutura(DropDownItemVO tipoEstrutura) {
        this.tipoEstrutura = tipoEstrutura;
    }

    public DropDownItemVO getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(DropDownItemVO tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public DropDownItemVO getTipoConservacao() {
        return tipoConservacao;
    }

    public void setTipoConservacao(DropDownItemVO tipoConservacao) {
        this.tipoConservacao = tipoConservacao;
    }

    public DropDownItemVO getPadraoEconomico() {
        return padraoEconomico;
    }

    public void setPadraoEconomico(DropDownItemVO padraoEconomico) {
        this.padraoEconomico = padraoEconomico;
    }

    public int getPavimentos() {
        return pavimentos;
    }

    public void setPavimentos(int pavimentos) {
        this.pavimentos = pavimentos;
    }

    public Date getDataConstrucao() {
        return dataConstrucao;
    }

    public void setDataConstrucao(Date dataConstrucao) {
        this.dataConstrucao = dataConstrucao;
    }

    public double getAreaUtil() {
        return areaUtil;
    }

    public void setAreaUtil(double areaUtil) {
        this.areaUtil = areaUtil;
    }

    public double getAreaComum() {
        return areaComum;
    }

    public void setAreaComum(double areaComum) {
        this.areaComum = areaComum;
    }

    public double getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public DropDownItemVO getTipo() {
        return tipo;
    }

    public void setTipo(DropDownItemVO tipo) {
        this.tipo = tipo;
    }
}
