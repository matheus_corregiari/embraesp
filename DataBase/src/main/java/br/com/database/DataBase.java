package br.com.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Matheus on 12/08/2014.
 */
public class DataBase extends SQLiteOpenHelper {

    public final static String DATABASE_NAME = "DATABASE_APPLICATION";
    public final static int DATABASE_VERSION = 1;
    public Context mContext;

    // ATRIBUTOS GERAIS
    public final static String ID               = "Id";
    public final static String ID_SERVER        = "Id_Servidor";
    public final static String CREATION_DATE    = "Data_Criacao";
    public final static String LAST_UPDATE_DATE = "Data_Ultima_Atualizacao";
    public final static String TOKEN            = "Token";

    // TABELA CONTROLE DE UPDATE
    public final static String TABLE_UPDATE_CONTROL      = "Controle_Update";
    public final static String UPDATE_CONTROL_TABLE_NAME = "Nome_Tabela";
    public final static String[] TABLE_UPDATE_CONTROL_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            UPDATE_CONTROL_TABLE_NAME};

    // TABELA DROPDOWN_ITENS
    public final static String TABLE_DROPDOWN_ITENS = "DropDown_Itens";
    public final static String DROPDOWN_ITENS_CODE  = "Codigo";
    public final static String DROPDOWN_ITENS_VALUE = "Valor";
    public final static String DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM = "Parent_DropDown_Item";
    public final static String[] TABLE_DROPDOWN_ITENS_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            DROPDOWN_ITENS_CODE, DROPDOWN_ITENS_VALUE, DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM};

    // TABELA ENDERECO_IMOVEL
    public final static String TABLE_ENDERECO_IMOVEL        = "Endereco_Imovel";
    public final static String ENDERECO_TIPO_IMOVEL         = "Tipo_Imovel";
    public final static String ENDERECO_LOGRADOURO          = "Logradouro";
    public final static String ENDERECO_NUMERO              = "Numero";
    public final static String ENDERECO_CEP                 = "Cep";
    public final static String ENDERECO_BAIRRO              = "Bairro";
    public final static String ENDERECO_MUNICIPIO           = "Municipio";
    public final static String ENDERECO_UF                  = "UF";
    public final static String ENDERECO_LATITUDE            = "Latitude";
    public final static String ENDERECO_LONGITUDE           = "Longitude";
    public final static String ENDERECO_ENDERECO_APROXIMADO = "Endereco_Aproximado";
    public final static String[] TABLE_ENDERECO_IMOVEL_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            ENDERECO_TIPO_IMOVEL, ENDERECO_LOGRADOURO, ENDERECO_NUMERO, ENDERECO_CEP,
            ENDERECO_BAIRRO, ENDERECO_MUNICIPIO, ENDERECO_UF,
            ENDERECO_LATITUDE, ENDERECO_LONGITUDE, ENDERECO_ENDERECO_APROXIMADO};
    
    // TABELA UNIDADE
    public final static String TABLE_UNIDADE               = "Unidade";
    public final static String UNIDADE_SUB_TIPO_IMOVEL     = "Sub_Tipo_Imovel";
    public final static String UNIDADE_ENDERECO_IMOVEL     = "Endereco_Imovel";
    public final static String UNIDADE_DADO_ORIGEM         = "Dado_Origem";
    public final static String UNIDADE_CODIGO_EMBRAESP     = "Codigo_Embraesp";
    public final static String UNIDADE_INFORMACAO_AUDITADA = "Informacao_Auditada";
    public final static String UNIDADE_NOME_USUARIO        = "Nome_Usuario";
    public final static String[] TABLE_UNIDADE_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            UNIDADE_SUB_TIPO_IMOVEL, UNIDADE_ENDERECO_IMOVEL, UNIDADE_DADO_ORIGEM,
            UNIDADE_CODIGO_EMBRAESP, UNIDADE_INFORMACAO_AUDITADA,
            UNIDADE_NOME_USUARIO};

    // TABELA UNIDADE_TERRENO
    public final static String TABLE_UNIDADE_TERRENO           = "Unidade_Terreno";
    public final static String UNIDADE_TERRENO_UNIDADE         = "Unidade";
    public final static String UNIDADE_TERRENO_POTENCIALIDADE  = "Potencialidade";
    public final static String UNIDADE_TERRENO_NOME_LOTEAMENTO = "Nome_Loteamento";
    public final static String UNIDADE_TERRENO_ZONA            = "Zona";
    public final static String UNIDADE_TERRENO_SETOR           = "Setor";
    public final static String UNIDADE_TERRENO_QUADRA          = "Quadra";
    public final static String UNIDADE_TERRENO_LOTE            = "Lote";
    public final static String UNIDADE_TERRENO_FRENTE          = "Frente";
    public final static String UNIDADE_TERRENO_FUNDOS          = "Fundos";
    public final static String UNIDADE_TERRENO_AREA_TOTAL      = "Area_Total";
    public final static String UNIDADE_TERRENO_OBSERVACOES     = "Observacoes";
    public final static String[] TABLE_UNIDADE_TERRENO_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            UNIDADE_TERRENO_UNIDADE, UNIDADE_TERRENO_POTENCIALIDADE,
            UNIDADE_TERRENO_NOME_LOTEAMENTO, UNIDADE_TERRENO_ZONA,
            UNIDADE_TERRENO_SETOR, UNIDADE_TERRENO_QUADRA,
            UNIDADE_TERRENO_LOTE, UNIDADE_TERRENO_FRENTE, UNIDADE_TERRENO_FUNDOS, UNIDADE_TERRENO_AREA_TOTAL, UNIDADE_TERRENO_OBSERVACOES};

    // TABELA UNIDADE_EDIFICACAO
    public final static String TABLE_UNIDADE_EDIFICACAO            = "Unidade_Edificacao";
    public final static String UNIDADE_EDIFICACAO_UNIDADE          = "Unidade";
    public final static String UNIDADE_EDIFICACAO_TIPO_ESTRUTURA   = "Tipo_Estrutura";
    public final static String UNIDADE_EDIFICACAO_TIPO_COBERTURA   = "Tipo_Cobertura";
    public final static String UNIDADE_EDIFICACAO_TIPO_CONSERVACAO = "Tipo_Conservacao";
    public final static String UNIDADE_EDIFICACAO_PADRAO_ECONOMICO = "Padrao_Economico";
    public final static String UNIDADE_EDIFICACAO_TIPO             = "Tipo";
    public final static String UNIDADE_EDIFICACAO_PAVIMENTOS       = "Pavimentos";
    public final static String UNIDADE_EDIFICACAO_DATA_CONSTRUCAO  = "Data_Construcao";
    public final static String UNIDADE_EDIFICACAO_AREA_UTIL        = "Area_Util";
    public final static String UNIDADE_EDIFICACAO_AREA_COMUM       = "Area_Comum";
    public final static String UNIDADE_EDIFICACAO_AREA_TOTAL       = "Area_Total";
    public final static String UNIDADE_EDIFICACAO_OBSERVACOES      = "Observacoes";
    public final static String[] TABLE_UNIDADE_EDIFICACAO_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            UNIDADE_EDIFICACAO_UNIDADE, UNIDADE_EDIFICACAO_TIPO_ESTRUTURA,
            UNIDADE_EDIFICACAO_TIPO_COBERTURA, UNIDADE_EDIFICACAO_TIPO_CONSERVACAO,
            UNIDADE_EDIFICACAO_PADRAO_ECONOMICO, UNIDADE_EDIFICACAO_PAVIMENTOS, UNIDADE_EDIFICACAO_TIPO,
            UNIDADE_EDIFICACAO_DATA_CONSTRUCAO,
            UNIDADE_EDIFICACAO_AREA_UTIL, UNIDADE_EDIFICACAO_AREA_COMUM,
            UNIDADE_EDIFICACAO_AREA_TOTAL, UNIDADE_EDIFICACAO_OBSERVACOES};

    // TABELA UNIDADE_COMERCIALIZACAO
    public final static String TABLE_UNIDADE_COMERCIALIZACAO                    = "Unidade_Comercializacao";
    public final static String UNIDADE_COMERCIALIZACAO_UNIDADE                  = "Unidade";
    public final static String UNIDADE_COMERCIALIZACAO_TIPO_VENDA               = "Tipo_Venda";
    public final static String UNIDADE_COMERCIALIZACAO_TIPO_LOCACAO             = "Tipo_Locacao";
    public final static String UNIDADE_COMERCIALIZACAO_FONTE                    = "Tipo_Fonte";
    public final static String UNIDADE_COMERCIALIZACAO_DISPONIVEL_VENDA         = "Disponivel_Venda";
    public final static String UNIDADE_COMERCIALIZACAO_DISPONIVEL_LOCACAO       = "DisponivelLocacao";
    public final static String UNIDADE_COMERCIALIZACAO_VALOR_VENDA              = "Valor_Venda";
    public final static String UNIDADE_COMERCIALIZACAO_VALOR_LOCACAO            = "Valor_Locacao";
    public final static String UNIDADE_COMERCIALIZACAO_VALOR_CONDOMINIO_LOCACAO = "Valor_Condominio_Locacao";
    public final static String UNIDADE_COMERCIALIZACAO_VALOR_IPTU_LOCACAO       = "Valor_Iptu_Locacao";
    public final static String UNIDADE_COMERCIALIZACAO_CREATION_DATE_VENDA      = "Data_Criacao_Venda";
    public final static String UNIDADE_COMERCIALIZACAO_LAST_UPDATE_VENDA        = "Data_Ultima_Atualizacao_Venda";
    public final static String UNIDADE_COMERCIALIZACAO_CREATION_DATE_LOCACAO    = "Data_Criacao_Locacao";
    public final static String UNIDADE_COMERCIALIZACAO_LAST_UPDATE_LOCACAO      = "Data_Ultima_Atualizacao_Locacao";
    public final static String UNIDADE_COMERCIALIZACAO_NOME                     = "Nome";
    public final static String UNIDADE_COMERCIALIZACAO_TELEFONE                 = "Telefone";
    public final static String UNIDADE_COMERCIALIZACAO_EMAIL                    = "Email";
    public final static String UNIDADE_COMERCIALIZACAO_OBSERVACOES              = "Observacoes";
    public final static String[] TABLE_UNIDADE_COMERCIALIZACAO_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            UNIDADE_COMERCIALIZACAO_UNIDADE, UNIDADE_COMERCIALIZACAO_TIPO_VENDA,
            UNIDADE_COMERCIALIZACAO_TIPO_LOCACAO, UNIDADE_COMERCIALIZACAO_FONTE,
            UNIDADE_COMERCIALIZACAO_DISPONIVEL_VENDA, UNIDADE_COMERCIALIZACAO_DISPONIVEL_LOCACAO,
            UNIDADE_COMERCIALIZACAO_VALOR_VENDA, UNIDADE_COMERCIALIZACAO_VALOR_LOCACAO,
            UNIDADE_COMERCIALIZACAO_VALOR_CONDOMINIO_LOCACAO, UNIDADE_COMERCIALIZACAO_VALOR_IPTU_LOCACAO,
            UNIDADE_COMERCIALIZACAO_CREATION_DATE_VENDA, UNIDADE_COMERCIALIZACAO_LAST_UPDATE_VENDA,
            UNIDADE_COMERCIALIZACAO_CREATION_DATE_LOCACAO, UNIDADE_COMERCIALIZACAO_LAST_UPDATE_LOCACAO,
            UNIDADE_COMERCIALIZACAO_NOME, UNIDADE_COMERCIALIZACAO_TELEFONE, UNIDADE_COMERCIALIZACAO_EMAIL,
            UNIDADE_COMERCIALIZACAO_OBSERVACOES};

    // TABELA IMOVEL
    public final static String TABLE_IMOVEL                   = "Imovel";
    public final static String IMOVEL_ENDERECO_IMOVEL         = "Endereco_Imovel";
    public final static String IMOVEL_UNIDADE                 = "Unidade";
    public final static String IMOVEL_UNIDADE_COMERCIALIZACAO = "Unidade_Comercializacao";
    public final static String IMOVEL_UNIDADE_EDIFICACAO      = "Unidade_Edificacao";
    public final static String IMOVEL_UNIDADE_TERRENO         = "Unidade_Terreno";
    public final static String IMOVEL_TIPO_ATUALIZACAO        = "Tipo_Atualizacao";
    public final static String[] TABLE_IMOVEL_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            IMOVEL_ENDERECO_IMOVEL, IMOVEL_UNIDADE,
            IMOVEL_UNIDADE_COMERCIALIZACAO, IMOVEL_UNIDADE_EDIFICACAO,
            IMOVEL_UNIDADE_TERRENO, IMOVEL_TIPO_ATUALIZACAO};

    // TABELA IMAGENS
    public final static String TABLE_IMAGENS   = "Imagens";
    public final static String IMAGENS_PACKAGE = "Package";
    public final static String IMAGENS_URL     = "Url";
    public final static String IMAGENS_UNIDADE = "Unidade";
    public final static String IMAGENS_PATH    = "Path";
    public final static String[] TABLE_IMAGENS_COLUMNS = {
            ID, ID_SERVER, CREATION_DATE, LAST_UPDATE_DATE, TOKEN,
            IMAGENS_PACKAGE, IMAGENS_URL, IMAGENS_UNIDADE, IMAGENS_PATH};

    public DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //ATRIBUTOS GERAIS
        String BASE_COLUMNS = ""
                + ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ID_SERVER        + " INTEGER NOT NULL, "
                + CREATION_DATE    + " DATE NOT NULL, "
                + LAST_UPDATE_DATE + " DATE NOT NULL, "
                + TOKEN            + " TEXT NOT NULL, ";

        //TABELA CONTROLE DE UPDATE
        String CREATE_UPDATE_CONTROL = "CREATE TABLE " + TABLE_UPDATE_CONTROL + " ( "
                + BASE_COLUMNS
                + UPDATE_CONTROL_TABLE_NAME + " TEXT NOT NULL "
                + " )";

        //TABELA DROPDOWN_ITENS
        String CREATE_DROPDOWN_ITENS = "CREATE TABLE " + TABLE_DROPDOWN_ITENS + " ( "
                + BASE_COLUMNS
                + DROPDOWN_ITENS_CODE           + " TEXT NOT NULL, "
                + DROPDOWN_ITENS_VALUE          + " TEXT NOT NULL, "
                + DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM + " INTEGER, "
                + " FOREIGN KEY ( " + DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " )"
                + " )";

        //TABELA ENDERECO_IMOVEL
        String CREATE_ENDERECO_IMOVEL = "CREATE TABLE " + TABLE_ENDERECO_IMOVEL + " ( "
                + BASE_COLUMNS
                + ENDERECO_TIPO_IMOVEL         + " INTEGER, "
                + ENDERECO_LOGRADOURO          + " TEXT, "
                + ENDERECO_NUMERO              + " TEXT, "
                + ENDERECO_CEP                 + " TEXT, "
                + ENDERECO_BAIRRO              + " TEXT, "
                + ENDERECO_MUNICIPIO           + " TEXT, "
                + ENDERECO_UF                  + " TEXT, "
                + ENDERECO_LATITUDE            + " NUMERIC, "
                + ENDERECO_LONGITUDE           + " NUMERIC, "
                + ENDERECO_ENDERECO_APROXIMADO + " INTEGER, "
                + " FOREIGN KEY ( " + ENDERECO_TIPO_IMOVEL + " ) REFERENCES " + TABLE_DROPDOWN_ITENS  + " ( " + ID + " )"
                + " )";

        //TABELA UNIDADE
        String CREATE_UNIDADE = "CREATE TABLE " + TABLE_UNIDADE + " ( "
                + BASE_COLUMNS
                + UNIDADE_SUB_TIPO_IMOVEL     + " INTEGER, "
                + UNIDADE_ENDERECO_IMOVEL     + " INTEGER, "
                + UNIDADE_DADO_ORIGEM         + " INTEGER, "
                + UNIDADE_CODIGO_EMBRAESP     + " TEXT, "
                + UNIDADE_INFORMACAO_AUDITADA + " TEXT, "
                + UNIDADE_NOME_USUARIO        + " TEXT, "
                + " FOREIGN KEY ( " + UNIDADE_SUB_TIPO_IMOVEL + " ) REFERENCES " + TABLE_DROPDOWN_ITENS  + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_ENDERECO_IMOVEL + " ) REFERENCES " + TABLE_ENDERECO_IMOVEL + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_DADO_ORIGEM     + " ) REFERENCES " + TABLE_DROPDOWN_ITENS  + " ( " + ID + " ) "
                + " )";

        //TABELA UNIDADE_TERRENO
        String CREATE_UNIDADE_TERRENO = "CREATE TABLE " + TABLE_UNIDADE_TERRENO + " ( "
                + BASE_COLUMNS
                + UNIDADE_TERRENO_UNIDADE                + " INTEGER, "
                + UNIDADE_TERRENO_POTENCIALIDADE         + " INTEGER, "
                + UNIDADE_TERRENO_NOME_LOTEAMENTO        + " TEXT, "
                + UNIDADE_TERRENO_ZONA                   + " TEXT, "
                + UNIDADE_TERRENO_SETOR                  + " TEXT, "
                + UNIDADE_TERRENO_QUADRA                 + " TEXT, "
                + UNIDADE_TERRENO_LOTE                   + " TEXT, "
                + UNIDADE_TERRENO_FRENTE                 + " NUMERIC, "
                + UNIDADE_TERRENO_FUNDOS                 + " NUMERIC, "
                + UNIDADE_TERRENO_AREA_TOTAL             + " NUMERIC, "
                + UNIDADE_TERRENO_OBSERVACOES            + " TEXT, "
                + " FOREIGN KEY ( " + UNIDADE_TERRENO_UNIDADE        + " ) REFERENCES " + TABLE_UNIDADE        + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_TERRENO_POTENCIALIDADE + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " )"
                + " )";

        //TABELA UNIDADE_EDIFICACAO
        String CREATE_UNIDADE_EDIFICACAO = "CREATE TABLE " + TABLE_UNIDADE_EDIFICACAO + " ( "
                + BASE_COLUMNS
                + UNIDADE_EDIFICACAO_UNIDADE          + " INTEGER, "
                + UNIDADE_EDIFICACAO_TIPO_ESTRUTURA   + " INTEGER, "
                + UNIDADE_EDIFICACAO_TIPO_COBERTURA   + " INTEGER, "
                + UNIDADE_EDIFICACAO_TIPO_CONSERVACAO + " INTEGER, "
                + UNIDADE_EDIFICACAO_PADRAO_ECONOMICO + " INTEGER, "
                + UNIDADE_EDIFICACAO_TIPO             + " INTEGER, "
                + UNIDADE_EDIFICACAO_PAVIMENTOS       + " INTEGER, "
                + UNIDADE_EDIFICACAO_DATA_CONSTRUCAO  + " DATE, "
                + UNIDADE_EDIFICACAO_AREA_UTIL        + " NUMERIC, "
                + UNIDADE_EDIFICACAO_AREA_COMUM       + " NUMERIC, "
                + UNIDADE_EDIFICACAO_AREA_TOTAL       + " NUMERIC, "
                + UNIDADE_EDIFICACAO_OBSERVACOES      + " TEXT, "
                + " FOREIGN KEY ( " + UNIDADE_EDIFICACAO_UNIDADE          + " ) REFERENCES " + TABLE_UNIDADE        + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_EDIFICACAO_TIPO_ESTRUTURA   + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_EDIFICACAO_TIPO_COBERTURA   + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_EDIFICACAO_TIPO_CONSERVACAO + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_EDIFICACAO_PADRAO_ECONOMICO + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " ),"
                + " FOREIGN KEY ( " + UNIDADE_EDIFICACAO_TIPO             + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " )"
                + " )";

        //TABELA UNIDADE_COMERCIALIZACAO
        String CREATE_UNIDADE_COMERCIALIZACAO = "CREATE TABLE " + TABLE_UNIDADE_COMERCIALIZACAO + " ( "
                + BASE_COLUMNS
                + UNIDADE_COMERCIALIZACAO_UNIDADE                  + " INTEGER, "
                + UNIDADE_COMERCIALIZACAO_TIPO_VENDA               + " INTEGER, "
                + UNIDADE_COMERCIALIZACAO_TIPO_LOCACAO             + " INTEGER, "
                + UNIDADE_COMERCIALIZACAO_FONTE                    + " INTEGER, "
                + UNIDADE_COMERCIALIZACAO_DISPONIVEL_VENDA         + " INTEGER, "
                + UNIDADE_COMERCIALIZACAO_DISPONIVEL_LOCACAO       + " INTEGER, "
                + UNIDADE_COMERCIALIZACAO_VALOR_VENDA              + " NUMERIC, "
                + UNIDADE_COMERCIALIZACAO_VALOR_LOCACAO            + " NUMERIC, "
                + UNIDADE_COMERCIALIZACAO_VALOR_CONDOMINIO_LOCACAO + " NUMERIC, "
                + UNIDADE_COMERCIALIZACAO_VALOR_IPTU_LOCACAO       + " NUMERIC, "
                + UNIDADE_COMERCIALIZACAO_CREATION_DATE_VENDA      + " DATE, "
                + UNIDADE_COMERCIALIZACAO_LAST_UPDATE_VENDA        + " DATE, "
                + UNIDADE_COMERCIALIZACAO_CREATION_DATE_LOCACAO    + " DATE, "
                + UNIDADE_COMERCIALIZACAO_LAST_UPDATE_LOCACAO      + " DATE, "
                + UNIDADE_COMERCIALIZACAO_NOME                     + " TEXT, "
                + UNIDADE_COMERCIALIZACAO_TELEFONE                 + " TEXT, "
                + UNIDADE_COMERCIALIZACAO_EMAIL                    + " TEXT, "
                + UNIDADE_COMERCIALIZACAO_OBSERVACOES              + " TEXT, "
                + " FOREIGN KEY ( " + UNIDADE_COMERCIALIZACAO_UNIDADE      + " ) REFERENCES " + TABLE_UNIDADE        + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_COMERCIALIZACAO_TIPO_VENDA   + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_COMERCIALIZACAO_TIPO_LOCACAO + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + UNIDADE_COMERCIALIZACAO_FONTE        + " ) REFERENCES " + TABLE_DROPDOWN_ITENS + " ( " + ID + " )"
                + " )";
        
        //TABELA UNIDADE_COMERCIALIZACAO
        String CREATE_IMOVEL = "CREATE TABLE " + TABLE_IMOVEL + " ( "
                + BASE_COLUMNS
                + IMOVEL_ENDERECO_IMOVEL         + " INTEGER, "
                + IMOVEL_UNIDADE                 + " INTEGER, "
                + IMOVEL_UNIDADE_COMERCIALIZACAO + " INTEGER, "
                + IMOVEL_UNIDADE_EDIFICACAO      + " INTEGER, "
                + IMOVEL_UNIDADE_TERRENO         + " INTEGER, "
                + IMOVEL_TIPO_ATUALIZACAO        + " INTEGER, "
                + " FOREIGN KEY ( " + IMOVEL_ENDERECO_IMOVEL         + " ) REFERENCES " + TABLE_ENDERECO_IMOVEL         + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + IMOVEL_UNIDADE                 + " ) REFERENCES " + TABLE_UNIDADE                 + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + IMOVEL_UNIDADE_COMERCIALIZACAO + " ) REFERENCES " + TABLE_UNIDADE_COMERCIALIZACAO + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + IMOVEL_UNIDADE_EDIFICACAO             + " ) REFERENCES " + TABLE_UNIDADE_EDIFICACAO      + " ( " + ID + " ), "
                + " FOREIGN KEY ( " + IMOVEL_UNIDADE_TERRENO         + " ) REFERENCES " + TABLE_UNIDADE_TERRENO         + " ( " + ID + " )"
                + " )";

        //TABELA IMAGENS
        String CREATE_IMAGENS = "CREATE TABLE " + TABLE_IMAGENS + " ( "
                + BASE_COLUMNS
                + IMAGENS_PACKAGE + " TEXT, "
                + IMAGENS_UNIDADE + " INTEGER, "
                + IMAGENS_URL     + " TEXT, "
                + IMAGENS_PATH    + " TEXT, "
                + " FOREIGN KEY ( " + IMAGENS_UNIDADE + " ) REFERENCES " + TABLE_UNIDADE + " ( " + ID + " )"
                + " )";

        db.execSQL(CREATE_UPDATE_CONTROL         );
        db.execSQL(CREATE_DROPDOWN_ITENS         );
        db.execSQL(CREATE_ENDERECO_IMOVEL        );
        db.execSQL(CREATE_UNIDADE                );
        db.execSQL(CREATE_UNIDADE_TERRENO        );
        db.execSQL(CREATE_UNIDADE_EDIFICACAO     );
        db.execSQL(CREATE_UNIDADE_COMERCIALIZACAO);
        db.execSQL(CREATE_IMOVEL                 );
        db.execSQL(CREATE_IMAGENS                );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGENS                );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMOVEL                 );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UNIDADE_COMERCIALIZACAO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UNIDADE_EDIFICACAO     );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UNIDADE_TERRENO        );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UNIDADE                );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENDERECO_IMOVEL        );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DROPDOWN_ITENS         );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UPDATE_CONTROL         );

        this.onCreate(db);

    }

}
