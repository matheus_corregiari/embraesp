package br.com.database;

import android.text.format.DateFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Matheus on 28/10/2014.
 */
public class DateSerializer extends JsonSerializer<Date>{

    @Override
    public void serialize(Date value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        if(value != null) {
            String string = "/Date(";
            string += value.getTime() + ")/";
            jgen.writeString(DateFormat.format("dd/MM/yyyy hh:mm:ss", value).toString());
        }
    }
}
