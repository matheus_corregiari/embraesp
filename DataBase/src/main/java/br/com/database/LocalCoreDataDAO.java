package br.com.database;

import android.app.Activity;
import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class LocalCoreDataDAO {

	private static String TAG = "LocalCoreDataDAO";
	
	public interface LocalDataDelegate {
		public void onResult(Object data);
	}
	
	/**
	 * 
	 * @param context
	 * @param object
	 * @param filename
	 */
	public static void save( Context context, Object object, String filename ) {
		
		ObjectOutputStream objectOut = null;
		try {

			FileOutputStream fileOut = context.openFileOutput( filename, Activity.MODE_PRIVATE );
			objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(object);
			fileOut.getFD().sync();

		} catch (IOException e) {
///////////////////////////////////////////////////
		} finally {
			if (objectOut != null) {
				try {
					objectOut.close();
				} catch (IOException e) {
					///////////////////////////////////////////////////
				}
			}
		}
	}
	
	public static void asyncSave( final Context context, final Object object, final String filename, final LocalDataDelegate delegate ) {
		
		if ( delegate == null ){
			return;
		}
		
		Thread asyncSaveThread = new Thread( new Runnable() {
			
			@Override
			public void run() {
				save(context, object, filename);
				delegate.onResult(object);
			}
		}, TAG + ".asyncSaveThread" );
		
		asyncSaveThread.start();
	}

	/**
	 * 
	 * @param context
	 * @param filename
	 * @return
	 */
	public static Object get(Context context, String filename) {

		ObjectInputStream objectIn = null;
		Object object = null;
		
		if ( filename == null || context == null ){
			return null;
		}
		
		try {

			FileInputStream fileIn = context.getApplicationContext().openFileInput(filename);
			objectIn = new ObjectInputStream(fileIn);
			object = objectIn.readObject();

		} catch (Exception e) {
///////////////////////////////////////////////////
		} finally {
			if (objectIn != null) {
				try {
					objectIn.close();
				} catch (IOException e) {
///////////////////////////////////////////////////
				}
			}
		}

		return object;
	}
	
	public static void asyncGet( final Context context, final String filename, final LocalDataDelegate delegate ) {
		
		if ( delegate == null ){
			return;
		}
		
		Thread asyncSaveThread = new Thread( new Runnable() {
			
			@Override
			public void run() {
				Object object = get(context, filename);				
				delegate.onResult(object);
			}
		}, TAG + ".asyncGet" );
		
		asyncSaveThread.start();
	}

}
