package br.com.database.Manager;

import android.content.Context;

import br.com.database.AsyncTask.DataBaseAsyncTask;
import br.com.database.Enum.MethodTag;
import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.GenericMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class DataBaseManager implements DataBaseResultDelegate{

    private GenericMethods genericMethods;
    private Context mContext;
    private DataBaseResultDelegate dataBaseResultDelegate;
    private DataBaseAsyncTask dataBaseAsyncTask;

    public DataBaseManager(Context mContext, DataBaseResultDelegate dataBaseResultDelegate, GenericMethods genericMethods) {

        if(mContext == null){
            throw new IllegalArgumentException("CONTEXT IS REQUIRED");
        }

        if(genericMethods == null){
            throw new IllegalArgumentException("GENERIC METHODS IS REQUIRED");
        }

        this.mContext = mContext;
        this.dataBaseResultDelegate = dataBaseResultDelegate;
        this.genericMethods = genericMethods;
    }

    public void execOnDataBase(MethodTag methodTag, Object... parameters){

        if (methodTag == null) {
            throw new IllegalStateException("<<METHOD_TAG_CANNOT_BE_NULL>>");
        }

        Object[] objects;
        if(parameters != null && parameters.length > 0) {
            objects = new Object[parameters.length + 1];
            objects[0] = methodTag;
            for (int i = 0; i < parameters.length; i++) {
                objects[i+1] = parameters[i];
            }
        }else{
            objects = new Object[1];
            objects[0] = methodTag;
        }

        try {
            this.dataBaseAsyncTask = new DataBaseAsyncTask(mContext, dataBaseResultDelegate, genericMethods);
            this.dataBaseAsyncTask.execute(objects);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        if(this.dataBaseResultDelegate != null){
            this.dataBaseResultDelegate.onDataBaseResult(method, object);
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        if(this.dataBaseResultDelegate != null){
            this.dataBaseResultDelegate.onDataBaseError(method, exception);
        }
    }
}
