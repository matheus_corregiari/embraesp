package br.com.database.Management;

import android.content.Context;

import br.com.database.Enum.MethodTag;
import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.Manager.DataBaseManager;
import br.com.database.TableMethods.GenericMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public abstract class DataBaseManagement implements DataBaseResultDelegate {

    private Context                mContext;
    private DataBaseManager        dataBaseManager;
    private DataBaseResultDelegate dataBaseResultDelegate;
    private GenericMethods         genericMethods;

    protected DataBaseManagement(Context mContext, GenericMethods genericMethods, DataBaseResultDelegate dataBaseResultDelegate) {

        if(mContext == null){
            throw new IllegalArgumentException("CONTEXT IS REQUIRED");
        }

        if(genericMethods == null){
            throw new IllegalArgumentException("GENERIC METHODS IS REQUIRED");
        }

        this.mContext               = mContext;
        this.genericMethods         = genericMethods;
        this.dataBaseResultDelegate = dataBaseResultDelegate;
        this.dataBaseManager = new DataBaseManager(mContext, dataBaseResultDelegate, genericMethods);
    }

    public void sendToDataBase(MethodTag methodTag, Object... parameters){

        if (methodTag == null) {
            throw new IllegalStateException("<<METHOD_TAG_CANNOT_BE_NULL>>");
        }

        if (this.dataBaseManager == null) {
            throw new IllegalStateException("<<DATA_BASE_MANAGER_NULL>>");
        }

        this.dataBaseManager.execOnDataBase(methodTag, parameters);
    }

    public String getClassKey(){
        return this.genericMethods.getClassKey();
    }

    @Override
    public void onDataBaseResult(String method, Object object) {
        if(this.dataBaseResultDelegate != null){
            this.dataBaseResultDelegate.onDataBaseResult(method, object);
        }
    }

    @Override
    public void onDataBaseError(String method, Exception exception) {
        if(this.dataBaseResultDelegate != null){
            this.dataBaseResultDelegate.onDataBaseError(method, exception);
        }
    }

}
