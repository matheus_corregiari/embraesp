package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.UnidadeComercializacaoMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class UnidadeComercializacaoManagement extends DataBaseManagement {
    public UnidadeComercializacaoManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new UnidadeComercializacaoMethods(mContext), dataBaseResultDelegate);
    }
}
