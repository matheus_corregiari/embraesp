package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.TableMethods.GenericMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class DropDownItemManagement extends DataBaseManagement {
    public DropDownItemManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new DropDownItensMethods(mContext), dataBaseResultDelegate);
    }
}
