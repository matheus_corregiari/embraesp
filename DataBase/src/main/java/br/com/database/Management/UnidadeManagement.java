package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.EnderecoImovelMethods;
import br.com.database.TableMethods.UnidadeMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class UnidadeManagement extends DataBaseManagement {
    public UnidadeManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new UnidadeMethods(mContext), dataBaseResultDelegate);
    }
}
