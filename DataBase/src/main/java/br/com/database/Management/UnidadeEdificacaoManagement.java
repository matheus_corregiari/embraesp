package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.UnidadeEdificacaoMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class UnidadeEdificacaoManagement extends DataBaseManagement {
    public UnidadeEdificacaoManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new UnidadeEdificacaoMethods(mContext), dataBaseResultDelegate);
    }
}
