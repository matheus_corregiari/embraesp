package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.ImagemMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class ImagemManagement extends DataBaseManagement {
    public ImagemManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new ImagemMethods(mContext), dataBaseResultDelegate);
    }
}
