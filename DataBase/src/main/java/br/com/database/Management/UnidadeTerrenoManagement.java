package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.UnidadeTerrenoMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class UnidadeTerrenoManagement extends DataBaseManagement {
    public UnidadeTerrenoManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new UnidadeTerrenoMethods(mContext), dataBaseResultDelegate);
    }
}
