package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.EnderecoImovelMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class EnderecoImovelManagement extends DataBaseManagement {
    public EnderecoImovelManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new EnderecoImovelMethods(mContext), dataBaseResultDelegate);
    }
}
