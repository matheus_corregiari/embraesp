package br.com.database.Management;

import android.content.Context;

import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.TableMethods.DropDownItensMethods;
import br.com.database.TableMethods.ImovelMethods;

/**
 * Created by Matheus on 19/08/2014.
 */
public class ImovelManagement extends DataBaseManagement {
    public ImovelManagement(Context mContext, DataBaseResultDelegate dataBaseResultDelegate) {
        super(mContext, new ImovelMethods(mContext), dataBaseResultDelegate);
    }
}
