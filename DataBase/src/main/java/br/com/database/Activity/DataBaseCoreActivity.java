package br.com.database.Activity;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import br.com.database.Enum.MethodTag;
import br.com.database.Interfaces.DataBaseResultDelegate;
import br.com.database.Management.DataBaseManagement;
import br.com.database.R;
import br.com.genericdata.coredata.activity.CoreActivity;

/**
 * Created by Matheus on 19/08/2014.
 */
public abstract class DataBaseCoreActivity extends CoreActivity implements DataBaseResultDelegate {

    private DataBaseManagement dataBaseManagement;

    public DataBaseManagement getDataBaseManagement() {
        return dataBaseManagement;
    }

    public void setDataBaseManagement(DataBaseManagement dataBaseManagement) {
        this.dataBaseManagement = dataBaseManagement;
    }

    public void sendToDataBase(MethodTag methodTag, Object... parameters){
        if(this.dataBaseManagement != null){
            this.dataBaseManagement.sendToDataBase(methodTag, parameters);
        }
    }

    public String getClassKey(){
        if(this.dataBaseManagement != null){
           return this.dataBaseManagement.getClassKey();
        }
        return "";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //final ActionBar actionBar = getActionBar();
        //BitmapDrawable background = new BitmapDrawable (BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher2));
        //background.setTileModeX(android.graphics.Shader.TileMode.REPEAT);
        //actionBar.setBackgroundDrawable(background);
    }

    public static String getMethodName(String classKey, MethodTag methodTag){

        if(classKey == null && methodTag != null){
            return methodTag.getName();
        }

        if(classKey != null && methodTag == null){
            return classKey;
        }

        if(classKey != null && methodTag != null){
            return classKey + "." + methodTag.getName();
        }

        return "";
    }
}
