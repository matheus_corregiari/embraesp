package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.ImovelVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class ImovelMethods extends GenericMethods {

    public ImovelMethods(Context context) {
        super(context, TABLE_IMOVEL, TABLE_IMOVEL_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new ImovelVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return this.getClass().getSimpleName();
    }

}
