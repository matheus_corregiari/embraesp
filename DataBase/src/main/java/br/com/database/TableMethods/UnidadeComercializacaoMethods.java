package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.UnidadeComercializacaoVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeComercializacaoMethods extends GenericMethods {

    public UnidadeComercializacaoMethods(Context context) {
        super(context, TABLE_UNIDADE_COMERCIALIZACAO, TABLE_UNIDADE_COMERCIALIZACAO_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new UnidadeComercializacaoVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return this.getClass().getSimpleName();
    }

}
