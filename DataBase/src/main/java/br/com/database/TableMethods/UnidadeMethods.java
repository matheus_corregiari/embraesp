package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.EnderecoImovelVO;
import br.com.database.ValueObjects.UnidadeVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeMethods extends GenericMethods {

    public UnidadeMethods(Context context) {
        super(context, TABLE_UNIDADE, TABLE_UNIDADE_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new UnidadeVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return this.getClass().getSimpleName();
    }

}
