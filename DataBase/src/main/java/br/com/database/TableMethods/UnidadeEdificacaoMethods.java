package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.UnidadeEdificacaoVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeEdificacaoMethods extends GenericMethods {

    public UnidadeEdificacaoMethods(Context context) {
        super(context, TABLE_UNIDADE_EDIFICACAO, TABLE_UNIDADE_EDIFICACAO_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new UnidadeEdificacaoVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return this.getClass().getSimpleName();
    }

}
