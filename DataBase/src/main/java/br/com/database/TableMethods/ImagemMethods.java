package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.ImagemVO;
import br.com.database.ValueObjects.ImovelVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class ImagemMethods extends GenericMethods {

    public ImagemMethods(Context context) {
        super(context, TABLE_IMAGENS, TABLE_IMAGENS_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new ImagemVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return ((Object)this).getClass().getSimpleName();
    }

    public List<ImagemVO> getImagensByUnidadeId(long unidadeId){

        if(unidadeId <= 0L){
            throw new IllegalArgumentException("<<UNIDADEID_MUST_BE_>0>>");
        }

        String query = "SELECT * FROM " + this.getTableName()
                + " WHERE " + IMAGENS_UNIDADE + " = " + unidadeId;

        List<BasicValueObject> basicValueObjects = this.execSQL(query);

        if(basicValueObjects == null){
            return new ArrayList<ImagemVO>();
        }

        ArrayList<ImagemVO> imagemVOs = new ArrayList<ImagemVO>();

        for(BasicValueObject basicValueObject : basicValueObjects){
            imagemVOs.add((ImagemVO) basicValueObject);
        }

        return imagemVOs;
    }
}
