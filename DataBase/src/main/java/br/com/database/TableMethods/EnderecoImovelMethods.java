package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.EnderecoImovelVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class EnderecoImovelMethods extends GenericMethods {

    public EnderecoImovelMethods(Context context) {
        super(context, TABLE_ENDERECO_IMOVEL, TABLE_ENDERECO_IMOVEL_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new EnderecoImovelVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return this.getClass().getSimpleName();
    }

}
