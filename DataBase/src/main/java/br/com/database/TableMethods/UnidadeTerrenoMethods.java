package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.UnidadeTerrenoVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class UnidadeTerrenoMethods extends GenericMethods {

    public UnidadeTerrenoMethods(Context context) {
        super(context, TABLE_UNIDADE_TERRENO, TABLE_UNIDADE_TERRENO_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new UnidadeTerrenoVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return this.getClass().getSimpleName();
    }

}
