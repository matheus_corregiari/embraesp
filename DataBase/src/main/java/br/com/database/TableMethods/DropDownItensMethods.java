package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;

import java.util.List;

import br.com.database.ValueObjects.BasicValueObject;
import br.com.database.ValueObjects.DropDownItemVO;

/**
 * Created by Matheus on 12/08/2014.
 */
public class DropDownItensMethods extends GenericMethods {

    public DropDownItensMethods(Context context) {
        super(context, TABLE_DROPDOWN_ITENS, TABLE_DROPDOWN_ITENS_COLUMNS);
    }

    @Override
    public BasicValueObject initializeVO(Cursor cursor) {
        return new DropDownItemVO(this.mContext, cursor);
    }

    @Override
    public String getClassKey() {
        return this.getClass().getSimpleName();
    }

    public List<BasicValueObject> getDropDownItensByKey(String key){

        if(key == null || key.equals("")){
            throw new IllegalArgumentException("<<KEY_CANNOT_BE_NULL_OR_EMPTY>>");
        }

        String query = "SELECT * FROM " + this.getTableName()
                + " WHERE " + DROPDOWN_ITENS_CODE + " IS NOT NULL "
                + " AND "   + DROPDOWN_ITENS_CODE + " = '" + key + "'";

        return this.execSQL(query);
    }

    public List<BasicValueObject> getDropDownItensByParent(long parentId){

        if(parentId <= 0L){
            throw new IllegalArgumentException("<<PARENTID_MUST_BE_>0>>");
        }

        String query = "SELECT * FROM " + this.getTableName()
                + " WHERE " + DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM + " IS NOT NULL "
                + " AND "   + DROPDOWN_ITENS_PARENT_DROPDOWN_ITEM + " = " + parentId;

        return this.execSQL(query);
    }
}
