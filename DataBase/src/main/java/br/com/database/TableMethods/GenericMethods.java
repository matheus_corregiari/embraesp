package br.com.database.TableMethods;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.database.DataBase;
import br.com.database.Interfaces.DataBaseMethods;
import br.com.database.ValueObjects.BasicValueObject;

/**
 * Created by Matheus on 12/08/2014.
 */
public abstract class GenericMethods extends DataBase implements DataBaseMethods {

    private String tableName;
    private String[] tableColumns;

    public GenericMethods(Context context, String tableName, String[] tableColumns) {
        super(context);

        if(tableName == null || tableName.equals("")){
            throw new IllegalArgumentException("TABLE NAME IS REQUIRED");
        }

        if(tableColumns == null || tableColumns.length == 0){
            throw new IllegalArgumentException("TABLE COLUMNS IS REQUIRED");
        }

        this.tableName = tableName;
        this.tableColumns = tableColumns;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String[] getTableColumns() {
        return tableColumns;
    }

    public void setTableColumns(String[] tableColumns) {
        this.tableColumns = tableColumns;
    }

    public abstract BasicValueObject initializeVO(Cursor cursor);

    public abstract String getClassKey();

    /*
     * Basic Methods
     */
    @Override
    public BasicValueObject get(long id) {

        if(id <= 0L){
            throw new IllegalArgumentException( "<<GET_CANNOT_GET_INVALID_ID>>" );
        }

        String[] filters = new String[1];
        filters[0] = ""+id;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(this.getTableName(), this.getTableColumns(), ID + " = ?", filters, null, null, null, null);

        if (cursor == null) {
            throw new IllegalArgumentException( "<<GET_ID_NOT_FOUND>>" );
        }

        BasicValueObject basicValueObject;
        cursor.moveToFirst();

        try {
            basicValueObject = this.initializeVO(cursor);
        } catch (Exception e) {
            cursor.close();
            db.close();
            this.close();
            throw new UnsupportedOperationException(e.getMessage());
        }

        cursor.close();
        db.close();
        this.close();
        return basicValueObject;
    }

    @Override
    public List<BasicValueObject> getAll() {
        String query = "SELECT  * FROM " + this.getTableName();
        return this.execSQL(query);
    }

    @Override
    public BasicValueObject save(BasicValueObject objectToSave) {

        if(objectToSave.getIdServer() < 0){
            throw new IllegalArgumentException( "<<SAVE_ID_INVALIDO>>" );
        }

        if(objectToSave.getId() > 0){
            return this.update(objectToSave);
        }

        SQLiteDatabase db = this.getReadableDatabase();

        long id = db.insert(this.getTableName(), null, objectToSave.getContentValues(mContext));

        db.close();
        this.close();

        if(id <= 0){
            throw new IllegalArgumentException( "<<SAVE_NOT_INSERTED>>" );
        }

        objectToSave.setId(id);

        return objectToSave;
    }

    @Override
    public BasicValueObject update(BasicValueObject objectToUpdate) {

        if (objectToUpdate.getId() <= 0 ) {
            throw new IllegalArgumentException( "<<UPDATE_CANNOT_UPDATE_INVALID_ID>>" );
        }

        objectToUpdate.setLastUpdateDate(Calendar.getInstance().getTime());
        objectToUpdate.setToken("");

        SQLiteDatabase db = this.getWritableDatabase();

        Integer rows_updated = db.update(this.getTableName(), objectToUpdate.getContentValues(mContext), ID + " = ?", new String[] {"" + objectToUpdate.getId()});

        db.close();
        this.close();

        if ( rows_updated < 0 ) {
            throw new IllegalArgumentException( "<<UPDATE_ERROR>>" );
        }

        return objectToUpdate;
    }

    @Override
    public BasicValueObject delete(long id) {

        if(id <= 0){
            throw new IllegalArgumentException( "<<DELETE_ID_INVALIDO>>" );
        }

        BasicValueObject basicValueObject = this.get(id);
        Integer deleted = 0;
        if(basicValueObject != null){
            basicValueObject.removeChildren(this.mContext);
            SQLiteDatabase db = this.getWritableDatabase();
            deleted = db.delete(this.getTableName(), ID + " = ?",
                    new String[] { ""+id });
            db.close();
            this.close();
        }

        if ( deleted <= 0 ) {
            throw new IllegalArgumentException( "<<DELETE_ERROR_OR_VALUE_NOT_EXIST>>" );
        }

        return basicValueObject;
    }

    @Override
    public List<BasicValueObject> execSQL(String query) {

        if(query == null || query.equals("")){
            throw new IllegalArgumentException("<<QUERY_CANNOT_BE_NULL_OR_EMPTY>>");
        }

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        List<BasicValueObject> basicValueObjects = new ArrayList<BasicValueObject>();

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                try {
                    basicValueObjects.add(this.initializeVO(cursor));
                } catch (Exception e) {
                    db.close();
                    this.close();
                    cursor.close();
                    throw new UnsupportedOperationException(e.getMessage());
                }
            } while (cursor.moveToNext());
        }

        if (cursor != null ) {
            cursor.close();
        }

        db.close();
        this.close();
        return basicValueObjects;
    }

    /*
     * Especial Methods
     */
    @Override
    public BasicValueObject getByServerId(long serverId) {

        if(serverId <= 0L){
            throw new IllegalArgumentException( "<<GET_CANNOT_GET_INVALID_ID>>" );
        }

        String[] filters = new String[1];
        filters[0] = ""+serverId;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(this.getTableName(), this.getTableColumns(), ID_SERVER + " = ?", filters, null, null, null, null);

        if (cursor == null) {
            throw new IllegalArgumentException( "<<GET_ID_NOT_FOUND>>" );
        }

        BasicValueObject basicValueObject;
        cursor.moveToFirst();

        try {
            basicValueObject = this.initializeVO(cursor);
        } catch (Exception e) {
            cursor.close();
            db.close();
            this.close();
            throw new UnsupportedOperationException(e.getMessage());
        }

        cursor.close();
        db.close();
        this.close();
        return basicValueObject;
    }

    @Override
    public BasicValueObject saveFromServer(BasicValueObject objectToSave) {

        if(objectToSave.getIdServer() <= 0){
            throw new IllegalArgumentException( "<<SAVE_ID_INVALIDO>>" );
        }

        if(objectToSave.getIdServer() > 0){
            return this.updateFromServer(objectToSave);
        }

        SQLiteDatabase db = this.getReadableDatabase();

        long id = db.insert(this.getTableName(), null, objectToSave.getContentValues(mContext));

        db.close();
        this.close();

        if(id <= 0){
            throw new IllegalArgumentException( "<<SAVE_NOT_INSERTED>>" );
        }

        objectToSave.setId(id);

        return objectToSave;
    }

    @Override
    public BasicValueObject updateFromServer(BasicValueObject objectToUpdate) {

        if (objectToUpdate.getIdServer() <= 0 ) {
            throw new IllegalArgumentException( "<<UPDATE_CANNOT_UPDATE_INVALID_ID>>" );
        }

        SQLiteDatabase db = this.getWritableDatabase();

        Integer rows_updated = db.update(this.getTableName(), objectToUpdate.getContentValues(mContext), ID_SERVER + " = ?", new String[] {"" + objectToUpdate.getIdServer()});

        db.close();
        this.close();

        if ( rows_updated < 0 ) {
            throw new IllegalArgumentException( "<<UPDATE_ERROR>>" );
        }

        return objectToUpdate;
    }

    @Override
    public BasicValueObject deleteByServerId(long serverId) {

        if(serverId <= 0){
            throw new IllegalArgumentException( "<<DELETE_ID_INVALIDO>>" );
        }

        BasicValueObject basicValueObject = this.getByServerId(serverId);
        Integer deleted = 0;
        if(basicValueObject != null){
            basicValueObject.removeChildren(this.mContext);
            SQLiteDatabase db = this.getWritableDatabase();
            deleted = db.delete(this.getTableName(), ID_SERVER + " = ?",
                    new String[] { ""+serverId });
            db.close();
            this.close();
        }

        if ( deleted <= 0 ) {
            throw new IllegalArgumentException( "<<DELETE_ERROR_OR_VALUE_NOT_EXIST>>" );
        }

        return basicValueObject;
    }

}
